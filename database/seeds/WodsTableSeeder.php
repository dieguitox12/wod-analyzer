<?php

use Illuminate\Database\Seeder;

class WodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('wods')->truncate();

        \DB::table('wods')->insert(array (
            array (
                'name' => 'Cindy',
                'created_at' => date('Y-m-d'),
                'time_cap' => 1200,
                'rounds' => NULL
            ),
            array (
                'name' => 'Grace',
                'created_at' => date('Y-m-d'),
                'rounds' => 1,
                'time_cap' => NULL
            ),
        ));
    }
}
