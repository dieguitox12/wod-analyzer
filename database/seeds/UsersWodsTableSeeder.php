<?php

use Illuminate\Database\Seeder;

class UsersWodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users_wods')->truncate();

        \DB::table('users_wods')->insert(array (
            array (
                'wod_id' => 1,
                'user_id' => 1,
                'lat' => null,
                'lon' => null,
                'temp' => null,
                'humidity' => null,
                'created_at' => date('Y-m-d')
            ),
            array (
                'wod_id' => 2,
                'user_id' => 1,
                'lat' => null,
                'lon' => null,
                'temp' => null,
                'humidity' => null,
                'created_at' => date('Y-m-d')
            ),
            array (
                'wod_id' => 2,
                'user_id' => 1,
                'lat' => null,
                'lon' => null,
                'temp' => null,
                'humidity' => null,
                'created_at' => date('Y-m-d')
            ),
        ));
    }
}
