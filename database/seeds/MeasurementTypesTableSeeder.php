<?php

use Illuminate\Database\Seeder;

class MeasurementTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('measurementtypes')->truncate();

        \DB::table('measurementtypes')->insert(array (
            array (
                'name' => 'Distance',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Weight',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Effort',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Time',
                'created_at' => date('Y-m-d')
            ),
        ));
    }
}
