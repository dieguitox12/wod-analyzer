<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // $this->call(GendersTableSeeder::class);
        // $this->call(UsersTableSeeder::class);
        $this->call(MeasurementTypesTableSeeder::class);
        $this->call(MeasurementsTableSeeder::class);
        $this->call(MovementsTableSeeder::class);
        // $this->call(WodsTableSeeder::class);
        // $this->call(MovementsWodsTableSeeder::class);
        // $this->call(UsersWodsTableSeeder::class);
        // $this->call(ScoresTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
