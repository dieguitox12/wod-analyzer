<?php

use Illuminate\Database\Seeder;

class ScoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('scores')->truncate();

        \DB::table('scores')->insert(array (
            array (
                'users_wods_id' => 2,
                'score' => 276,
                'wod_part' => 1,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 3,
                'score' => 245,
                'wod_part' => 1,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 1,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 2,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 3,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 4,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 5,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 6,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 7,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 8,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 9,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 10,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 11,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 12,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 13,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 14,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 15,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 16,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 17,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 18,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 30,
                'wod_part' => 19,
                'created_at' => date('Y-m-d')
            ),
            array (
                'users_wods_id' => 1,
                'score' => 17,
                'wod_part' => 20,
                'created_at' => date('Y-m-d')
            ),
        ));
    }
}
