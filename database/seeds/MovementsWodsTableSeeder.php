<?php

use Illuminate\Database\Seeder;

class MovementsWodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('movements_wods')->truncate();

        \DB::table('movements_wods')->insert(array (
            array (
                'wod_id' => 1,
                'movement_id' => 2,
                'men_measurement_id' => null,
                'women_measurement_id' => null,
                'men_measurement_value' => null,
                'women_measurement_value' => null,
                'created_at' => date('Y-m-d'),
                'reps' => 5
            ),
            array (
                'wod_id' => 1,
                'movement_id' => 3,
                'men_measurement_id' => null,
                'women_measurement_id' => null,
                'men_measurement_value' => null,
                'women_measurement_value' => null,
                'created_at' => date('Y-m-d'),
                'reps' => 10
            ),
            array (
                'wod_id' => 1,
                'movement_id' => 1,
                'men_measurement_id' => null,
                'women_measurement_id' => null,
                'men_measurement_value' => null,
                'women_measurement_value' => null,
                'created_at' => date('Y-m-d'),
                'reps' => 15
            ),
            array (
                'wod_id' => 2,
                'movement_id' => 6,
                'men_measurement_id' => 5,
                'women_measurement_id' => 5,
                'men_measurement_value' => 135,
                'women_measurement_value' => 95,
                'created_at' => date('Y-m-d'),
                'reps' => 30
            )
        ));
    }
}
