<?php

use Illuminate\Database\Seeder;

class GendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('genders')->truncate();

        \DB::table('genders')->insert(array (
            array (
                'name' => 'Male',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Female',
                'created_at' => date('Y-m-d')
            )
        ));
    }
}
