<?php

use Illuminate\Database\Seeder;

class MovementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('movements')->truncate();

        \DB::table('movements')->insert(array (
            array (
                'name' => 'Squat',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Pull Up',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Push Up',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Clean',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Jerk',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Clean & Jerk',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Snatch',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Power Clean',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Power Snatch',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Squat Snatch',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Squat Clean',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Squat Clean & Jerk',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Power Clean & Jerk',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Wall Ball Shots',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Rope Climb',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Deadlift',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Run',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Row',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Air Bike',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Ring Muscle Up',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Bar Muscle Up',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Hang Clean',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Hang Squat Clean',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Hang Power Clean',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Hang Snatch',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Hang Power Snatch',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Hang Squat Snatch',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Hang Clean & Jerk',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Hang Squat Clean & Jerk',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Hang Power Clean & Jerk',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Push Press',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Shoulder Press',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Push Jerk',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Split Jerk',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Chest-to-bar Pull up',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Bar Facing Burpee',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Front Squat',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'DB Front Squat',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Row Calorie',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Handstand Push up',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Double Under',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Single Under',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Toes-to-bar',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'DB Snatch',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'DB Cluster',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Burpee Box Jump Over',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Strict Handstand Push-up',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Handstand Walk',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'DB Single Arm Overhead Walking Lunge',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Single DB Step Up',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Rest',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Thruster',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Burpee',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Sit Up',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Burpee Box Jump',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Box Jump',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Box Jump Over',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Lateral Burpee Over Rower',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Strict Toes-to-bar',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Farmer Carry',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Kettlebell Swing',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Russian Kettlebell Swing',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Lateral Burpee Over Bar',
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Overhead Squat',
                'created_at' => date('Y-m-d')
            ),
        ));
    }
}
