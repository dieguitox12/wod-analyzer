<?php

use Illuminate\Database\Seeder;

class MeasurementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('measurements')->truncate();

        \DB::table('measurements')->insert(array (
            array (
                'name' => 'Meter',
                'symbol' => 'm',
                'measurementtype_id' => 1,
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Centimeter',
                'symbol' => 'cm',
                'measurementtype_id' => 1,
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Feet',
                'symbol' => 'ft',
                'measurementtype_id' => 1,
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Kilometer',
                'symbol' => 'km',
                'measurementtype_id' => 1,
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Pounds',
                'symbol' => 'lb',
                'measurementtype_id' => 2,
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Kilograms',
                'symbol' => 'kg',
                'measurementtype_id' => 2,
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Poods',
                'symbol' => 'poods',
                'measurementtype_id' => 2,
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Cals',
                'symbol' => 'cals',
                'measurementtype_id' => 3,
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Seconds',
                'symbol' => 'secs',
                'measurementtype_id' => 4,
                'created_at' => date('Y-m-d')
            ),
            array (
                'name' => 'Minutes',
                'symbol' => 'mins',
                'measurementtype_id' => 4,
                'created_at' => date('Y-m-d')
            ),
        ));
    }
}
