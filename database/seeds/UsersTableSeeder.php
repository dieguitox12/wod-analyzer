<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->truncate();

        \DB::table('users')->insert(array (
            array (
                'name' => 'Diego Mena',
                'email' => 'dieguito.mena@gmail.com',
                'birthdate' => '1994-04-12',
                'password' => bcrypt('dieguito12'),
                'gender_id' => 1,
                'remember_token' => NULL,
                'created_at' => date('Y-m-d'),
            )
        ));
    }
}
