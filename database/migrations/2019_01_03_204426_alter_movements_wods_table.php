<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMovementsWodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movements_wods', function (Blueprint $table) {
            $table->unsignedInteger('men_measurement_id')->nullable();
            $table->foreign('men_measurement_id')->references('id')->on('measurements');
            $table->unsignedInteger('women_measurement_id')->nullable();
            $table->foreign('women_measurement_id')->references('id')->on('measurements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('movements_wods_men_measurement_id_foreign');
        $table->dropForeign('movements_wods_women_measurement_id_foreign');
    }
}
