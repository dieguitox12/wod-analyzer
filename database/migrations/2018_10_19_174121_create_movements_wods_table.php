<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovementsWodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movements_wods', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('movement_id');
            $table->foreign('movement_id')->references('id')->on('movements');
            $table->unsignedInteger('wod_id');
            $table->foreign('wod_id')->references('id')->on('wods');
            $table->integer('reps');
            $table->double('men_measurement_value')->nullable();
            $table->double('women_measurement_value')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movements_wods');
    }
}
