let mix = require('laravel-mix');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .react('resources/assets/js/app.js', 'public/js/app.min.js')
    .sass('resources/assets/sass/app.scss', 'public/css/app.min.css')
    .webpackConfig({
        resolve: {
            modules: [
                path.resolve('./resources/assets/js'),
                path.resolve('./node_modules')
            ]
        },
    })
    .sourceMaps()
    .copy('resources/assets/images/**/*{.png,.jpg,.svg}', 'public/images')
    .disableNotifications();

