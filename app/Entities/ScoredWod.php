<?php

namespace App\Entities;

use Phpml\Regression\LeastSquares;
use Phpml\Regression\SVR;
use Phpml\SupportVectorMachine\Kernel;

use URL;
use Exception;
use Auth;

use Storage;

/**
 * Representative class of a Wod with scores.
 */
class ScoredWod extends Wod
{
    private $score;
    private $previousScores = [];

    public function __construct(array $wodData)
    {
        parent::__construct($wodData);
        $this->score = isset($wodData['score']) ? $wodData['score'] : null;
    }

    /** Methods **/
    public function setScore(WodScore $score) : self
    {
        $this->score = $score;
        return $this;
    }

    public function getScore() : WodScore
    {
        return $this->score;
    }

    public function setPreviousScores(array $previousScores) : self
    {
        $this->previousScores = $previousScores;
        return $this;
    }

    public function getPreviousScores() : array
    {
        return $this->previousScores;
    }

    private function identifyDegree(array $scores) : int
    {
        $degree = 0;

        $data = collect($scores)->map(function ($element) {
            return $element->getScore();
        })->toArray();

        $differences = [];
        while (count(array_unique($differences)) === count($differences) && count($data) > 1) {
            $differences = [];
            for ($index = 1; $index < count($data); $index++) {
                $differences[] = $data[$index] - $data[$index - 1];
            }
            $data = $differences;
            $degree++;
        }

        return $degree;
    }

    public function optimize($same = false) : WodScore
    {
        $newWodScore = unserialize(serialize($this->getScore()));
        $scores = $this->getScore()->getScores();
        $targets = [$scores[0]->getScore()];

        $degree = $this->identifyDegree($scores);

        if (!$same) {
            $data = [[0, $scores[0]->getPart()]];
        } else {
            $data = [[$scores[0]->getPart()]];
        }
        for ($i = 1; $i < count($scores); $i++) {
            if (!$same) {
                $targets[$i] = $scores[$i]->getScore() + $targets[$i - 1];
                $data[$i] = [$scores[$i - 1]->getScore(), $scores[$i]->getPart()];
            } else {
                $targets[$i] = $scores[$i]->getScore();
                $data[$i] = [$scores[$i]->getPart()];
            }
        }

        if ($degree === 1 || !$same) {
            $regression = new LeastSquares();
        } else {
            $regression = new SVR(Kernel::POLYNOMIAL, $degree = $degree, $epsilon = 0.0);
        }

        $regression->train($data, $targets);

        $newData = $regression->predict($data);
        ($newWodScore->getScores()[0])->setScore($this->getRounds() === null ? ceil($newData[0]) : round($newData[0], 2));
        for ($i = 1; $i < count($newData); $i++) {
            if (!$same) {
                $newWodScore->getScores()[$i]->setScore($this->getRounds() === null ? ceil($newData[$i] - $newData[$i - 1]) : round($newData[$i] - $newData[$i - 1], 2));
            } else {
                $newWodScore->getScores()[$i]->setScore($this->getRounds() === null ? ceil($newData[$i]) : round($newData[$i], 2));
            }
        }
        return $newWodScore;
    }

    public function edit(array $params) : self
    {
        if (isset($params['name'])) {
            $this->setName($params['name']);
        }

        if (array_key_exists('rounds', $params)) {
            $this->setRounds($params['rounds']);
        }

        if (array_key_exists('timeCap', $params)) {
            $this->setTimeCap($params['timeCap']);
        }

        if (isset($params['movements'])) {
            $wodMovements = [];
            foreach ($params['movements'] as $movement) {
                $newMovement = $movement;
                $newMovement['movement'] = new Movement($movement);
                if (isset($movement['manMeasure'])) {
                    $manMeasurement = [
                        'id' => $movement['menMeasurementId'],
                        'value' => $movement['manMeasure']
                    ];
                    $newMovement['manMeasure'] = new Measurement($manMeasurement);
                }
                if (isset($movement['womanMeasure'])) {
                    $womanMeasurement = [
                        'id' => $movement['womenMeasurementId'],
                        'value' => $movement['womanMeasure']
                    ];
                    $newMovement['womanMeasure'] = new Measurement($womanMeasurement);
                }

                unset($newMovement['id']);
                $wodMovements[] = new WodMovement($newMovement);
            }
            $this->setMovements($wodMovements);
        }

        $this->editScore($params);

        return $this->save($this->getScore()->getId());
    }

    public function editScore(array $params) : WodScore
    {
        $wodScores = [];
        $individualScores = collect($params['scores'])->map(function ($element) {
            $element['score'] = floatval($element['value']);
            $element['part'] = intval($element['part']);
            return new Score($element);
        })->toArray();

        $this->getScore()->setObservations($params['observations']);
        $this->getScore()->setScores($individualScores);

        return $this->getScore()->save($this->getId());
    }

    public function toArray($withDescription = false) : array
    {
        $array = parent::toArray($withDescription);
        $array['previousScores'] = [];
        foreach ($this->getPreviousScores() as $score) {
            $array['previousScores'][] = $score->toArray();
        }
        $array['wodScore'] = $this->getScore() ? $this->getScore()->toArray() : null;
        return $array;
    }
}
