<?php

namespace App\Entities;

use URL;
use Exception;
use Auth;

use Storage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable;

use App\Jobs\SendForgotPasswordEmail;

use App\Repositories\UserRepository;

/**
 * Representative class of a User.
 */
class User implements Authenticatable
{
    use Notifiable;

    private $id;
    private $gender;
    private $rememberMe;
    private $name;
    private $email;
    private $password;
    private $birthdate;
    private $createdAt;
    private $updatedAt;

    public function __construct(array $userData)
    {
        $this->id = isset($userData['id']) ? $userData['id'] : null;
        $this->gender = isset($userData['gender']) ? $userData['gender'] : null;
        $this->rememberMe = isset($userData['rememberMe']) ? $userData['rememberMe'] : null;
        $this->name = isset($userData['name']) ? $userData['name'] : null;
        $this->email = isset($userData['email']) ? $userData['email'] : null;
        $this->password = isset($userData['password']) ? $userData['password'] : null;
        $this->birthdate = isset($userData['birthdate']) ? $userData['birthdate'] : null;
        $this->createdAt = isset($userData['createdAt']) ?
            $userData['createdAt'] : (isset($userData['created_at']) ? $userData['created_at'] : null);
        $this->updatedAt = isset($userData['updatedAt']) ?
            $userData['updatedAt'] : (isset($userData['updated_at']) ? $userData['updated_at'] : null);
    }

    /** Methods **/

    public function setId(int $id) : self
    {
        $this->id = $id;
        return $this;
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function setName(string $name) : self
    {
        $this->name = $name;
        return $this;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setBirthdate(string $birthdate) : self
    {
        $this->birthdate = $birthdate;
        return $this;
    }

    public function getBirthdate() : string
    {
        return $this->birthdate;
    }

    public function setGender(array $gender) : self
    {
        $this->gender = $gender;
        return $this;
    }

    public function getGender() : ?array
    {
        return $this->gender;
    }

    public function setEmail(string $email) : self
    {
        $this->email = $email;
        return $this;
    }

    public function getEmail() : string
    {
        return $this->email;
    }

    public function setPassword(string $password) : self
    {
        $this->password = $password;
        return $this;
    }

    public function getPassword() : string
    {
        return $this->password;
    }

    public function setCreatedAt(string $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getCreatedAt() : string
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(string $updatedAt) : self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getUpdatedAt() : ?string
    {
        return $this->updatedAt;
    }

    public function getAuthIdentifierName() : string
    {
        return 'id';
    }

    public function getAuthIdentifier() : int
    {
        return $this->id;
    }

    public function getAuthPassword() : string
    {
        return $this->password;
    }

    public function getRememberToken() : ?string
    {
        return $this->rememberMe;
    }

    public function setRememberToken($value) : self
    {
        $this->rememberMe = $value;
        return $this;
    }

    public function getRememberTokenName() : string
    {
        return 'remember_me';
    }

    public function forgotPassword() : SendForgotPasswordEmail
    {
        $token = UserRepository::storeForgotPasswordToken($this);

        $url = URL::to('reset-password/?t=' . $token . '&email=' . $this->getEmail());

        return new SendForgotPasswordEmail(
            $this->getEmail(),
            $url,
            $this->getName(),
            'Reset password - Wod Anlayzer'
        );
    }

    public function save() : self
    {
        return UserRepository::save($this);
    }

    public function toArray() : array
    {
        return [
            'name' => $this->getName(),
            'email' => $this->getEmail(),
            'id' => $this->getId(),
            'birthdate' => $this->getBirthdate(),
            'gender' => ['id' => $this->getGender()['id'], 'name' => $this->getGender()['name']]
        ];
    }
}
