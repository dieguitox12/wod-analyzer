<?php

namespace App\Entities;

use URL;
use Exception;
use Auth;

/**
 * Representative class of a MeasurementType
 */
class MeasurementType
{
    private $id;
    private $name;
    private $createdAt;
    private $updatedAt;

    public function __construct(array $measurementTypeData)
    {
        $this->id = isset($measurementTypeData['id']) ? $measurementTypeData['id'] : null;
        $this->name = isset($measurementTypeData['name']) ? $measurementTypeData['name'] : null;
        $this->createdAt =
            isset($measurementTypeData['createdAt']) ?
                $measurementTypeData['createdAt'] :
                (isset($measurementTypeData['created_at']) ? $measurementTypeData['created_at'] : null);
        $this->updatedAt =
            isset($measurementTypeData['updatedAt']) ?
                $measurementTypeData['updatedAt'] :
                (isset($measurementTypeData['updated_at']) ? $measurementTypeData['updated_at'] : null);
    }

    /** Methods **/

    public function setId(int $id) : self
    {
        $this->id = $id;
        return $this;
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function setName(string $name) : self
    {
        $this->name = $name;
        return $this;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setCreatedAt(string $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getCreatedAt() : string
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(string $updatedAt) : self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getUpdatedAt() : ?string
    {
        return $this->updatedAt;
    }

    public function toArray() : array
    {
        return [
            'name' => $this->getName(),
            'createdAt' => $this->getCreatedAt(),
            'updatedAt' => $this->getUpdatedAt(),
            'id' => $this->getId(),
        ];
    }
}
