<?php

namespace App\Entities;

use App\Repositories\WodRepository;
use App\Enums\IntervalMode;

use URL;
use Exception;
use Auth;

use Storage;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Carbon\Carbon;

/**
 * Representative class of a Wod
 */
class Wod
{
    private $id;
    private $name;
    private $rounds;
    private $movements;
    private $timeCap;
    private $createdBy;
    private $createdAt;
    private $updatedAt;

    public function __construct(array $wodData)
    {
        $this->id = isset($wodData['id']) ? $wodData['id'] : null;
        $this->name = isset($wodData['name']) ? $wodData['name'] : null;
        $this->rounds = isset($wodData['rounds']) ? $wodData['rounds'] : null;
        $this->movements = isset($wodData['movements']) ? $wodData['movements'] : null;
        $this->timeCap = isset($wodData['timeCap']) ? $wodData['timeCap'] : null;
        $this->createdBy = isset($wodData['createdBy']) ? $wodData['createdBy'] : null;
        $this->createdAt =
            isset($wodData['createdAt']) ?
                $wodData['createdAt'] :
                (isset($wodData['created_at']) ? $wodData['created_at'] : null);
        $this->updatedAt =
            isset($wodData['updatedAt']) ?
                $wodData['updatedAt'] :
                (isset($wodData['updated_at']) ? $wodData['updated_at'] : null);
    }

    /** Methods **/

    public function setId(int $id) : self
    {
        $this->id = $id;
        return $this;
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function setName(string $name) : self
    {
        $this->name = $name;
        return $this;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setRounds(?int $rounds) : self
    {
        $this->rounds = $rounds;
        return $this;
    }

    public function getRounds() : ?int
    {
        return $this->rounds;
    }

    public function setCreatedBy(?User $createdBy) : self
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    public function getCreatedBy() : ?User
    {
        return $this->createdBy;
    }

    public function setTimeCap(?int $timeCap) : self
    {
        $this->timeCap = $timeCap;
        return $this;
    }

    public function getTimeCap() : ?int
    {
        return $this->timeCap;
    }

    public function setMovements(array $movements) : self
    {
        $this->movements = $movements;
        return $this;
    }

    public function getMovements() : array
    {
        return $this->movements;
    }

    public function setCreatedAt(string $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getCreatedAt() : string
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(string $updatedAt) : self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getUpdatedAt() : ?string
    {
        return $this->updatedAt;
    }

    public function getDescription() : ?string
    {
        $description = '';
        if ($this->rounds) {
            if ($this->rounds > 1) {
                $description = $this->rounds . ' RFT: ';
            } else {
                $description = 'FT: ';
            }
        } else {
            $description = gmdate('i:s', $this->timeCap) . ' min AMRAP: ';
        }

        for ($index = 0; $index < count($this->movements); $index++) {
            $description .= $this->movements[$index]->getReps() . ' ' .
                            $this->movements[$index]->getMovement()->getName();
            if (isset($this->movements[$index + 1])) {
                $description .= ', ';
            }
        }
        return $description;
    }

    public function save(int $wodScoreId = null) : self
    {
        return WodRepository::save($this, $wodScoreId);
    }

    public function delete() : void
    {
        WodRepository::delete($this);
    }

    public function score(array $params) : WodScore
    {
        $wodScores = [];
        $individualScores = collect($params['scores'])->map(function ($element) {
            $element['score'] = floatval($element['value']);
            $element['part'] = intval($element['part']);
            return new Score($element);
        })->toArray();

        $data['scores'] = $individualScores;
        $data['observations'] = $params['observations'];

        $wodScore = new WodScore($data);
        return $wodScore->save($this->id);
    }

    public function estimateScores(UploadedFile $video, int $interval = IntervalMode::PER_MOVEMENTS, ?int $perReps = null, ?int $perSeconds = null) : array
    {
        $filename = Carbon::now()->timestamp . '.' . $video->getClientOriginalExtension();
        $path = public_path() . '/Python/uploaded/' ;
        $video->move($path, $filename);

        $newPath = $path . $filename;

        ob_start();
        passthru(public_path() . '/Python/play_and_predict.py ' . $newPath . ' ' . public_path() . '/Python');
        $output = ob_get_clean(); 
        $outputs = preg_replace( "/\r|\n/", "-", $output);
        $outputs = explode('-', $outputs);
        $predictions = explode(',', $outputs[0]);
        $fps = intval($outputs[1]);
        $totalFrames = intval($outputs[2]);
        $totalReps = explode(';', $outputs[3]);

        switch ($interval) {
            case IntervalMode::PER_MOVEMENTS:
                return $this->estimatePerMovement($fps, $predictions);
            case IntervalMode::PER_ROUNDS:
                return $this->estimatePerRound($fps, $predictions);
            case IntervalMode::PER_REPS:
                return $this->estimatePerReps($fps, $predictions, $totalReps, $perReps);
            default:
                return $this->estimatePerSeconds($fps, $predictions, $totalReps, $perSeconds);
        }
    }

    private function estimatePerSeconds(int $fps, array $predictions, array $totalReps, int $perSeconds) : array
    {
        $clusters = $this->getClusters($predictions);
        $scores = [];
        $arrayReps = [];
        foreach ($totalReps as $key => $repsPerMovement) {
            $arrayReps[] = explode(',', str_replace('[', '', str_replace(']', '', $repsPerMovement)));
        }

        $intTotalReps = array_sum(collect($arrayReps)->map(function ($element) {
            return array_sum($element);
        })->toArray());

        $allSeconds = 0;
        $reps = [];
        
        $seconds = 0;
        foreach ($clusters as $key => $cluster) {
            $arrayCluster = explode(',', $cluster);
            foreach ($arrayCluster as $key2 => $pred) {
                if (ceil($allSeconds) >= $this->getTimeCap()) {
                    break;
                }
                $seconds += (env('FRAMES_PER_PREDICTION') * env('FRAMES_PER_FLOW')) / $fps;
                $allSeconds += (env('FRAMES_PER_PREDICTION') * env('FRAMES_PER_FLOW')) / $fps;

                if (intval($arrayReps[$key][0]) === 1) {
                    if ($key2 % 2 === 0) {
                        if (isset($arrayReps[$key][$key2 / 2])) {
                            $reps[] = $arrayReps[$key][$key2 / 2];
                        }
                    }
                } else {
                    $reps[] = 0;
                }
                
                if (ceil($seconds) >= $perSeconds) {
                    $seconds = 0;
                    $scores[] = $reps;
                    $reps = [];
                }
            }
            
            if (ceil($allSeconds) >= $this->getTimeCap()) {
                break;
            }
        }

        if (array_sum($reps) > 0) {
            $scores[] = $reps;
        }

        $estimatedScores = [];
        foreach ($scores as $score) {
            $estimatedScores[] = ceil(array_sum($score));
        }
        return $estimatedScores;
    }

    private function estimatePerReps(int $fps, array $predictions, array $totalReps, int $perReps) : array
    {
        $clusters = $this->getClusters($predictions);
        $scores = [];
        $arrayReps = [];
        foreach ($totalReps as $key => $repsPerMovement) {
            $arrayReps[] = explode(',', str_replace('[', '', str_replace(']', '', $repsPerMovement)));
        }

        $intTotalReps = array_sum(collect($arrayReps)->map(function ($element) {
            return array_sum($element);
        })->toArray());

        $allReps = 0;
        $repsCount = 0;
        $seconds = [];
        $offset = 0;
        foreach ($arrayReps as $key => $reps) {
            if ($allReps > $intTotalReps) {
                break;
            }
            if (intval($reps[0]) === 0) {
                $seconds[] = ((env('FRAMES_PER_PREDICTION') * env('FRAMES_PER_FLOW')) / $fps) * count(explode(',', $clusters[$key]));
            } else {
                if (count($reps) < ($perReps - $repsCount)) {
                    $repsCount += count($reps);
                    $allReps += count($reps);
                    $seconds[] = ((env('FRAMES_PER_PREDICTION') * env('FRAMES_PER_FLOW')) / $fps) * count(explode(',', $clusters[$key]));
                } else {
                    foreach ($reps as $key2 => $rep) {
                        if ($repsCount === $perReps) {
                            $scores[] = $seconds;
                            $seconds = [];
                            $repsCount = 0;
                        }
                        $seconds[] = ((env('FRAMES_PER_PREDICTION') * env('FRAMES_PER_FLOW')) / $fps) * 2;
                        $repsCount++;
                        $allReps++;
                    }
                }
                
            }
            
        }

        if ($repsCount > 0 && $repsCount <= $perReps) {
            $scores[] = $seconds;
        }
        
        $estimatedScores = [];
        foreach ($scores as $score) {
            $estimatedScores[] = ceil(array_sum($score));
        }
        return $estimatedScores;
    }


    private function estimatePerRound(int $fps, array $predictions) : array
    {
        $clusters = $this->getClusters($predictions);
        $scores = [];
        $movementIndex = 0;
        $roundIndex = 0;
        dump($clusters);
        foreach ($clusters as $cluster) {
            $seconds = [];
            $arrayCluster = explode(',', $cluster);
            foreach ($arrayCluster as $prediction) {
                if (count($arrayCluster) >= 2) {
                    if (intval($prediction) === $this->getMovements()[$movementIndex]->getMovement()->getId() || intval($prediction) === 0) {
                        $seconds[] = (env('FRAMES_PER_PREDICTION') * env('FRAMES_PER_FLOW')) / $fps;
                    } elseif (array_search(intval($prediction), collect($this->getMovements())->map(function ($el) { return $el->getMovement()->getId(); })->toArray()) === FALSE) {
                        $seconds[] = (env('FRAMES_PER_PREDICTION') * env('FRAMES_PER_FLOW')) / $fps;
                    } else {
                        $movementIndex++;
                        
                        
                        $seconds = [];
                        if ($movementIndex >= count($this->getMovements())) {
                            if ($roundIndex + 1 < intval($this->getRounds())) {
                                $roundIndex++;
                            }
                            $movementIndex = 0;
                        }
                    }
                } else {
                    $seconds[] = (env('FRAMES_PER_PREDICTION') * env('FRAMES_PER_FLOW')) / $fps;
                }
            }
            $scores[$roundIndex][] = array_sum($seconds);
            
        }
        $estimatedScores = [];
        foreach ($scores as $score) {
            $estimatedScores[] = ceil(array_sum($score));
        }
        return $estimatedScores;
    }

    private function getClusters(array $predictions) : array
    {
        $prev = null; 
        $key = -1; 
        foreach ($predictions as $value) { 
            if ($prev === $value){ 
                $res[$key] .= ',' . $value;
            } else { 
                $key++; 
                $res[$key] = $value; 
                $prev = $value; 
            } 
        }
        return $res;
    }


    private function estimatePerMovement(int $fps, array $predictions) : array
    {
        $clusters = $this->getClusters($predictions);
        $scores = [];
        $movementIndex = 0;
        $movementIndex2 = 0;
        foreach ($clusters as $cluster) {
            $seconds = [];
            $arrayCluster = explode(',', $cluster);
            foreach ($arrayCluster as $prediction) {
                if (count($arrayCluster) >= 2) {
                    // if (intval($prediction) === $this->getMovements()[$movementIndex]->getMovement()->getId() || intval($prediction) === 0) {
                    //     $seconds[] = (env('FRAMES_PER_PREDICTION') * env('FRAMES_PER_FLOW')) / $fps;
                    // } elseif (array_search(intval($prediction), collect($this->getMovements())->map(function ($el) { return $el->getMovement()->getId(); })->toArray()) === FALSE) {
                        $seconds[] = (env('FRAMES_PER_PREDICTION') * env('FRAMES_PER_FLOW')) / $fps;
                    // } else {
                        if ($movementIndex + 1 < count($this->getMovements())) {
                            if ($this->getRounds() < 2) {
                                if (intval($prediction) === $this->getMovements()[$movementIndex + 1]->getMovement()->getId()) {
                                    $movementIndex++;
                                    $movementIndex2++;
                                }
                            } else {
                                $movementIndex++;
                                $movementIndex2++;
                            }
                            
                        }
                        if ($movementIndex >= count($this->getMovements()) && $this->getRounds() > 1) {
                            $movementIndex = 0;
                        }
                        // $seconds = [];
                    // }
                } else {
                    $seconds[] = (env('FRAMES_PER_PREDICTION') * env('FRAMES_PER_FLOW')) / $fps;
                }
            }
            $scores[$movementIndex2][] = array_sum($seconds);
        }
        $estimatedScores = [];
        foreach ($scores as $score) {
            $estimatedScores[] = ceil(array_sum($score));
        }
        return $estimatedScores;
    }

    public function toArray($withDescription = false) : array
    {
        $movements = [];
        foreach ($this->getMovements() as $movement) {
            $movements[] = $movement->toArray();
        }

        $description = '';
        if ($withDescription) {
            $description = $this->getDescription();
        }

        return [
            'name' => $this->getName(),
            'rounds' => $this->getRounds(),
            'timeCap' => $this->getTimeCap(),
            'createdBy' => $this->getCreatedBy() ? $this->getCreatedBy()->toArray() : null,
            'description' => $description,
            'createdAt' => $this->getCreatedAt(),
            'updatedAt' => $this->getUpdatedAt(),
            'movements' => $movements,
            'id' => $this->getId()
        ];
    }
}
