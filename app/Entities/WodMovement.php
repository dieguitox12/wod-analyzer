<?php

namespace App\Entities;

use URL;
use Exception;
use Auth;

/**
 * Representative class of a Movement in the wod
 */
class WodMovement
{
    private $id;
    private $movement;
    private $reps;
    private $manMeasure;
    private $womanMeasure;
    private $createdAt;
    private $updatedAt;

    public function __construct(array $wodMovementData)
    {
        $this->id = isset($wodMovementData['id']) ? $wodMovementData['id'] : null;
        $this->movement = isset($wodMovementData['movement']) ? $wodMovementData['movement'] : null;
        $this->reps = isset($wodMovementData['reps']) ? $wodMovementData['reps'] : null;
        $this->manMeasure = isset($wodMovementData['manMeasure']) ? $wodMovementData['manMeasure'] : null;
        $this->womanMeasure = isset($wodMovementData['womanMeasure']) ? $wodMovementData['womanMeasure'] : null;
        $this->createdAt =
            isset($wodMovementData['createdAt']) ?
                $wodMovementData['createdAt'] :
                (isset($wodMovementData['created_at']) ? $wodMovementData['created_at'] : null);
        $this->updatedAt =
            isset($wodMovementData['updatedAt']) ?
                $wodMovementData['updatedAt'] :
                (isset($wodMovementData['updated_at']) ? $wodMovementData['updated_at'] : null);
    }

    /** Methods **/

    public function setId(int $id) : self
    {
        $this->id = $id;
        return $this;
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function setMovement(Movement $movement) : self
    {
        $this->movement = $movement;
        return $this;
    }

    public function getMovement() : Movement
    {
        return $this->movement;
    }

    public function setReps(int $reps) : self
    {
        $this->reps = $reps;
        return $this;
    }

    public function getReps() : ?int
    {
        return $this->reps;
    }

    public function setManMeasure(Measurement $manMeasure) : self
    {
        $this->manMeasure = $manMeasure;
        return $this;
    }

    public function getManMeasure() : ?Measurement
    {
        return $this->manMeasure;
    }

    public function setWomanMeasure(Measurement $womanMeasure) : self
    {
        $this->womanMeasure = $womanMeasure;
        return $this;
    }

    public function getWomanMeasure() : ?Measurement
    {
        return $this->womanMeasure;
    }

    public function setCreatedAt(string $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getCreatedAt() : string
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(string $updatedAt) : self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getUpdatedAt() : ?string
    {
        return $this->updatedAt;
    }

    public function toArray() : array
    {
        return [
            'movement' => $this->getMovement()->toArray(),
            'manMeasure' => $this->getManMeasure() ? $this->getManMeasure()->toArray() : null,
            'womanMeasure' => $this->getWomanMeasure() ? $this->getWomanMeasure()->toArray() : null,
            'createdAt' => $this->getCreatedAt(),
            'updatedAt' => $this->getUpdatedAt(),
            'reps' => $this->getReps(),
            'id' => $this->getId(),
        ];
    }
}
