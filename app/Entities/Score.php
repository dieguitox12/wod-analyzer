<?php

namespace App\Entities;

use URL;
use Exception;
use Auth;

/**
 * Representative class of a Score
 */
class Score
{
    private $id;
    private $part;
    private $score;
    private $createdAt;
    private $updatedAt;

    public function __construct(array $scoreData)
    {
        $this->id = isset($scoreData['id']) ? $scoreData['id'] : null;
        $this->part = isset($scoreData['part']) ? $scoreData['part'] : null;
        $this->score = isset($scoreData['score']) ? $scoreData['score'] : null;
        $this->createdAt =
            isset($scoreData['createdAt']) ?
                $scoreData['createdAt'] :
                (isset($scoreData['created_at']) ? $scoreData['created_at'] : null);
        $this->updatedAt =
            isset($scoreData['updatedAt']) ?
                $scoreData['updatedAt'] :
                (isset($scoreData['updated_at']) ? $scoreData['updated_at'] : null);
    }

    /** Methods **/

    public function setId(int $id) : self
    {
        $this->id = $id;
        return $this;
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function setPart(int $part) : self
    {
        $this->part = $part;
        return $this;
    }

    public function getPart() : int
    {
        return $this->part;
    }

    public function setScore(float $score) : self
    {
        $this->score = $score;
        return $this;
    }

    public function getScore() : float
    {
        return $this->score;
    }

    public function setCreatedAt(string $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getCreatedAt() : string
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(string $updatedAt) : self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getUpdatedAt() : ?string
    {
        return $this->updatedAt;
    }

    public function toArray() : array
    {
        return [
            'value' => $this->getScore(),
            'part' => $this->getPart(),
            'createdAt' => $this->getCreatedAt(),
            'updatedAt' => $this->getUpdatedAt(),
            'id' => $this->getId(),
        ];
    }
}
