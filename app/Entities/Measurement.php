<?php

namespace App\Entities;

use URL;
use Exception;
use Auth;

/**
 * Representative class of a Measurement
 */
class Measurement
{
    private $id;
    private $name;
    private $symbol;
    private $measurementType;
    private $value;
    private $createdAt;
    private $updatedAt;

    public function __construct(array $measurementData)
    {
        $this->id = isset($measurementData['id']) ? $measurementData['id'] : null;
        $this->name = isset($measurementData['name']) ? $measurementData['name'] : null;
        $this->symbol = isset($measurementData['symbol']) ? $measurementData['symbol'] : null;
        $this->measurementType = isset($measurementData['measurementType']) ?
            $measurementData['measurementType'] : null;
        $this->value = isset($measurementData['value']) ? $measurementData['value'] : null;
        $this->createdAt = isset($measurementData['createdAt']) ?
            $measurementData['createdAt'] :
                (isset($measurementData['created_at']) ? $measurementData['created_at'] : null);
        $this->updatedAt = isset($measurementData['updatedAt']) ?
            $measurementData['updatedAt'] :
                (isset($measurementData['updated_at']) ? $measurementData['updated_at'] : null);
    }

    /** Methods **/

    public function setId(int $id) : self
    {
        $this->id = $id;
        return $this;
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function setName(string $name) : self
    {
        $this->name = $name;
        return $this;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setSymbol(string $symbol) : self
    {
        $this->symbol = $symbol;
        return $this;
    }

    public function getSymbol() : string
    {
        return $this->symbol;
    }

    public function setMeasurementType(MeasurementType $measurementType) : self
    {
        $this->measurementType = $measurementType;
        return $this;
    }

    public function getMeasurementType() : MeasurementType
    {
        return $this->measurementType;
    }

    public function setValue(float $value) : self
    {
        $this->value = $value;
        return $this;
    }

    public function getValue() : float
    {
        return $this->value;
    }

    public function setCreatedAt(string $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getCreatedAt() : string
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(string $updatedAt) : self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getUpdatedAt() : ?string
    {
        return $this->updatedAt;
    }

    public function toArray() : array
    {
        return [
            'name' => $this->getName(),
            'symbol' => $this->getSymbol(),
            'value' => $this->getValue(),
            'measurementType' => $this->getMeasurementType()->toArray(),
            'createdAt' => $this->getCreatedAt(),
            'updatedAt' => $this->getUpdatedAt(),
            'id' => $this->getId(),
        ];
    }
}
