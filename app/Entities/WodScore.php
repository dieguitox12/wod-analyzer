<?php

namespace App\Entities;

use App\Repositories\WodRepository;

use URL;
use Exception;
use Auth;

/**
 * Representative class of the scores of a wod
 */
class WodScore
{
    private $id;
    private $scores;
    private $observations;
    private $createdAt;
    private $updatedAt;

    public function __construct(array $wodScoreData)
    {
        $this->id = isset($wodScoreData['id']) ? $wodScoreData['id'] : null;
        $this->observations = isset($wodScoreData['observations']) ? $wodScoreData['observations'] : null;
        $this->scores = isset($wodScoreData['scores']) ? $wodScoreData['scores'] : null;
        $this->createdAt =
            isset($wodScoreData['createdAt']) ?
                $wodScoreData['createdAt'] :
                (isset($wodScoreData['created_at']) ? $wodScoreData['created_at'] : null);
        $this->updatedAt =
            isset($wodScoreData['updatedAt']) ?
                $wodScoreData['updatedAt'] :
                (isset($wodScoreData['updated_at']) ? $wodScoreData['updated_at'] : null);
    }

    /** Methods **/

    public function setId(int $id) : self
    {
        $this->id = $id;
        return $this;
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function setObservations(?string $observations) : self
    {
        $this->observations = $observations;
        return $this;
    }

    public function getObservations() : ?string
    {
        return $this->observations;
    }

    public function setScores(array $scores) : self
    {
        $this->scores = $scores;
        return $this;
    }

    public function getScores() : array
    {
        return $this->scores;
    }

    public function setCreatedAt(string $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getCreatedAt() : string
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(string $updatedAt) : self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getUpdatedAt() : ?string
    {
        return $this->updatedAt;
    }

    public function save(int $wodId) : self
    {
        return WodRepository::score($wodId, $this);
    }

    public function toArray() : array
    {
        $scores = [];
        foreach ($this->getScores() as $score) {
            $scores[] = $score->toArray();
        }

        return [
            'createdAt' => $this->getCreatedAt(),
            'updatedAt' => $this->getUpdatedAt(),
            'observations' => $this->getObservations(),
            'scores' => $scores,
            'id' => $this->getId(),
        ];
    }
}
