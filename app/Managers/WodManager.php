<?php

namespace App\Managers;

use App\Entities\Wod;
use App\Entities\ScoredWod;
use App\Entities\Movement;
use App\Entities\WodMovement;
use App\Entities\Measurement;

use App\Repositories\WodRepository;
use Exception;

/**
 * Representative class of a Wod manager.
 */
class WodManager
{

    /**
     * Find a Wod by its id
     *
     * @param integer $id
     * @return Wod
     */
    public static function findById(int $id) : Wod
    {
        return WodRepository::findSimpleWodById($id);
    }

    /**
     * Find a Wod by its score id
     *
     * @param integer $wodScoreId
     * @return Wod
     */
    public static function findByWodScoreId(int $wodScoreId) : ScoredWod
    {
        return WodRepository::findByWodScoreId($wodScoreId);
    }

    public static function store(array $wodData) : Wod
    {
        $wodMovements = [];
        foreach ($wodData['movements'] as $movement) {
            $newMovement = $movement;
            $newMovement['movement'] = new Movement($movement);
            if (isset($movement['manMeasure'])) {
                $manMeasurement = [
                    'id' => $movement['menMeasurementId'],
                    'value' => $movement['manMeasure']
                ];
                $newMovement['manMeasure'] = new Measurement($manMeasurement);
            }
            if (isset($movement['womanMeasure'])) {
                $womanMeasurement = [
                    'id' => $movement['womenMeasurementId'],
                    'value' => $movement['womanMeasure']
                ];
                $newMovement['womanMeasure'] = new Measurement($womanMeasurement);
            }

            unset($newMovement['id']);
            $wodMovements[] = new WodMovement($newMovement);
        }
        $wodData['movements'] = $wodMovements;
        $newWod = new Wod($wodData);

        return $newWod->save();
    }

    /**
     * Get all wods
     *
     * @return array
     */
    public static function allWods() : array
    {
        return WodRepository::allWods();
    }

    /**
     * Get all movements
     *
     * @return array
     */
    public static function allMovements() : array
    {
        return WodRepository::allMovements();
    }

    /**
     * Search wods with their scores
     *
     * @param array $searchArray
     * @param boolean $withPagination
     * @return array
     */
    public static function search(array $searchArray, bool $withPagination = true) : array
    {
        $listWods = WodRepository::search($searchArray, $withPagination);

        $listWodsArray = [];
        $listWodsArray['data'] = [];
        foreach ($listWods as $wod) {
            if (!is_array($wod)) {
                if ($withPagination) {
                    $listWodsArray['data'][] = $wod->toArray();
                } else {
                    $listWodsArray[] = $wod->toArray();
                }
            }
        }
        if ($withPagination) {
            $listWodsArray['pagination'] = $listWods['pagination'];
        } else {
            unset($listWodsArray['data']);
        }
        return $listWodsArray;
    }
}
