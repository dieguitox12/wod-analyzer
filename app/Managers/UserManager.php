<?php

namespace App\Managers;

use App\Entities\User;
use App\Repositories\UserRepository;
use Exception;

/**
 * Representative class of a user manager.
 */
class UserManager
{
    public static function findById(int $id) : ?User
    {
        return UserRepository::findById($id);
    }

    public static function updateToken(int $id, string $token) : void
    {
        UserRepository::updateToken($id, $token);
    }

    public static function getByToken(int $id, string $token) : ?User
    {
        return UserRepository::getByToken($id, $token);
    }

    public static function findByEmail(string $email, bool $loginAttempt = false) : ?User
    {
        return UserRepository::findByEmail($email, $loginAttempt);
    }

    public static function store(array $userData) : User
    {
        $userData['password'] = bcrypt($userData['password']);
        $newUser = new User($userData);
        return $newUser->save();
    }

    public static function resetPassword(string $token, string $newPassword) : bool
    {
        $user = UserRepository::deleteForgotPasswordToken($token);

        if (!$user) {
            throw new Exception(
                "This link has expired. Request another one and try again.",
                \App\Enums\ErrorCode::CUSTOM_EXCEPTION
            );
        }

        $hashedPassword = bcrypt($newPassword);

        $user->setPassword($hashedPassword)->save();

        return true;
    }
}
