<?php

namespace App\Validators;

use App\Enums\UserRole;

/**
 * Representative class of a validator for the Auth controller.
 */
class AuthValidator extends AbstractValidator
{

    /**
     * Validates the incoming Forgot Password request.
     *
     * @return bool|array
     */
    public function validateForgotPasswordRequest()
    {
        return $this->validateRules([
          'email' => 'bail|required|email|exists:users,email,deleted_at,NULL'
        ]);
    }

    /**
     * Validates the incoming login request.
     *
     * @return bool|array
     */
    public function validateLoginRequest()
    {
        return $this->validateRules([
          'email' => "bail|required|string|exists:users,email,deleted_at,NULL",
          'password' => 'required|string',
          'rememberMe' => 'sometimes|bool'
        ]);
    }

    /**
     * Validates the incoming create request.
     *
     * @return bool|array
     */
    public function validateCreateRequest()
    {
        return $this->validateRules([
          'email' => 'bail|required|email|unique:users,email,NULL,id,deleted_at,NULL',
          'password' => 'required|string',
          'passwordConfirmation' => 'required|same:password',
          'gender' => 'bail|sometimes|integer|min:0|exists:genders,id',
          'name' => 'required|string|filled',
          'birthdate' => 'required|date',
        ]);
    }

    /**
     * Validates the incoming reset password request.
     * @return bool|array
     */
    public function validateResetPasswordRequest()
    {
        return $this->validateRules([
            'token' => 'required|string',
            'password' => 'required|string',
            'passwordConfirmation' => 'required|same:password'
        ]);
    }
}
