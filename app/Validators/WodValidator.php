<?php

namespace App\Validators;

/**
 * Representative class of a validator for the Wod controller.
 */
class WodValidator extends AbstractValidator
{

    public function validateGetByWodScoreIdRequest()
    {
        return $this->validateRules([
            'wodScoreId' => 'required|integer|exists:users_wods,id,deleted_at,NULL',
        ]);
    }

    public function validateSearchRequest()
    {
        return $this->validateRules([
            'q' => 'sometimes|string',
            'page' => 'sometimes|integer|min:1',
            'perPage' => 'sometimes|integer|min:1',
            'from' => 'sometimes|date',
            'to' => 'sometimes|date|after:from',
        ]);
    }

    public function validateEditRequest($id)
    {
        return $this->validateRules([
            'name' => 'bail|sometimes|string|filled|unique:wods,name,' . $id . ',id,deleted_at,NULL',
            'rounds' => 'sometimes|integer',
            'observations' => 'sometimes|string|nullable',
            'timeCap' => 'sometimes|numeric',
            'movements' => 'sometimes|array',
            'movements.*' => 'sometimes|array',
            'movements.*.id' => 'sometimes|integer|exists:movements,id,deleted_at,NULL',
            'movements.*.reps' => 'sometimes|integer',
            'movements.*.manMeasure' => 'sometimes|numeric',
            'movements.*.womanMeasure' => 'sometimes|numeric',
            'movements.*.menMeasurementId' => 'sometimes|integer|exists:measurements,id,deleted_at,NULL',
            'movements.*.womenMeasurementId' => 'sometimes|integer|exists:measurements,id,deleted_at,NULL',
            'scores' => 'required|array',
            'scores.*' => 'required|array',
            'scores.*.value' => 'required|numeric',
            'scores.*.part' => 'required|numeric',
            'lat' => 'sometimes|numeric',
            'lon' => 'sometimes|numeric'
        ]);
    }

    public function validateCreateRequest()
    {
        return $this->validateRules([
            'name' => 'required|string|unique:wods,name,NULL,id,deleted_at,NULL',
            'rounds' => 'sometimes|integer',
            'observations' => 'sometimes|string|nullable',
            'timeCap' => 'sometimes|numeric',
            'movements' => 'required|array',
            'movements.*' => 'required|array',
            'movements.*.id' => 'required|integer|exists:movements,id,deleted_at,NULL',
            'movements.*.reps' => 'sometimes|integer',
            'movements.*.manMeasure' => 'sometimes|numeric',
            'movements.*.womanMeasure' => 'sometimes|numeric',
            'movements.*.menMeasurementId' => 'sometimes|integer|exists:measurements,id,deleted_at,NULL',
            'movements.*.womenMeasurementId' => 'sometimes|integer|exists:measurements,id,deleted_at,NULL',
            'scores' => 'required|array',
            'scores.*' => 'required|array',
            'scores.*.value' => 'required|numeric',
            'scores.*.part' => 'required|numeric',
            'lat' => 'sometimes|numeric',
            'lon' => 'sometimes|numeric'
        ]);
    }

    public function validateScoreRequest()
    {
        return $this->validateRules([
            'wodId' => 'required|integer|exists:wods,id,deleted_at,NULL',
            'observations' => 'sometimes|string|nullable',
            'scores' => 'required|array',
            'scores.*' => 'required|array',
            'scores.*.value' => 'required|numeric',
            'scores.*.part' => 'required|numeric',
            'lat' => 'sometimes|numeric',
            'lon' => 'sometimes|numeric'
        ]);
    }
}
