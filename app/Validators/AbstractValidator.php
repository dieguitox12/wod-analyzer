<?php

namespace App\Validators;

use Illuminate\Http\Request;
use App\Enums\ErrorCode;
use Illuminate\Validation\Validator;
use Validator as ValidatorMaker;

/**
 * Abstract class of a validator.
 */
abstract class AbstractValidator
{
    /**
     * @var Illuminate\Http\Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Given an array containing a set of rules, validates them
     * and return the results.
     *
     * @param array  $rules       the rules to validate.
     * @return bool|array         returns TRUE if all the rules pass,
     *                            an array otherwise.
     */
    protected function validateRules(array $rules)
    {
        $validator = ValidatorMaker::make($this->request->all(), $rules);

        return $validator->passes() ?: $this->getErrorlist($validator);
    }

    /**
     * Given an array of parameters and a validator, gets the first ErrorCode
     * and the message associated with each parameter.
     *
     * @param Validator $validator  The validator instance that was used to validate
     *                              the parameters.
     */
    protected function getErrorList(Validator $validator)
    {
        $errors = [];
        $validatorErrors = $validator->errors();
        $validatorFailedRules = $validator->failed();

        foreach ($validatorFailedRules as $parameter => $failedRules) {
            $firstFailedRule = key($failedRules);
            $firstFailedRuleMessage = $validatorErrors->get($parameter)[0];

            $errors[$parameter] = [
                'code' => $this->getErrorCode($firstFailedRule),
                'message' => $firstFailedRuleMessage,
            ];
        }
        return $errors;
    }

    /**
     * Given the name of a rule, returns the appropriated error
     * code.
     *
     * @param string $ruleName    the name of the rule according to laravel's validation.
     *                            Some examples includes required, exists, Unique, etc.
     * @return int                an appropriated error code for the given rule.
     */
    private function getErrorCode(string $ruleName)
    {
        switch ($ruleName) {
            case 'Required':
            case 'RequiredIf':
            case 'RequiredWith':
                return ErrorCode::REQUIRED;

            case 'Unique':
                return ErrorCode::ALREADY_TAKEN;

            case 'In':
            case 'Exists':
            case 'Appears':
                return ErrorCode::NOT_FOUND;

            case 'NotPresent':
                return ErrorCode::NOT_PRESENT;

            case 'CouponAvailable':
            case 'Same':
                return ErrorCode::INVALID_STATE;

            default:
                return ErrorCode::BAD_FORMAT;
        }
    }
}
