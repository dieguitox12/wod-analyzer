<?php

namespace App\Exceptions;

use App\Http\Controllers\Controller;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Exception;
use Log;
use App\Enums\ErrorCode;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        // parent::report($exception);

        // We'll manually log the error (see the render function below).
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // If the response is in our not-to-log list, then we'll
        // just call the parent render and return it's response.

        if ($this->shouldntReport($exception)) {
            return parent::render($request, $exception);
        }

        if ($exception instanceof NotFoundHttpException) {
            return redirect('/404');
        }

        // Let the unauthenticated function manage the
        // authenticated exceptions.

        if ($exception instanceof AuthenticationException) {
            return $this->unauthenticated($request, $exception);
        }

        // Manually logs the error. We'll log the error class,
        // along with it's data.
        Log::error(get_class($exception) . ':', [
            'path' => url($request->path()),
            'error' => $exception->getMessage(),
            'data' => $request->all(),
            'trace' => $exception->getTraceAsString(),
        ]);

        $message = 'Something went wrong in the server. Please try again later.';
        if ($exception->getCode() === ErrorCode::CUSTOM_EXCEPTION || $exception->getCode() === ErrorCode::NOT_FOUND) {
            $message = $exception->getMessage();
        }
        // Returns a JSON response.
        return response()->json([
            'error' => $message
        ], $exception->getCode() === ErrorCode::NOT_FOUND ? ErrorCode::NOT_FOUND : 500);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }
}
