<?php

namespace App\Services\Mailer;

/**
 * Interface of a blade template.
 */
interface InterfaceTemplate
{
    /**
     * Creates a new template.
     * @param $data array[]
     */
    public function __construct($data);

    /**
     * Get an html view.
     * @return String
     */
    public function render();

    /**
     * Verifies that the given data if valid.
     * @param $data array[]
     * @return Boolean
     */
    public function validateData($data);
}
