<?php

namespace App\Services\Mailer;

/**
 * Representative class of a template for the forgot password mail.
 */
class Mailer
{
    /**
     * @var Boolean
     */
    protected $useTemplate = false;

    /**
     * @var Boolean
     */
    protected $contentIsHtml = true;

    /**
     * @var InterfaceTemplate
     */
    protected $templateRender;

    /**
     * @var String
     */
    protected $from;

    /**
     * @var String[]
     */
    protected $destinataries;

    /**
     * @var String
     */
    protected $subject;

    /**
     * @var String
     */
    protected $content;

    /**
     * @var array Data for the template
     */
    protected $data = [];

    /**
     * Constructs a new Mailer
     * @param $from String
     * @param $destinataries String[]
     * @param $subject
     */
    public function __construct($from, $destinataries, $subject)
    {
        $this->from = $from;
        $this->destinataries = $destinataries;
        $this->subject = $subject;
    }

    /**
     * Add new destinataries to the current list.
     * @param $to String[]
     * @return Boolean
     */
    public function addTo($to)
    {
        if (!is_array($to)) {
            return false;
        }
        foreach ($to as $email) {
            $this->destinataries[] = $email;
        }
        return true;
    }

    /**
     * Sets the sender of the message.
     * @param $from String
     * @return Mailer
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * Sets the content of the message.
     * @param $content String
     * @param $isHtml Boolean
     * @return Mailer
     */
    public function setContent($content, $isHtml = true)
    {
        $this->useTemplate = false;
        $this->content = $content;
        $this->contentIsHtml = $isHtml;
        return $this;
    }

    /**
     * Sets the template of the message.
     * @param $template String
     * @param $data array[]
     * @return Mailer
     */
    public function setTemplate($template, $data)
    {
        $this->templateRender = TemplateFactory::make($template, $data);
        $this->data = $data;
        $this->useTemplate = true;
        $this->contentIsHtml = true;
        return $this;
    }

    /**
     * Sends the message.
     */
    public function send()
    {
        if ($this->useTemplate) {
            $this->content = $this->templateRender->render();
        }
        $contentType = 'text/html';
        if (!$this->contentIsHtml) {
            $contentType = 'text/plaintext';
        }
        \Mail::raw('', function ($message) use ($contentType) {
            $message->from($this->from);
            foreach ($this->destinataries as $email) {
                $message->cc($email, $email);
            }
            $message->subject($this->subject);
            $message->setBody($this->content, $contentType);
        });
    }
}
