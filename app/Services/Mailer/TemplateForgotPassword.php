<?php

namespace App\Services\Mailer;

/**
 * Representative class of a template for the forgot password mail.
 */
class TemplateForgotPassword implements InterfaceTemplate
{
    /**
     * @var array[]
     */
    private $data;

    /**
     * Creates a new template.
     * @param $data array[]
     */
    public function __construct($data)
    {
        if (!$this->validateData($data)) {
            throw new \Exception('MISSING PARAMETERS FOR THE FORGOT PASSWORD EMAIL TEMPLATE');
        }
        $this->data = $data;
    }

    /**
     * Get an html view.
     * @return String
     */
    public function render()
    {
        return BladeTemplate::render('emails.forgot', 'email.css', $this->data);
    }

    /**
     * Verifies that the given data if valid.
     * @param $data array[]
     * @return Boolean
     */
    public function validateData($data)
    {
        if (!isset($data['name']) || !$data['name'] || !$data['name'] != '') {
            return false;
        }
        if (!isset($data['email']) || !$data['email'] || !$data['email'] != '') {
            return false;
        }
        if (!isset($data['url']) || !$data['url'] || !$data['url'] != '') {
            return false;
        }
        return true;
    }
}
