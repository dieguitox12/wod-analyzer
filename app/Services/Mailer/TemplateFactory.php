<?php

namespace App\Services\Mailer;

/**
 * Representative class of a factory of templates.
 */
class TemplateFactory
{
    /**
     * Makes a new templates
     * @param $template String
     * @param $data array[]
     * @return InterfaceTemplate
     */
    public static function make($template, $data)
    {
        switch ($template) {
            case 'contact':
                return new TemplateContactInfo($data);
            case 'forgot-password':
                return new TemplateForgotPassword($data);
            default:
                throw new \Exception('TEMPLATE NOT IMPLEMENTED');
        }
    }
}
