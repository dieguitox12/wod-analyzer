<?php

namespace App\Services\Mailer;

use HTML;
use View;
use Mail;

/**
 * Representative class of a template made in Blade.
 */
class BladeTemplate
{
    /**
     * Makes and render an html view given the name of the blade view, the path of the css file and the data.
     * @param $bladeView String
     * @param $css String
     * @param $data array[]
     * @return String
     */
    public static function render($bladeView, $css, $data)
    {
        $myHTML = View::make($bladeView, $data)->render();
        $myCss = '';
        if (file_exists(public_path().'/css/'.$css)) {
            $myCss = file_get_contents(public_path().'/css/'.$css);
        }
        $emogrifier = new \Pelago\Emogrifier($myHTML, $myCss);
        return $emogrifier->emogrify();
    }
}
