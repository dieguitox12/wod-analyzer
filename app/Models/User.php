<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{

    /**
     * Generated
     */

    use SoftDeletes;

    protected $table = 'users';

    protected $fillable = ['id', 'name', 'email', 'birthdate', 'password', 'gender_id'];

    protected $dates = ['deleted_at'];


    public function gender()
    {
        return $this->belongsTo(Gender::class, 'gender_id', 'id');
    }

    public function userWods()
    {
        return $this->hasMany(UserWod::class, 'user_id', 'id');
    }

    public function wods()
    {
        return $this->belongsToMany(Wod::class, 'users_wods', 'user_id', 'wod_id');
    }
}
