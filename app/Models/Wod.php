<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wod extends Model
{

    /**
     * Generated
     */

    use SoftDeletes;

    protected $table = 'wods';

    protected $fillable = ['id', 'name', 'rounds', 'time_cap', 'created_by'];

    protected $dates = ['deleted_at'];


    public function movements()
    {
        return $this->belongsToMany(Movement::class, 'movements_wods', 'wod_id', 'movement_id');
    }

    public function movementsWods()
    {
        return $this->hasMany(MovementsWod::class, 'wod_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function usersWods()
    {
        return $this->hasMany(UserWod::class, 'wod_id', 'id');
    }
}
