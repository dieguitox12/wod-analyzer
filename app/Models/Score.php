<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Score extends Model
{

    /**
     * Generated
     */

    use SoftDeletes;

    protected $table = 'scores';

    protected $fillable = ['id', 'users_wods_id', 'wod_part', 'score'];

    protected $dates = ['deleted_at'];
}
