<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserWod extends Model
{

    /**
     * Generated
     */

    use SoftDeletes;

    protected $table = 'users_wods';

    protected $fillable = ['id', 'user_id', 'wod_id', 'lat', 'lon', 'temp', 'humidity', 'observations'];

    protected $dates = ['deleted_at'];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function wod()
    {
        return $this->belongsTo(Wod::class, 'wod_id', 'id');
    }

    public function scores()
    {
        return $this->hasMany(Score::class, 'users_wods_id', 'id');
    }
}
