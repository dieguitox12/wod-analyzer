<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movement extends Model
{

    /**
     * Generated
     */

    use SoftDeletes;

    protected $table = 'movements';

    protected $fillable = ['id', 'name', 'model'];

    protected $dates = ['deleted_at'];
}
