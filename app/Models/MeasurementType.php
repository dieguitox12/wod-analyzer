<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MeasurementType extends Model
{

    /**
     * Generated
     */

    use SoftDeletes;

    protected $table = 'measurementtypes';

    protected $fillable = ['id', 'name'];

    protected $dates = ['deleted_at'];
}
