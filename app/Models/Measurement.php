<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Measurement extends Model
{

    /**
     * Generated
     */

    use SoftDeletes;

    protected $table = 'measurements';

    protected $fillable = ['id', 'name', 'symbol', 'measurementtype_id'];

    protected $dates = ['deleted_at'];

    public function measurementType()
    {
        return $this->belongsTo(MeasurementType::class, 'measurementtype_id', 'id');
    }
}
