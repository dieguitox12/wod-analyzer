<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gender extends Model
{

    /**
     * Generated
     */
    use SoftDeletes;

    protected $table = 'genders';

    protected $fillable = ['id', 'name'];

    protected $dates = ['deleted_at'];
}
