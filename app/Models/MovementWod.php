<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MovementsWod extends Model
{

    /**
     * Generated
     */

    use SoftDeletes;

    protected $table = 'movements_wods';

    protected $fillable = [
        'id',
        'movement_id',
        'wod_id',
        'reps',
        'men_measurement_value',
        'men_measurement_id',
        'women_measurement_id',
        'women_measurement_value'
    ];

    protected $dates = ['deleted_at'];

    public function menMeasurement()
    {
        return $this->belongsTo(Measurement::class, 'men_measurement_id', 'id');
    }

    public function womenMeasurement()
    {
        return $this->belongsTo(Measurement::class, 'women_measurement_id', 'id');
    }

    public function movement()
    {
        return $this->belongsTo(Movement::class, 'movement_id', 'id');
    }

    public function wod()
    {
        return $this->belongsTo(Wod::class, 'wod_id', 'id');
    }
}
