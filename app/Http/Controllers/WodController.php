<?php

namespace App\Http\Controllers;

use Exception;
use Session;
use Auth;

use Illuminate\Http\Request;
use App\Validators\WodValidator;
use App\Managers\WodManager;
use App\Enums\IntervalMode;

/**
 * Representative class of a controller for the WODs
 */
class WodController extends AppController
{

    public function uploadVideo()
    {
        $video = $this->request->file('video');
        $wodId = $this->request->input('wodId');
        $interval = $this->request->input('interval');
        $perReps = $this->request->input('perReps');
        $perSeconds = $this->request->input('perSeconds', 30);

        $wod = WodManager::findById($wodId);
        if ($interval === null) {
            if ($wod->getRounds() > 1) {
                $interval = IntervalMode::PER_ROUNDS;
            } elseif ($wod->getRounds() !== null) {
                $interval = IntervalMode::PER_MOVEMENTS;
            } else {
                $interval = IntervalMode::PER_SECONDS;
                $perSeconds = $perSeconds;
            }
        }

        $scores = $wod->estimateScores($video, $interval, $perReps, $perSeconds);

        return $this->respondJSONSuccess(['scores' => $scores]);
    }

    public function optimize(int $wodScoreId)
    {
        $wodScored = WodManager::findByWodScoreId($wodScoreId);

        $sameResult = $wodScored->optimize(true);
        $betterResults = $wodScored->optimize();

        return $this->respondJSONSuccess([
            'same' => $sameResult->toArray(),
            'better' => $betterResults->toArray(),
        ]);
    }

    public function edit(int $wodScoreId)
    {
        $validator = new WodValidator($this->request);

        $wodScored = WodManager::findByWodScoreId($wodScoreId);

        $result = $validator->validateEditRequest($wodScored->getId());

        if ($result !== true) {
            return $this->respondJSON($result, 400);
        }

        $wodData = [
            'scores' => $this->request->input('scores'),
            'observations' => $this->request->input('observations'),
        ];

        if ($wodScored->getCreatedBy()) {
            if ($wodScored->getCreatedBy()->getId() === Auth::user()->getId()) {
                $wodData['name'] = $this->request->input('name');
                $wodData['rounds'] = $this->request->input('rounds', null);
                $wodData['timeCap'] = $this->request->input('timeCap', null);
                $wodData['movements'] = $this->request->input('movements');
            }
        }

        return $this->respondJSONSuccess($wodScored->edit($wodData)->toArray());
    }

    public function create()
    {
        $validator = new WodValidator($this->request);

        $result = $validator->validateCreateRequest();

        if ($result !== true) {
            return $this->respondJSON($result, 400);
        }

        $wodData = [
            'name' => $this->request->input('name'),
            'rounds' => $this->request->input('rounds', null),
            'timeCap' => $this->request->input('timeCap', null),
            'movements' => $this->request->input('movements'),
        ];

        $wod = WodManager::store($wodData);

        $scoreParams = [
          'id' => $wod->getId(),
          'scores' => $this->request->input('scores'),
          'observations' => $this->request->input('observations'),
          'lat' => $this->request->input('lat', null),
          'lon' => $this->request->input('lon', null),
        ];

        $newWodScore = $wod->score($scoreParams);

        return $this->respondJSONSuccess($newWodScore->toArray());
    }

    public function score()
    {
        $validator = new WodValidator($this->request);
        $result = $validator->validateScoreRequest();

        if ($result !== true) {
            return $this->respondJSON($result, 400);
        }

        $params = [
          'id' => $this->request->input('wodId'),
          'scores' => $this->request->input('scores'),
          'observations' => $this->request->input('observations'),
          'lat' => $this->request->input('lat', null),
          'lon' => $this->request->input('lon', null),
        ];

        $wod = WodManager::findById($params['id']);

        $newWodScore = $wod->score($params);

        return $this->respondJSONSuccess($newWodScore->toArray());
    }

    public function delete(int $id)
    {
        $wod = WodManager::findByWodScoreId($id);
        $wod->delete();

        return $this->respondJSONNoContent();
    }

    public function findByWodScoreId(int $id)
    {
        $wod = WodManager::findByWodScoreId($id);

        return $this->respondJSONSuccess($wod->toArray());
    }

    public function allWodsNames()
    {
        $wodList = WodManager::allWods();

        return $this->respondJSONSuccess($wodList);
    }

    public function allMovements()
    {
        $movementList = WodManager::allMovements();

        return $this->respondJSONSuccess($movementList);
    }

    public function search()
    {
        $validator = new WodValidator($this->request);
        $result = $validator->validateSearchRequest();

        if ($result !== true) {
            return $this->respondJSON($result, 400);
        }

        $searchArray = [
          'q' => $this->request->input('q', ''),
          'page' => $this->request->input('page'),
          'perPage' => $this->request->input('perPage', 10),
          'from' => $this->request->input('from'),
          'to' => $this->request->input('to')
        ];

        $withPagination = true;
        if (!$searchArray['page']) {
            $withPagination = false;
        }

        $wodList = WodManager::search($searchArray, $withPagination);

        return $this->respondJSONSuccess($wodList);
    }
}
