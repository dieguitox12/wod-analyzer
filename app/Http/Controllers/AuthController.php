<?php

namespace App\Http\Controllers;

use Exception;
use Session;
use Auth;

use Illuminate\Http\Request;
use App\Validators\AuthValidator;
use App\Managers\UserManager;

/**
 * Representative class of a controller for the authentication
 */
class AuthController extends Controller
{
    /**
     * Logs in the user into the system
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $validator = new AuthValidator($this->request);
        $result = $validator->validateLoginRequest();

        if ($result !== true) {
            return $this->respondJSON($result, 400);
        }

        // Get the user's data.

        $email = $this->request->input('email');
        $password = $this->request->input('password');
        $rememberUser = $this->request->input('rememberMe') === '1';

        $user = UserManager::findByEmail($email);

        $token = Auth::attempt([
            'email' => $email,
            'password' => $password
        ], $rememberUser);

        // If there's no token, then the login attempt
        // was not valid.

        if (!$token) {
            return $this->respondJSON([
                'error' => "Credentials don't match"
            ], 400);
        }

        $userData = $user->toArray();

        return $this->respondJSONSuccess($userData);
    }

    /**
     * Request to reset the password of a user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgotPassword()
    {
        $validator = new AuthValidator($this->request);
        $result = $validator->validateForgotPasswordRequest();

        if ($result !== true) {
            return $this->respondJSON($result, 400);
        }

        $email = $this->request->input('email');
        $user = UserManager::findByEmail($email);
        $emailJob = $user->forgotPassword();

        $this->dispatch($emailJob);

        return $this->respondJSONNoContent();
    }

    /**
     * Registers a user in the system.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register()
    {
        $validator = new AuthValidator($this->request);
        $result = $validator->validateCreateRequest();

        if ($result !== true) {
            return $this->respondJSON($result, 400);
        }

        $userData = [
          'email' => $this->request->input('email'),
          'password' => $this->request->input('password'),
          'name' => $this->request->input('name'),
          'birthdate' => $this->request->input('birthdate'),
          'gender' => [ 'id' => $this->request->input('gender') ]
        ];

        $user = UserManager::store($userData);

        // Logs the user.
        $token = Auth::attempt([
          'email' => $this->request->input('email'),
          'password' => $this->request->input('password')
        ]);

        $userArray = $user->toArray();

        return $this->respondJSON($userArray, 201);
    }

    /**
     * Resets the password of a user.
     *
     * @return \Illuminate\Http\Response
     */
    public function resetPassword()
    {
        $validator = new AuthValidator($this->request);
        $result = $validator->validateResetPasswordRequest();

        if ($result !== true) {
            return $this->respondJSON($result, 400);
        }

        $token = $this->request->input('token');
        $newPassword = $this->request->input('password');

        UserManager::resetPassword($token, $newPassword);

        return $this->respondJSONNoContent();
    }

    /**
     * Logs a user out the system.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        Auth::logout();
        Session::flush();
        return $this->respondJSONNoContent();
    }
}
