<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $request = null;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Reponds a view with an error message
     *
     * @param  String $view View
     * @param  String $message Message
     * @param  Array $data
     * @return \Illuminate\Http\Response
     */
    public function respondViewWithError($view, $message, $data = [], $status = 500)
    {
        Session::put('messages', ['message' => $message, 'type' => 'danger']);
        return response()->view($view, $data, $status);
    }

    /**
     * Reponds a view with a success message
     *
     * @param  String $view View
     * @param  String $message Message
     * @param  Array $data
     * @return \Illuminate\Http\Response
     */
    public function respondViewWithSuccess($view, $message, $data = [], $status = 200)
    {
        Session::put('messages', ['message' => $message, 'type' => 'success']);
        return response()->view($view, $data, $status);
    }

    /**
     * Responds a generic json
     *
     * @param $data
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondJSON($data, $status = 200)
    {
        $response = response()->json($data, $status);
        if ($this->request->input('callback')) {
            $response->setCallback($this->request->input('callback'));
        }
        return $response;
    }

    /**
     * Responds successfully with a json
     *
     * @param $data
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondJSONSuccess($data, $status = 200)
    {
        return $this->respondJSON($data, $status);
    }

    /**
     * Responds with error with a json
     *
     * @param $errorCode
     * @param $message
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondJSONError($message, $status = 500)
    {
        $data = [
            'error' => $message
        ];
        return $this->respondJSON($data, $status);
    }

    /**
     * Responds a json with a message
     *
     * @param $message
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondJSONWithMessage($message, $status)
    {
        $data = array('code' => $status, 'message' => $message);
        return $this->respondJSON($data, $status);
    }

    /**
     * Responds with redirection
     *
     * @param [type] $location
     * @param integer $status
     * @return void
     */
    public function respondWithRedirection($location, $status = 302)
    {
        return response()->redirectTo($location, $status);
    }

    /**
     * Responds successfully with no content
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondJSONNoContent()
    {
        return $this->respondJSON([], 204);
    }
}
