<?php

namespace App\Http\Controllers;

/**
 * Representative class of a controller for generic requests.
 */
class AppController extends Controller
{
    /**
     * Returns a not autorized response.
     */
    public function notAuthorized()
    {
        return $this->respondJSONError('You are not authorized to perform this action.', 401);
    }

    /**
     * Returns a view with the react HTML file.
     */
    public function reactRoute()
    {
        return view('welcome');
    }
}
