<?php

namespace App\Repositories;

use App\Entities\Wod;
use App\Entities\Score;
use App\Entities\WodScore;
use App\Entities\ScoredWod;
use App\Entities\Measurement;
use App\Entities\MeasurementType;
use App\Entities\Movement;
use App\Entities\WodMovement;

use App\Enums\ErrorCode;

use App\Models\UserWod as UserWodModel;
use App\Models\Wod as WodModel;
use App\Models\Movement as MovementModel;
use App\Models\Score as ScoreModel;
use App\Models\MovementsWod as WodMovementModel;

use Illuminate\Pagination\Paginator;

use Exception;
use Auth;
use Uuid;
use Cache;
use DB;
use Carbon\Carbon;

/**
 * Representative class of a Repository for wods
 */
class WodRepository
{
    public static function allWods() : array
    {
        return WodModel::with(self::getBasicWodRelationship())
                        ->whereNull('created_by')
                        ->orWhere('created_by', Auth::user()->getId())
                        ->get()
                        ->map(function ($wod) {
                            return self::entityBuilder(['wod' => $wod->toArray()], false)->toArray(true);
                        })
                        ->toArray();
    }

    public static function allMovements() : array
    {
        return MovementModel::all()
                            ->map(function ($movement) {
                                return (new Movement($movement->toArray()))->toArray();
                            })
                            ->toArray();
    }

    public static function getBasicWodRelationship() : array
    {
        $relations = [
          'movementsWods.movement',
          'movementsWods.menMeasurement.measurementType',
          'movementsWods.womenMeasurement.measurementType',
          'createdBy.gender'
        ];

        return $relations;
    }

    public static function getBasicRelationship() : array
    {
        $relations = [
          'scores',
          'wod.createdBy.gender',
          'wod.movementsWods.movement',
          'wod.movementsWods.womenMeasurement.measurementType',
          'wod.movementsWods.menMeasurement.measurementType'
        ];

        return $relations;
    }

    public static function search(array $searchArray, bool $withPagination = true) : array
    {
        if ($withPagination) {
            Paginator::currentPageResolver(
                function () use ($searchArray) {
                    return $searchArray['page'];
                }
            );
        }

        $userId = Auth::user()->getId();

        $relations = self::getBasicRelationship();
        $query = UserWodModel::with($relations)
                             ->where('user_id', $userId);

        ///// BEGIN SEARCH PARAMETER FILTERS

        if (isset($searchArray['q'])) {
            $query->whereHas('wod', function ($query2) use ($searchArray) {
                $query2->where('name', 'like', "%{$searchArray['q']}%");
            });
        }

        if (isset($searchArray['from'])) {
            $query->where('created_at', '>=', $searchArray['from']);
        }

        if (isset($searchArray['to'])) {
            $query->where('created_at', '<=', $searchArray['to']);
        }

        ///// END SEARCH PARAMETER FILTERS

        $query->orderBy('id', 'DESC');

        if ($withPagination) {
            $eloquentListWods = $query->paginate($searchArray['perPage']);
        }

        $results = $query->distinct('id')->get()->toArray();
        $listWods = [];

        foreach ($results as $wod) {
            $listWods[] = self::entityBuilder($wod);
        }
        if ($withPagination) {
            $listWods['pagination'] = [
                'total' => $eloquentListWods->total(),
                'perPage' => $eloquentListWods->perPage(),
                'hasMorePages' => $eloquentListWods->hasMorePages(),
                'currentPage' => $eloquentListWods->currentPage(),
                'count' => $eloquentListWods->count()
            ];
        }
        return $listWods;
    }

    public static function findByWodScoreId(int $id) : ScoredWod
    {
        $relations = self::getBasicRelationship();
        $wodDB = UserWodModel::with($relations)
                             ->find($id);


        if (!$wodDB) {
            throw new Exception('Wod not found', ErrorCode::NOT_FOUND);
        }
        $previousScores = UserWodModel::with($relations)
                                      ->where('wod_id', $wodDB->wod_id)
                                      ->where('id', '!=', $wodDB->id)
                                      ->has('scores', '=', $wodDB->scores->count())
                                      ->get()
                                      ->toArray();

        $listWods = [];

        foreach ($previousScores as $wod) {
            $listWods[] = self::entityBuilder($wod);
        }

        $newWod = self::entityBuilder($wodDB->toArray());
        $newWod->setPreviousScores($listWods);

        return $newWod;
    }

    public static function findSimpleWodById(int $id) : Wod
    {
        $relations = self::getBasicWodRelationship();
        $wodDB = WodModel::with($relations)
                         ->find($id);
        if (!$wodDB) {
            throw new Exception('Wod not found', ErrorCode::NOT_FOUND);
        }
        return self::entityBuilder(['wod' => $wodDB->toArray()], false);
    }

    public static function findById(int $id) : Wod
    {
        $relations = self::getBasicRelationship();
        $wodDB = UserWodModel::with($relations)
                             ->where('wod_id', $id)
                             ->orderBy('id', 'DESC')
                             ->first();
        if (!$wodDB) {
            throw new Exception('Wod not found', ErrorCode::NOT_FOUND);
        }
        return self::entityBuilder($wodDB->toArray(), false);
    }

    public static function delete(ScoredWod $wod) : void
    {
        DB::transaction(function () use ($wod) {
            $wodModel = UserWodModel::find($wod->getScore()->getId());
            if (!$wodModel) {
                throw new Exception('Wod not found', ErrorCode::NOT_FOUND);
            }
            if (!$wodModel->delete()) {
                throw new Exception('Error deleting the score. Try again later.');
            }
        });
    }

    public static function score(int $wodId, WodScore $newScore) : WodScore
    {
        return DB::transaction(function () use ($wodId, $newScore) {
            $userWodModel = new UserWodModel();

            if ($newScore->getId()) {
                $userWodModel = UserWodModel::find($newScore->getId());
            }

            $userWodModel->user_id = Auth::user()->getId();
            $userWodModel->wod_id = $wodId;
            $userWodModel->observations = $newScore->getObservations();

            if (!$userWodModel->save()) {
                throw new Exception('Error storing the new score.');
            }

            $newScoresArray = [];

            ScoreModel::where('users_wods_id', $newScore->getId())->delete();

            foreach ($newScore->getScores() as $score) {
                $scoreModel = new ScoreModel();

                $scoreModel->users_wods_id = $userWodModel->id;
                $scoreModel->wod_part = $score->getPart();
                $scoreModel->score = $score->getScore();

                if (!$scoreModel->save()) {
                    throw new Exception('Error storing the new score.');
                }
                $score->setId($scoreModel->id);
                $score->setCreatedAt($scoreModel->created_at);
                $score->setUpdatedAt($scoreModel->updated_at);
                $newScoresArray[] = $score;
            }

            $newScore->setId($userWodModel->id);
            $newScore->setScores($newScoresArray);
            $newScore->setCreatedAt($userWodModel->created_at);
            $newScore->setUpdatedAt($userWodModel->updated_at);

            return $newScore;
        });
    }

    public static function save(Wod $wod, int $wodScoreId = null) : Wod
    {
        return DB::transaction(function () use ($wod, $wodScoreId) {
            $wodModel = new WodModel();

            if ($wod->getId()) {
                $wodModel = WodModel::find($wod->getId());
            } else {
                $wodModel->created_by = Auth::user()->getId();
            }

            $wodModel->name = $wod->getName();
            $wodModel->rounds = $wod->getRounds();
            $wodModel->time_cap = $wod->getTimeCap();

            if (!$wodModel->save()) {
                throw new Exception('Error storing the wod.');
            }

            WodMovementModel::where('wod_id', $wod->getId())->delete();

            foreach ($wod->getMovements() as $movement) {
                $wodMovementModel = new WodMovementModel();

                $wodMovementModel->wod_id = $wodModel->id;
                $wodMovementModel->movement_id = $movement->getMovement()->getId();
                $wodMovementModel->reps = $movement->getReps();
                $wodMovementModel->men_measurement_value =
                    $movement->getManMeasure() ? $movement->getManMeasure()->getValue() : null;
                $wodMovementModel->women_measurement_value =
                    $movement->getWomanMeasure() ? $movement->getWomanMeasure()->getValue() : null;
                $wodMovementModel->men_measurement_id =
                    $movement->getManMeasure() ? $movement->getManMeasure()->getId() : null;
                $wodMovementModel->women_measurement_id =
                    $movement->getWomanMeasure() ? $movement->getWomanMeasure()->getId() : null;

                if (!$wodMovementModel->save()) {
                    throw new Exception('Error storing the wod.');
                }
            }

            if ($wodScoreId) {
                return self::findByWodScoreId($wodScoreId);
            }
            return self::findSimpleWodById($wodModel->id);
        });
    }

    public static function entityBuilder(array $data, bool $withScore = true) : Wod
    {
        $wodScore = null;
        if ($withScore) {
            $wodScores = [];
            $individualScores = collect($data['scores'])->map(function ($element) {
                $element['part'] = $element['wod_part'];
                return new Score($element);
            })->toArray();

            $data['scores'] = $individualScores;

            $wodScore = new WodScore($data);
        }
        $movements = collect($data['wod']['movements_wods'])->map(function ($element) {
            $individualMovement = new Movement($element['movement']);
            $manMeasurement = null;
            $womanMeasurement = null;

            if (isset($element['men_measurement'])) {
                $measurementType = new MeasurementType($element['men_measurement']['measurement_type']);
                $manMeasurementData = $element['men_measurement'];
                $manMeasurementData['value'] = $element['men_measurement_value'];
                $manMeasurementData['measurementType'] = $measurementType;

                $manMeasurement = new Measurement($manMeasurementData);
            }

            if (isset($element['women_measurement'])) {
                $measurementType = new MeasurementType($element['women_measurement']['measurement_type']);

                $womanMeasurementData = $element['women_measurement'];
                $womanMeasurementData['value'] = $element['women_measurement_value'];
                $womanMeasurementData['measurementType'] = $measurementType;

                $womanMeasurement = new Measurement($womanMeasurementData);
            }

            $element['movement'] = $individualMovement;
            $element['manMeasure'] = $manMeasurement;
            $element['womanMeasure'] = $womanMeasurement;

            return new WodMovement($element);
        })->toArray();

        $data['wod']['movements'] = $movements;
        $data['wod']['timeCap'] = $data['wod']['time_cap'];
        $data['wod']['score'] = $wodScore;
        $data['wod']['createdBy'] = null;

        if (isset($data['wod']['created_by'])) {
            $data['wod']['createdBy'] = UserRepository::entityBuilder($data['wod']['created_by']);
        }

        $newWod = new Wod($data['wod']);

        if ($withScore) {
            $newWod = new ScoredWod($data['wod']);
        }
        return $newWod;
    }
}
