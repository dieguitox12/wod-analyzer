<?php

namespace App\Repositories;

use App\Entities\User;

use App\Models\User as UserModel;

use Illuminate\Pagination\Paginator;

use Exception;
use Auth;
use Uuid;
use Cache;
use DB;
use Carbon\Carbon;

/**
 * Representative class of a Repository for users
 */
class UserRepository
{
    public static function storeForgotPasswordToken(User $user) : string
    {
        $token = Uuid::generate(4);
        Cache::add($token->string, $user, 60);
        return $token->string;
    }

    public static function deleteForgotPasswordToken(string $token) : ?User
    {
        $user = Cache::pull($token);

        if (!$user) {
            return null;
        }
        return $user;
    }

    public static function findByEmail(string $email, bool $loginAttempt = false) : User
    {
        $relations = [
            'gender'
        ];

        $query = UserModel::with($relations);

        $userDB = $query->where('email', $email)->first();

        if (!$userDB) {
            return null;
        }

        return self::entityBuilder($userDB->toArray());
    }

    public static function findById(int $id) : User
    {
        $userDB = UserModel::with(['gender'])
                           ->findOrFail($id);
        return self::entityBuilder($userDB->toArray());
    }

    public static function getByToken(int $id, string $token) : ?User
    {
        $user = UserModel::where('id', $id)->where('remember_token', $token)->first();

        if ($user == null) {
            return $user;
        }

        return self::entityBuilder($user->toArray());
    }

    public static function updateToken(int $id, string $token) : void
    {
        UserModel::where('id', $id)->update(['remember_token' => $token]);
    }

    public static function save(User $user) : User
    {
        return DB::transaction(function () use ($user) {
            $userModel = new UserModel();

            if ($user->getId()) {
                $userModel = UserModel::find($user->getId());
            }

            $userModel->email = $user->getEmail();
            $userModel->gender_id = ($user->getGender())['id'];
            $userModel->password = $user->getPassword();
            $userModel->name = $user->getName();
            $userModel->birthdate = $user->getBirthdate();

            if (!$userModel->save()) {
                throw new Exception('Error storing the user.');
            }

            $user->setId($userModel->id);

            return self::findById($userModel->id);
        });
    }

    public static function entityBuilder(array $data) : User
    {
        $newUser = new User($data);

        return $newUser;
    }
}
