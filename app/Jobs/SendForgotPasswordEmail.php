<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\Mailer\Mailer;

class SendForgotPasswordEmail extends Mailer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $url, $name, $subject)
    {
        parent::__construct(
            config('mail.from.address', 'info@wodanalyzer.com'),
            [$email],
            $subject
        );
        $this->setTemplate(
            'forgot-password',
            [
                'name' => $name,
                'email' => $email,
                'url' => $url
            ]
        );
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->send();
    }
}
