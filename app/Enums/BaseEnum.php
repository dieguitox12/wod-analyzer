<?php

namespace App\Enums;

use MyCLabs\Enum\Enum;

/**
 * Representative enum class of a base enum
 */
class BaseEnum extends Enum
{
    /**
     * Gets the enum as an array
     *
     * @return array
     */
    public function toShortArray()
    {
        return [
            'key' => $this->getKey(),
            'value' => $this->getValue()
        ];
    }
}
