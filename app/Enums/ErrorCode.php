<?php

namespace App\Enums;

/**
 * Representative enum class of days of the week
 */
class ErrorCode extends BaseEnum
{
    const REQUIRED = 1;
    const BAD_FORMAT = 2;
    const ALREADY_TAKEN = 3;
    const EXPIRED = 4;
    const ILLEGAL = 5;
    const INVALID_STATE = 6;
    const NOT_PRESENT = 7;
    const CUSTOM_EXCEPTION = 8;
    const NOT_FOUND = 404;
}
