<?php

namespace App\Enums;

class IntervalMode extends BaseEnum
{
    const PER_ROUNDS = 1;
    const PER_MOVEMENTS = 2;
    const PER_REPS = 3;
    const PER_SECONDS = 4;
}
