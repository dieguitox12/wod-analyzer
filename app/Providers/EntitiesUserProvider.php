<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;

use Hash;
use Exception;
use App\Managers\UserManager;

class EntitiesUserProvider implements UserProvider
{

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $id
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($id)
    {
        // It could be whether from session or from database
        return $this->getUser(['id' => $id]);
    }

    /**
     * Retrieve a user by the given credentials.
     * DO NOT TEST PASSWORD HERE!
     *
     * @param  array $credentials
     * @return Authenticatable|null
     * @throws Exception
     */
    public function retrieveByCredentials(array $credentials)
    {
        // Here goes the differente ways to retrieve a user given the credentials
        if (isset($credentials['email']) || isset($credentials['id'])) {
            return $this->getUser($credentials);
        }
        throw new Exception("Not implemented for other credentials", 500);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     * @return bool
     * @throws Exception
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        if (!isset($credentials['password'])) {
            throw new Exception("Password credential is required", 500);
        }

        $plain = $credentials['password'];
        return Hash::check($plain, $user->getAuthPassword());
    }

    /**
     * Needed by Laravel 4.1.26 and above
     */
    public function retrieveByToken($identifier, $token)
    {
        return UserManager::getByToken($identifier, $token);
    }

    /**
     * Needed by Laravel 4.1.26 and above
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        UserManager::updateToken($user->getAuthIdentifier(), $token);
    }

    /**
     * Gets the user from the session or
     * from DB
     * @param array $credentials
     * @return User|null
     */
    private function getUser(array $credentials)
    {
        $user = null;

        if (isset($credentials['id'])) {
            $user = UserManager::findById($credentials['id']);
        } elseif (isset($credentials['email'])) {
            $user = UserManager::findByEmail($credentials['email'], true);
        }

        return $user;
    }
}
