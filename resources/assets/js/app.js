import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { store } from 'common/store';
import { authUserSuccess } from 'common/store/actions/user/user.actions';
import addFormsyRules from 'common/helpers/formsy-rules';

import Root from './containers';

/*global user*/
// localStorage.clear();

addFormsyRules();

////

function fastFilter(array, fn, thisArg) {
    var result = [],
        test = (thisArg === undefined ? fn : function (a, b, c) {
            return fn.call(thisArg, a, b, c);
        }),
        i, len;
    for (i = 0, len = array.length; i < len; i++) {
        if (test(array[i], i, array)) { result.push(array[i]); }
    }
    return result;
}

fastFilter.install = function (name) {
    Array.prototype[name || 'fastFilter'] = function (fn, thisArg) {
        return fastFilter(this, fn, thisArg);
    };

    return fastFilter;
};

fastFilter.install('fastFilter');

////

if (typeof user !== 'undefined') {
    store.dispatch(authUserSuccess(user));
}


store.subscribe(() => {
    if (store.getState().genericUIReducer.loading) {
        document.getElementById('html').classList.add('wait');
    } else {
        document.getElementById('html').classList.remove('wait');
    }
});

ReactDOM.render(
    <Provider store={store}>
        <Root />
    </Provider>,
    document.getElementById('root')
);
