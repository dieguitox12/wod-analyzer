'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';

import { createUser } from 'common/store/actions/user/user.actions';
import { getAuthUser, getUserReducerError } from 'common/store/selectors/user.selector';

import RegisterForm from './register-form';

function mapDispatchToProps(dispatch) {
    return {
        registerUser: (data) => {
            dispatch(createUser(data));
        }
    };
}

const mapStateToProps = (state) => {
    return {
        authUser: getAuthUser(state),
        error: getUserReducerError(state)
    };
};

/**
 * Container which handles the register logic.
 */
@connect(mapStateToProps, mapDispatchToProps)
export default class RegisterContainer extends React.Component {
    /**
     * Set the states and makes the binds as needed.
     */
    constructor(props) {
        super(props);

        this.onRegister = this.onRegister.bind(this);
        this.onFormChange = this.onFormChange.bind(this);

        this.state = {
            email: '',
            password: '',
            passwordConfirmation: '',
            name: '',
            birthdate: moment().format('MM/DD/YYYY'),
            gender: null,
            error: ''
        };
    }

    componentWillReceiveProps(newProps) {
        if (newProps.authUser) {
            this.history.push('/dashboard');
        }
        this.setState({error: newProps.error});
    }

    /**
     * Fired once the email or the password changes.
     */
    onFormChange(field, value) {
        this.setState({
            [field]: value,
            error: ''
        });
    }

    /**
     * Fired once the user attempts to log in.
     */
    onRegister() {
        let data = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password,
            passwordConfirmation: this.state.passwordConfirmation,
            birthdate: this.state.birthdate,
            gender: this.state.gender
        };
        this.props.registerUser(data);
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <RegisterForm
                onRegister={this.onRegister}
                email={this.state.email}
                name={this.state.name}
                birthdate={this.state.birthdate}
                gender={this.state.gender}
                password={this.state.password}
                passwordConfirmation={this.state.passwordConfirmation}
                error={this.state.error}
                onFormChange={this.onFormChange}
            />
        );
    }
}

/**
  * Container props validation.
  */
RegisterContainer.WrappedComponent.propTypes = { registerUser: PropTypes.func.isRequired };
