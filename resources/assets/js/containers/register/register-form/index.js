import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Formsy from 'formsy-react';
import moment from 'moment';

import {
    TextInput,
    PasswordInput,
    FormsyInputWrapper,
    DatetimePicker,
    Select
} from 'common/components/inputs';
import { GENDERS, GENDERS_LABELS } from 'common/constants/enums.constant';

export default class RegisterForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = { canSubmit: false };

        this.changeEmail = this.changeField.bind(this, 'email');
        this.changePassword = this.changeField.bind(this, 'password');
        this.changePasswordConfirmation = this.changeField.bind(this, 'passwordConfirmation');
        this.changeaName = this.changeField.bind(this, 'name');
        this.changeBirthdate = this.changeField.bind(this, 'birthdate');
        this.changeGender = this.changeField.bind(this, 'gender');

        this.enableSubmit = this.canSubmit.bind(this, true);
        this.disableSubmit = this.canSubmit.bind(this, false);
    }

    changeField(field, value) {
        this.props.onFormChange(field, value);
    }

    canSubmit(canSubmit) {
        this.setState({canSubmit});
    }

    render() {
        return (
            <Formsy
                onValidSubmit={this.props.onRegister}
                onValid={this.enableSubmit}
                onInvalid={this.disableSubmit}>
                <div className="col-12 pd0">
                    <FormsyInputWrapper
                        required
                        name="email"
                        value={this.props.email}
                        validations="isEmail"
                        validationError="Enter a valid email address">
                        <TextInput
                            label="Email"
                            value={this.props.email}
                            onChange={this.changeEmail}
                        />
                    </FormsyInputWrapper>
                </div>
                <div className="col-12 pd0">
                    <FormsyInputWrapper
                        required
                        name="password"
                        value={this.props.password}>
                        <PasswordInput
                            label="Password"
                            value={this.props.password}
                            onChange={this.changePassword}
                        />
                    </FormsyInputWrapper>
                </div>
                <div className="col-12 pd0">
                    <FormsyInputWrapper
                        required
                        name="passwordConfirmation"
                        value={this.props.passwordConfirmation === this.props.password}
                        validations="isTrue"
                        validationError="Passwords must match">
                        <PasswordInput
                            value={this.props.passwordConfirmation}
                            onChange={this.changePasswordConfirmation}
                            label="Repeat password" />
                    </FormsyInputWrapper>
                </div>
                <div className="col-12 pd0">
                    <FormsyInputWrapper
                        required
                        name="name"
                        value={this.props.name}>
                        <TextInput
                            label="Name"
                            value={this.props.name}
                            onChange={this.changeaName}
                        />
                    </FormsyInputWrapper>
                </div>
                <div className="col-12 pd0">
                    <FormsyInputWrapper
                        name="birthdate"
                        validations="isTrue"
                        value={moment(this.props.birthdate).isValid()}>
                        <DatetimePicker
                            label="Birthdate"
                            floating={true}
                            className="full-width"
                            onChange={this.changeBirthdate}
                            value={this.props.birthdate}
                        />
                    </FormsyInputWrapper>
                </div>
                <div className="col-12 pd0">
                    <FormsyInputWrapper
                        name="gender"
                        value={this.props.gender > 0}
                        validations="isTrue">
                        <Select
                            value={this.props.gender}
                            floatingLabel="Género"
                            searchable={false}
                            options={[
                                {id: GENDERS.MALE, name: GENDERS_LABELS[GENDERS.MALE]},
                                {id: GENDERS.FEMALE, name: GENDERS_LABELS[GENDERS.FEMALE]}
                            ]}
                            onChange={this.changeGender}
                        />
                    </FormsyInputWrapper>
                </div>
                {this.props.error && typeof this.props.error === 'string' &&
                    <div className='has-error'>
                        <span className='help-block'>{this.props.error}</span>
                    </div>
                }
                {this.props.error && typeof this.props.error === 'object' &&
                    <div className='has-error'>
                        <span className='help-block'>{this.props.error.email.message}</span>
                    </div>
                }
                <div className="mgT5 clearfix" />
                <div className="row">
                    <div className="col-6 text-center">
                        <Link to="/login">
                            <button type="button" className="btn btn-outline">Back</button>
                        </Link>
                    </div>
                    <div className="col-6 text-center">
                        <button
                            type="submit"
                            className="btn btn-primary"
                            disabled={!this.state.canSubmit}>
                            Sign Up
                        </button>
                    </div>
                </div>
            </Formsy>
        );
    }
}

RegisterForm.propTypes = {
    email: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    birthdate: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired,
    passwordConfirmation: PropTypes.string.isRequired,
    onRegister: PropTypes.func.isRequired,
    onFormChange: PropTypes.func
};
