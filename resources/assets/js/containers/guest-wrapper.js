import React from 'react';
import PropTypes from 'prop-types';

/**
* Container which warps the authentication screens.
*/
export default class GuestWrapper extends React.Component {

    /**
    * Renders the element.
    *
    * @return {ReactComponent}
    */
    render() {
        if (window.location.pathname.split('/')[1] === 'welcome') {
            return (
                <div className="login">
                    <div className="welcome__content">
                        <div className="welcome__body">
                            {this.props.children}
                        </div>
                    </div>
                </div>
            );
        }
        return (
            <div className="login">
                <div className="login__content">
                    <div className="login__body">
                        <div className="login__logo" />
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}

GuestWrapper.propTypes = { children: PropTypes.node.isRequired };
