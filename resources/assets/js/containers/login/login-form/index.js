import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Formsy from 'formsy-react';

import {
    TextInput,
    PasswordInput,
    FormsyInputWrapper
} from 'common/components/inputs';

import { USER_ICON, PASSWORD_ICON } from 'common/constants/icon.constant';

/**
 * Component responsible of rendering the login form.
 */
export default class LoginForm extends React.Component {
    /**
     * Initializes the form.
     */
    constructor(props) {
        super(props);

        this.state = { canSubmit: false };

        this.changeEmail = this.changeField.bind(this, 'email');
        this.changePassword = this.changeField.bind(this, 'password');

        this.enableSubmit = this.canSubmit.bind(this, true);
        this.disableSubmit = this.canSubmit.bind(this, false);
    }

    componentWillReceiveProps(newProps) {
        if (newProps.error) {
            this.disableSubmit();
        } else {
            this.enableSubmit();
        }
    }

    /**
     * Fired once any form input changes.
     */
    changeField(field, value) {
        this.props.onFormChange(field, value);
    }

    /**
     * Fired after formsy validations
     */
    canSubmit(canSubmit) {
        this.setState({canSubmit});
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <Formsy
                onValidSubmit={this.props.onLogin}
                onValid={this.enableSubmit}
                onInvalid={this.disableSubmit}>
                <div className="col-12 pd0">
                    <FormsyInputWrapper
                        required
                        name="email"
                        value={this.props.email}
                        validations="isEmail"
                        validationError="Enter a valid email address">
                        <TextInput
                            icon={USER_ICON}
                            label="Email"
                            value={this.props.email}
                            onChange={this.changeEmail}
                        />
                    </FormsyInputWrapper>
                </div>
                <div className="col-12 pd0">
                    <FormsyInputWrapper
                        required
                        name="password"
                        value={this.props.password}>
                        <PasswordInput
                            icon={PASSWORD_ICON}
                            label="Password"
                            value={this.props.password}
                            onChange={this.changePassword}
                        />
                    </FormsyInputWrapper>
                </div>
                {this.props.error && typeof this.props.error === 'string' &&
                    <div className='has-error'>
                        <span className='help-block'>{this.props.error}</span>
                    </div>
                }
                {this.props.error && typeof this.props.error === 'object' &&
                    <div className='has-error'>
                        <span className='help-block'>{this.props.error.email.message}</span>
                    </div>
                }
                <div className="d-flex justify-content-between">
                    <div>
                        <Link to="/forgot-password">Forgot password?</Link>
                    </div>
                    <div>
                        <Link to="/register">Sign Up</Link>
                    </div>
                </div>


                <div className="mgT5 clearfix" />
                <div className="row">
                    <div className="col-12 text-center">
                        <button
                            type="submit"
                            className="btn btn-primary"
                            disabled={!this.state.canSubmit}>
                            Sign In
                        </button>
                    </div>
                </div>
            </Formsy>
        );
    }
}

/**
 * Login form properties validation.
 */
LoginForm.propTypes = {
    email: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired,
    onLogin: PropTypes.func.isRequired,
    onFormChange: PropTypes.func
};
