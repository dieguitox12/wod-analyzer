'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { authUser } from 'common/store/actions/user/user.actions';
import { getAuthUser, getUserReducerError } from 'common/store/selectors/user.selector';

import LoginForm from './login-form';

/**
 * Connect the login container to the redux store,
 * so we can dispatch a logUser event.
 */
function mapDispatchToProps(dispatch) {
    return {
        logUser: (email, password) => {
            dispatch(authUser(email, password));
        }
    };
}

const mapStateToProps = (state) => {
    return {
        authUser: getAuthUser(state),
        error: getUserReducerError(state)
    };
};

/**
 * Container which handles the login logic.
 */
@connect(mapStateToProps, mapDispatchToProps)
export default class LoginContainer extends React.Component {
    /**
     * Set the states and makes the binds as needed.
     */
    constructor(props) {
        super(props);

        this.onLogin = this.onLogin.bind(this);
        this.onFormChange = this.onFormChange.bind(this);

        this.state = {
            email: '',
            password: '',
            error: ''
        };
    }

    componentWillReceiveProps(newProps) {
        if (newProps.authUser) {
            this.history.push('/dashboard');
        }
        this.setState({error: newProps.error});
    }

    /**
     * Fired once the email or the password changes.
     */
    onFormChange(field, value) {
        this.setState({
            [field]: value,
            error: ''
        });
    }

    /**
     * Fired once the user attempts to log in.
     */
    onLogin() {
        const email = this.state.email;
        const password = this.state.password;

        this.props.logUser(email, password);
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <LoginForm
                onLogin={this.onLogin}
                email={this.state.email}
                password={this.state.password}
                error={this.state.error}
                onFormChange={this.onFormChange}
            />
        );
    }
}

/**
  * Container props validation.
  */
LoginContainer.WrappedComponent.propTypes = { logUser: PropTypes.func.isRequired };
