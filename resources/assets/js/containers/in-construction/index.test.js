import React from 'react';
import renderer from 'react-test-renderer';

import InConstructionContainer from './';

describe('InConstruction Container', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <InConstructionContainer />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

