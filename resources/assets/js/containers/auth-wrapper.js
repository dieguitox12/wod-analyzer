'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Media from 'react-media';

import {
    logoutUser
} from 'common/store/actions/user/user.actions';
import * as wodActions from 'common/store/actions/wod/wod.actions';
import * as wodSelectors from 'common/store/selectors/wod.selector';

import {
    Header,
    Sidebar
} from 'common/components/layouts';

/**
 * Allows the auth wrapper to dispatch events to the
 * cart reducer.
 *
 * @param {function} dispatch  the Redux dispatcher.
 */
const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => {
            dispatch(logoutUser());
        },
        setFilters: (filters) => {
            dispatch(wodActions.setFilters(filters));
        }
    };
};

const mapStateToProps = (state) => {
    return {filters: wodSelectors.getFilters(state)};
};


/**
 * Container which warps the authentication screens.
 */
@withRouter
@connect(mapStateToProps, mapDispatchToProps)
export default class AuthWrapper extends React.Component {

    constructor(props) {
        super(props);

        this.pathsWithSidebar = [
            '/scores',
            '/scores/',
            '/'
        ];

        this.onSearchQueryChange = this.onFilterChange.bind(this, 'q');
        this.onFromChange = this.onFilterChange.bind(this, 'from');
        this.onToChange = this.onFilterChange.bind(this, 'to');
    }

    onFilterChange(field, value) {
        let filters = Object.assign({}, this.props.filters);
        filters[field] = value;
        this.props.setFilters(filters);
    }

    render() {
        let withSidebar = this.pathsWithSidebar.find(element => {
            return this.props.location.pathname === element;
        }) !== undefined;

        return (
            <div className={`dashboard layout full-height`}>
                <Media query='(max-width: 999px)'>
                    {matches =>
                        matches ? (
                            <main className={`layout-main full-height layout-main__responsive`}>{this.props.children}</main>
                        ) : (
                            <div>
                                { withSidebar &&
                                    <Sidebar
                                        onSearchQueryChange={this.onSearchQueryChange}
                                        onFromChange={this.onFromChange}
                                        onToChange={this.onToChange}
                                        filters={this.props.filters}
                                        history={this.props.history}
                                        location={this.props.location.pathname} />
                                }
                                <main className={`layout-main full-height ${withSidebar ? '' : 'layout-main__no-sidebar'}`}>{this.props.children}</main>
                            </div>
                        )
                    }
                </Media>
                <Header
                    user={this.props.user}
                    onLogout={this.props.logout}
                    pathname={this.props.location.pathname}
                />

            </div>
        );
    }
}

AuthWrapper.WrappedComponent.propTypes = {
    user: PropTypes.object.isRequired,
    children: PropTypes.node.isRequired,
    location: PropTypes.shape({ pathname: PropTypes.string }),
    history: PropTypes.shape({ push: PropTypes.func })
};
