import React from 'react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';

import Root from './';

window.matchMedia = () => ({
    addListener: () => {},
    removeListener: () => {}
});

describe('Root', () => {
    const mockStore = configureStore();

    let store = mockStore({ userReducer: {}, wodReducer: {} });

    test('renders without crashing', () => {
        const tree = renderer
            .create(
                <Provider store={store}>
                    <Root />
                </Provider>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    test('renders without crashing and having the user logged in', () => {
        let store = mockStore({ userReducer: { authUser: { id: 12345, name: 'DIEGO', 'auth_token': 'token', gender: {id: 1} }, notifications: []}, wodReducer: {scores: [], filters: {}} });
        const tree = renderer
            .create(
                <Provider store={store}>
                    <Root />
                </Provider>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

