'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { forgotPassword } from 'common/store/actions/user/user.actions';
import {
    getAuthUser,
    getUserReducerError,
    getUserReducerEmailSent
} from 'common/store/selectors/user.selector';

import ForgotPasswordForm from './forgot-password-form';

function mapDispatchToProps(dispatch) {
    return {
        forgotPassword: (email) => {
            dispatch(forgotPassword(email));
        }
    };
}

const mapStateToProps = (state) => {
    return {
        authUser: getAuthUser(state),
        error: getUserReducerError(state),
        emailSent: getUserReducerEmailSent(state)
    };
};

/**
 * Container which handles the forgot password logic.
 */
@connect(mapStateToProps, mapDispatchToProps)
export default class ForgotPasswordContainer extends React.Component {
    /**
     * Initializes the container.
     */
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            error: '',
            emailSent: false
        };

        this.onSend = this.onSend.bind(this);
        this.changeEmail = this.changeEmail.bind(this);
    }

    componentWillReceiveProps(newProps) {
        this.setState({ emailSent: newProps.emailSent, error: newProps.error });
    }

    /**
     * Fired once the email has changed.
     */
    changeEmail(email) {
        this.setState({ email, error: '', emailSent: false });
    }

    /**
     * Fired once the send button has been clicked.
     */
    onSend() {
        const email = this.state.email;

        this.props.forgotPassword(email);
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <ForgotPasswordForm
                email={this.state.email}
                emailError={this.state.error}
                emailSent={this.state.emailSent}
                onSend={this.onSend}
                onEmailChange={this.changeEmail}
            />
        );
    }
}

ForgotPasswordContainer.WrappedComponent.propTypes = { forgotPassword: PropTypes.func.isRequired };
