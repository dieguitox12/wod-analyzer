import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';

import { TextInput, FormsyInputWrapper } from 'common/components/inputs';


/**
* Component responsible of the forgot password form.
*/
export default class ForgotPasswordForm extends React.Component {
    /**
  * Initializes the form.
  */
    constructor(props) {
        super(props);

        this.state = { canSubmit: false };

        this.enableSubmit = this.canSubmit.bind(this, true);
        this.disableSubmit = this.canSubmit.bind(this, false);
    }

    /**
  * Fired after formsy validations
  */
    canSubmit(canSubmit) {
        this.setState({ canSubmit });
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <Formsy
                onValidSubmit={this.props.onSend}
                onValid={this.enableSubmit}
                onInvalid={this.disableSubmit}>
                <div className="layout-body__subtitle">
                    <h3 className="subtitle">Reset password</h3>
                </div>

                <p className="mgB">Enter the email address of your account.</p>

                <FormsyInputWrapper
                    required
                    name="email"
                    value={this.props.email}
                    validations="isEmail"
                    validationError="Enter a valid email address">
                    <TextInput
                        label="Email"
                        value={this.props.email}
                        onChange={this.props.onEmailChange}
                    />
                </FormsyInputWrapper>
                {this.props.emailSent &&
                    <div className='has-success'>
                        <span className='help-block'>Se le ha enviado un mensaje para recuperar su contraseña.</span>
                    </div>
                }
                {this.props.emailError && typeof this.props.emailError === 'string' &&
                    <div className='has-error'>
                        <span className='help-block'>{this.props.emailError}</span>
                    </div>
                }
                {this.props.emailError && typeof this.props.emailError === 'object' &&
                    <div className='has-error'>
                        <span className='help-block'>{this.props.emailError.email.message}</span>
                    </div>
                }
                <div className="mgT5 clearfix" />

                <div className="row">
                    <div className="col-6 text-center">
                        <Link to="/login">
                            <button type="button" className="btn btn-outline">Back</button>
                        </Link>
                    </div>
                    <div className="col-6 text-center">
                        <button
                            type="submit"
                            className="btn btn-primary"
                            disabled={!this.state.canSubmit}>
                            {this.props.emailSent ? 'Resend' : 'Send'}
                        </button>
                    </div>
                </div>
            </Formsy>
        );
    }
}

/**
 * Forgot password form properties validation.
 */
ForgotPasswordForm.propTypes = {
    email: PropTypes.string.isRequired,
    emailError: PropTypes.string,
    emailSent: PropTypes.bool.isRequired,
    onSend: PropTypes.func.isRequired,
    onEmailChange: PropTypes.func.isRequired
};
