import React from 'react';
import PropTypes from 'prop-types';


// Redux & React Router.
import { connect } from 'react-redux';
import { Redirect, BrowserRouter, Switch, Route } from 'react-router-dom';

// Wrappers
import AuthWrapper from './auth-wrapper';
import GuestWrapper from './guest-wrapper';

import { getAuthUser } from 'common/store/selectors/user.selector';

// Containers
import LoginContainer from './login';
import NotFoundContainer from './404';
import ForgotPasswordContainer from './forgot-password';
import ResetPasswordContainer from './reset-password';
import InConstructionContainer from './in-construction';
import RegisterContainer from './register';
import WodsContainer from './wods';


/**
 * Connect the Root element to the redux store.
 */
function mapStateToProps(state) {
    return {authUser: getAuthUser(state)};
}

/**
 * Root container, which wrap the application to be used with React-Router.
 * Routing logic is also contained here.
 */
@connect(mapStateToProps, null)
export default class Root extends React.Component {

    /**
     * Initializes the component.
     */
    constructor(props) {
        super(props);

        // Renders
        this.renderGuestRoutes = this.renderGuestRoutes.bind(this);
        this.renderAuthenticatedRoutes = this.renderAuthenticatedRoutes.bind(this);
    }

    /**
     * Renders the authenticated user's routes.
     *
     * @return {ReactComponent}
     */
    renderAuthenticatedRoutes() {
        return (
            <BrowserRouter>
                <AuthWrapper user={this.props.authUser}>
                    <Switch>
                        <Route exact path="/" component={WodsContainer} />
                        <Route path="/scores" component={WodsContainer} />
                        <Route path="/my-profile" component={InConstructionContainer} />
                        <Route path="/404" component={NotFoundContainer} />
                        <Redirect to="/404" />
                    </Switch>
                </AuthWrapper>
            </BrowserRouter>
        );
    }

    /**
     * Renders the guest routes.
     *
     * @return {ReactComponent}
     */
    renderGuestRoutes() {
        return (
            <BrowserRouter>
                <GuestWrapper>
                    <Switch>
                        <Route exact path="/" component={LoginContainer} />
                        <Route path="/login" component={LoginContainer} />
                        <Route path="/register" component={RegisterContainer} />
                        <Route path="/forgot-password" component={ForgotPasswordContainer} />
                        <Route path="/reset-password" component={ResetPasswordContainer} />
                        <Redirect to="/" />
                    </Switch>
                </GuestWrapper>
            </BrowserRouter>
        );
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {

        const user = this.props.authUser;

        return (user)
            ? this.renderAuthenticatedRoutes()
            : this.renderGuestRoutes();
    }
}

/**
 * Root properties validation.
 */
Root.WrappedComponent.propTypes = { authUser: PropTypes.object };
