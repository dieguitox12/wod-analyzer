'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';

import { PasswordInput, FormsyInputWrapper } from 'common/components/inputs';

/**
* Component responsible of the reset password form.
*/
export default class WelcomeForm extends React.Component {
    /**
  * Initializes the form.
  */
    constructor(props) {
        super(props);

        this.changePassword = this.changeField.bind(this, 'password');
        this.changePasswordConfirmation = this.changeField.bind(
            this,
            'passwordConfirmation'
        );
        this.enableSubmit = this.canSubmit.bind(this, true);
        this.disableSubmit = this.canSubmit.bind(this, false);
    }

    changeField(field, value) {
        this.props.changeField(field, value);
    }

    /**
     * Fired after formsy validations
     */
    canSubmit(canSubmit) {
        this.props.onChangeSubmit(canSubmit);
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <Formsy
                onValidSubmit={this.props.onSend}
                onValid={this.enableSubmit}
                onInvalid={this.disableSubmit}>
                <h3 className="pdT7"> Bienvenido, {this.props.name}</h3>
                <h4 className="text--gray mgT5">Su cuenta ha sido registrada con éxito en la plataforma de RE/MAX con el correo de {this.props.email}</h4>
                <h4 className="text--gray mgT2 mgB2">Un último paso:</h4>
                <h5 className="mgB3"><p>Ingresa una nueva contraseña para el sistema</p></h5>
                <div>
                    <FormsyInputWrapper
                        required
                        name="password"
                        value={this.props.password}>
                        <PasswordInput
                            value={this.props.password}
                            onChange={this.changePassword}
                            label="Nueva contraseña" />
                    </FormsyInputWrapper>
                </div>
                <div>
                    <FormsyInputWrapper
                        required
                        name="passwordConfirmation"
                        value={this.props.passwordConfirmation === this.props.password}
                        validations="isTrue"
                        validationError="Las contraseñas deben ser iguales.">
                        <PasswordInput
                            value={this.props.passwordConfirmation}
                            onChange={this.changePasswordConfirmation}
                            label="Repetir nueva contraseña" />
                    </FormsyInputWrapper>
                </div>

                {this.props.passwordChanged &&
                    <div className='has-success'>
                        <span className='help-block'>Contraseña actualizada correctamente.</span>
                    </div>
                }
                {this.props.error &&
                    <div className='has-error'>
                        <span className='help-block'>{this.props.error}</span>
                    </div>
                }
                <div className="mgT5 clearfix" />
                <div className="row">
                    <div className="col-6 text-center"/>
                    <div className="col-6 text-center">
                        <button
                            type="submit"
                            className="btn btn-primary"
                            disabled={!this.props.canSubmit}>
                            Entrar
                        </button>
                    </div>
                </div>
            </Formsy>
        );
    }
}

WelcomeForm.propTypes = {
    onChangeSubmit: PropTypes.func.isRequired,
    changeField: PropTypes.func.isRequired,
    onSend: PropTypes.func.isRequired,
    password: PropTypes.string.isRequired,
    passwordConfirmation: PropTypes.string.isRequired,
    error: PropTypes.string,
    email: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    passwordChanged: PropTypes.bool.isRequired,
    canSubmit: PropTypes.bool
};
