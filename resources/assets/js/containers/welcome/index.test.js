import React from 'react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router';

import ResetPasswordContainer from './';

describe('ResetPassword Container', () => {

    const mockStore = configureStore();

    let store = mockStore({ userReducer: {} });

    test('renders without crashing', () => {


        const tree = renderer
            .create(
                <Provider store={store}>
                    <MemoryRouter>
                        <ResetPasswordContainer location={{search: {t: 'token', email: 'email'}}} />
                    </MemoryRouter>
                </Provider>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

