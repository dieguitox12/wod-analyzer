'use strict';

import React from 'react';
import { Switch, Route } from 'react-router-dom';

import WodScoreListContainer from './wod-score-list';
import WodScoreDetailContainer from './wod-score-detail';
import WodScoreCreateContainer from './wod-score-create';
import WodScoreEditContainer from './wod-score-edit';

export default class WodsContainer extends React.Component {
    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <Switch>
                <Route exact path="/" component={WodScoreListContainer} />
                <Route exact path="/scores" component={WodScoreListContainer} />
                <Route exact path="/scores/log" component={WodScoreCreateContainer} />
                <Route exact path="/scores/edit/:id" component={WodScoreEditContainer} />
                <Route path="/scores/:id" component={WodScoreDetailContainer} />
            </Switch>
        );
    }
}
