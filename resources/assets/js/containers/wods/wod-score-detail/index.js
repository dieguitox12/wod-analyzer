'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';

import {
    BACK_ICON,
    DELETE_ICON,
    PENCIL_ICON
} from 'common/constants/icon.constant';
import * as wodSelectors from 'common/store/selectors/wod.selector';
import * as wodActions from 'common/store/actions/wod/wod.actions';
import EmptyState from 'common/components/empty-state';
import LoadingComponent from 'common/components/loading';
import WodScoreCard from 'common/components/wod-score-card';
import {
    ConfirmModal
} from 'common/components/modals';

const makeMapStateToProps = () => {
    const mapStateToProps = (state) => {
        return {
            score: wodSelectors.getScoreProfile(state),
            optimizations: wodSelectors.getOptimizations(state),
            loading: wodSelectors.getIsLoading(state),
            error: wodSelectors.getWodReducerError(state),
            scoreDeleted: wodSelectors.getScoreDeleted(state)
        };
    };
    return mapStateToProps;
};

const mapDispatchToProps = (dispatch) => {
    return {
        optimize: (id) => {
            dispatch(wodActions.getOptimizations(id));
        },
        getScore: (id) => {
            dispatch(wodActions.getScore(id));
        },
        deleteScore: (id) => {
            dispatch(wodActions.deleteScore(id));
        },
        resetOptimizations: () => {
            dispatch(wodActions.resetOptimizations());
        }
        // resetScoreMessages: () => {
        //     dispatch(wodActions.resetScoreMessages());
        // }
    };
};

/**
 * Container for the dispensaries.
 */
@withRouter
@connect(makeMapStateToProps, mapDispatchToProps)
export default class WodScoreDetailContainer extends React.Component {

    constructor(props) {
        super(props);

        this.onDeleteScore = this.onDeleteScore.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.scoreDeleted) {
            this.props.history.push('/scores');
        }
        if (!nextProps.loading && !nextProps.score) {
            this.props.history.push('/404');
        }
    }

    componentWillUnmount() {
        this.props.resetOptimizations();
    }

    onCancel() {
        ReactDOM.unmountComponentAtNode(document.getElementById('modals'));
    }

    onDeleteScore(e) {
        e.persist();
        ReactDOM.render(
            <ConfirmModal
                title="Delete score"
                text="Are you sure you want to delete this score?"
                onSuccess={() => this.props.deleteScore(this.props.score.wodScore.id)}
                onCancel={this.onCancel}
            />
            , document.getElementById('modals')
        );
    }

    componentWillMount() {
        const id = this.props.match.params.id;

        this.props.getScore(id);
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <div className="layout-body">
                <div className="layout-body__header">
                    <div className="row align-items-left mgB3">
                        <div className="col-8 align-center">
                            <a
                                href="#"
                                className={`text--weight-bold`}
                                onClick={() => this.props.history.push('/scores')}>
                                <i style={{fontSize: '20px', verticalAlign: 'middle'}} className={BACK_ICON}/>
                                {'  Back to scores'}
                            </a>
                        </div>
                        <div className="col-4">
                        </div>
                    </div>
                    <div className="row align-items-center title-zone mgB2">
                        <div className="col-4">
                            <h2 className="section-headline">Score detail</h2>
                        </div>
                        <div className="col-8 text--align-right">
                            <span
                                onClick={() => this.props.history.push(`/scores/edit/${this.props.score.wodScore.id}`)}
                                className="option-icon mgR2">
                                <i className={PENCIL_ICON} />
                            </span>
                            <span
                                onClick={this.onDeleteScore}
                                className="option-icon">
                                <i className={DELETE_ICON} />
                            </span>
                        </div>
                    </div>
                </div>
                {this.props.loading &&
                    <LoadingComponent
                        height={160}
                        width={120} />
                }
                {this.props.error &&
                    <EmptyState
                        height={100}
                        image="/images/error.svg"
                        title="Error"
                        text={this.props.error}
                    />
                }
                {!this.props.loading &&
                    this.props.score &&
                    !this.props.error &&
                    <div>
                        <WodScoreCard
                            onOptimize={this.props.optimize}
                            resetOptimizations={this.props.resetOptimizations}
                            optimizations={this.props.optimizations}
                            detailed={true}
                            wodScore={this.props.score}
                        />
                    </div>

                }

            </div>
        );
    }
}

WodScoreDetailContainer.propTypes = {
    score: PropTypes.object,
    error: PropTypes.string,
    loading: PropTypes.bool,
    history: PropTypes.shape({ push: PropTypes.func }),
    location: PropTypes.shape({ search: PropTypes.string })
};
