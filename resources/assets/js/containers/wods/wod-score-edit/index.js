'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import * as wodSelectors from 'common/store/selectors/wod.selector';
import * as userSelectors from 'common/store/selectors/user.selector';
import * as wodActions from 'common/store/actions/wod/wod.actions';
import EmptyState from 'common/components/empty-state';
import LoadingComponent from 'common/components/loading';
import { BACK_ICON } from 'common/constants/icon.constant';

import KnownWodLogScoreContainer from '../wod-score-create/known-wod';
import NewWodLogScoreContainer from '../wod-score-create/new-wod';

const makeMapStateToProps = () => {
    const mapStateToProps = (state) => {
        return {
            score: wodSelectors.getScoreProfile(state),
            authUser: userSelectors.getAuthUser(state),
            loading: wodSelectors.getIsLoading(state),
            error: wodSelectors.getWodReducerError(state),
            movements: wodSelectors.getMovements(state),
            wods: wodSelectors.getWods(state),
            simpleError: wodSelectors.getWodReducerSimpleError(state),
            scoreUpdated: wodSelectors.getScoreUpdated(state)
        };
    };
    return mapStateToProps;
};

const mapDispatchToProps = (dispatch) => {
    return {
        getMovements: () => {
            dispatch(wodActions.getMovements());
        },
        getWods: () => {
            dispatch(wodActions.getWods());
        },
        getScore: (id) => {
            dispatch(wodActions.getScore(id));
        },
        updateScore: (id, data) => {
            dispatch(wodActions.updateScore(id, data));
        },
        resetScoreMessages: () => {
            dispatch(wodActions.resetScoreMessages());
        }
    };
};

/**
 * Container for the dispensaries.
 */
@withRouter
@connect(makeMapStateToProps, mapDispatchToProps)
export default class WodScoreEditContainer extends React.Component {

    constructor(props) {
        super(props);

        this.onUpdateScore = this.onUpdateScore.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.loading && !nextProps.score) {
            this.props.history.push('/404');
        }
        if (nextProps.scoreUpdated) {
            this.props.history.push('/scores/' + this.props.score.wodScore.id);
        }
        if (nextProps.score) {
            if (nextProps.score.createdBy && nextProps.score.createdBy.id === this.props.authUser.id && this.props.movements.length === 0) {
                this.props.getMovements();
            }
            if (this.props.wods.length === 0) {
                this.props.getWods();
            }
        }
    }

    componentWillUnmount() {
        this.props.resetScoreMessages();
    }

    onUpdateScore(data) {
        this.props.updateScore(this.props.score.wodScore.id, data);
    }

    componentWillMount() {
        const id = this.props.match.params.id;

        this.props.getScore(id);
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <div className="layout-body">
                <div className="layout-body__header">
                    <div className="row align-items-left mgB3">
                        <div className="col-8 align-center">
                            <a
                                href="#"
                                className={`text--weight-bold`}
                                onClick={() => this.props.history.goBack()}>
                                <i style={{fontSize: '20px', verticalAlign: 'middle'}} className={BACK_ICON}/>
                                {'  Back'}
                            </a>
                        </div>
                        <div className="col-4">
                        </div>
                    </div>
                    <div className="row align-items-center title-zone mgB2">
                        <div className="col-12">
                            <h2 className="section-headline">Edit Score</h2>
                        </div>
                        {this.props.simpleError && typeof this.props.simpleError === 'string' &&
                            <div className='has-error'>
                                <span className='help-block'>{this.props.simpleError}</span>
                            </div>
                        }
                        {this.props.simpleError && typeof this.props.simpleError === 'object' &&
                            <div className='has-error'>
                                <span className='help-block'>{this.props.simpleError.name.message}</span>
                            </div>
                        }
                    </div>
                </div>
                {this.props.loading &&
                    <LoadingComponent
                        height={160}
                        width={120} />
                }
                {this.props.error &&
                    <EmptyState
                        height={100}
                        image="/images/error.svg"
                        title="Error"
                        text={this.props.error}
                    />
                }
                {!this.props.loading &&
                    this.props.score &&
                    this.props.score.createdBy &&
                    this.props.score.createdBy.id === this.props.authUser.id &&
                    !this.props.error &&

                    <NewWodLogScoreContainer
                        movements={this.props.movements}
                        wods={this.props.wods}
                        onCancel={() => this.props.history.goBack()}
                        score={this.props.score}
                        onSubmit={this.onUpdateScore}
                    />
                }
                {!this.props.loading &&
                    this.props.score &&
                    !this.props.score.createdBy &&
                    !this.props.error &&

                    <KnownWodLogScoreContainer
                        wods={this.props.wods}
                        score={this.props.score}
                        onCancel={() => this.props.history.goBack()}
                        onSubmit={this.onUpdateScore}
                    />
                }

            </div>
        );
    }
}

WodScoreEditContainer.propTypes = {
    score: PropTypes.object,
    error: PropTypes.string,
    loading: PropTypes.bool,
    history: PropTypes.shape({ push: PropTypes.func }),
    location: PropTypes.shape({ search: PropTypes.string })
};
