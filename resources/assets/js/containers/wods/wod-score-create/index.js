import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Tabs from 'common/components/tabs';
import * as wodSelectors from 'common/store/selectors/wod.selector';
import * as wodActions from 'common/store/actions/wod/wod.actions';

import KnownWodLogScoreContainer from './known-wod';
import NewWodLogScoreContainer from './new-wod';

const mapStateToProps = (state) => {
    return {
        error: wodSelectors.getWodReducerSimpleError(state),
        movements: wodSelectors.getMovements(state),
        wods: wodSelectors.getWods(state),
        scoreCreated: wodSelectors.getScoreCreated(state)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getMovements: () => {
            dispatch(wodActions.getMovements());
        },
        getWods: () => {
            dispatch(wodActions.getWods());
        },
        createWod: (data) => {
            dispatch(wodActions.createWod(data));
        },
        logScore: (data) => {
            dispatch(wodActions.logScore(data));
        },
        resetScoreMessages: () => {
            dispatch(wodActions.resetScoreMessages());
        }
    };
};

@withRouter
@connect(mapStateToProps, mapDispatchToProps)
export default class WodScoreCreateContainer extends React.Component {

    constructor(props) {
        super(props);

        this.createWod = this.createWod.bind(this);
        this.logScore = this.logScore.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.scoreCreated) {
            this.props.history.push('/scores/' + nextProps.scoreCreated.id);
        }
    }

    componentWillUnmount() {
        this.props.resetScoreMessages();
    }

    logScore(newData) {
        let data = Object.assign({}, newData);
        this.props.logScore(data);
    }

    createWod(newData) {
        let data = Object.assign({}, newData);
        this.props.createWod(data);
    }

    componentWillMount() {
        window.scrollTo(0, 0);

        this.props.getWods();

        this.props.getMovements();
    }

    render() {
        return (
            <div className="layout-body">
                <div className="layout-body__header">
                    <div className="row align-items-center title-zone mgB2">
                        <div className="col-12">
                            <h2 className="section-headline">Log Score</h2>
                        </div>
                        {this.props.error && typeof this.props.error === 'string' &&
                            <div className='has-error'>
                                <span className='help-block'>{this.props.error}</span>
                            </div>
                        }
                        {this.props.error && typeof this.props.error === 'object' &&
                            <div className='has-error'>
                                <span className='help-block'>{this.props.error.name.message}</span>
                            </div>
                        }
                    </div>
                </div>
                <Tabs list={[
                    {
                        title: 'Known Wod',
                        element:
                            <KnownWodLogScoreContainer
                                wods={this.props.wods}
                                onCancel={() => this.props.history.push('/scores')}
                                onSubmit={this.logScore}
                            />
                    },
                    {
                        title: 'New Wod',
                        element:
                            <NewWodLogScoreContainer
                                movements={this.props.movements}
                                wods={this.props.wods}
                                onCancel={() => this.props.history.push('/scores')}
                                onSubmit={this.createWod}
                            />
                    }
                ]} />
            </div>
        );
    }
}

WodScoreCreateContainer.propTypes = {
    error: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    movements: PropTypes.array,
    wods: PropTypes.array,
    scoreCreated: PropTypes.object
};
