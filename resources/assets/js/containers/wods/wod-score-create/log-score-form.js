import React from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';

import {
    FormsyInputWrapper,
    TextInput,
    Textarea,
    FloatingMaskedInput
} from 'common/components/inputs';

export default class LogScoreForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            canSubmit: false,
            scores: props.scores,
            observations: '',
            inputs: []
        };

        this.basicDataRef = React.createRef();
        this.addInput = this.addInput.bind(this);

        this.canSubmit = this.canSubmit.bind(this);

        this.enableSubmit = this.canSubmit.bind(this, true);
        this.disableSubmit = this.canSubmit.bind(this, false);

        this.onScoresChange = this.onScoresChange.bind(this);
        this.onObservationsChange = this.onFieldChange.bind(this, 'observations');
    }

    onScoresChange(value, id) {
        let scores = this.props.scores.slice().map(element => {
            if (element.part === id) {
                return {
                    value: value,
                    part: id
                };
            } else {
                return element;
            }
        }).filter(element => element.value !== '');
        scores = scores.sort((a, b) => {
            return a.id - b.id;
        }).map((element, i) => { return {value: element.value, part: i + 1}; });

        if (scores.length - id > -2) {
            scores.push({value: '', part: scores.length + 1});
        }
        this.props.onFieldChange('scores', scores);
    }

    addInput(element) {
        if (this.props.wod.rounds > 0) {
            return (
                <FormsyInputWrapper
                    key={element.part}
                    className={'col-6'}
                    name="score"
                    required={element.part === 1}
                    validations="isValidTimeOptional"
                    value={element.value}>
                    <FloatingMaskedInput
                        label={'Interval ' + (element.part)}
                        mask={[/[0-9]/, /[0-9]/, ':', /[0-9]/, /[0-9]/]}
                        id={element.part}
                        placeholder={'mm:ss'}
                        value={element.value}
                        onChange={this.onScoresChange}
                    />
                </FormsyInputWrapper>
            );
        } else {
            return (
                <FormsyInputWrapper
                    key={element.part}
                    name="score"
                    required={element.part === 1}
                    className=" col-md-6"
                    value={element.value}>
                    <TextInput
                        label={'Interval ' + (element.part)}
                        placeholder={'Reps'}
                        onlyNumbers={true}
                        numbers={true}
                        id={element.part}
                        value={element.value}
                        onChange={this.onScoresChange}
                    />
                </FormsyInputWrapper>
            );
        }
    }

    componentDidMount() {
        if (this.basicDataRef.current) {
            this.basicDataRef.current.reset();
        }
    }

    canSubmit(canSubmit) {
        if (this.props.canSubmit) {
            this.props.canSubmit(canSubmit);
        }
    }

    onFieldChange(field, value) {
        this.props.onFieldChange(field, value);
    }

    render() {
        return (
            <Formsy
                ref={this.basicDataRef}
                onValid={this.enableSubmit}
                onInvalid={this.disableSubmit}>
                <div className="row">
                    {this.props.scores.map(element => this.addInput(element))}
                </div>
                <div className="row">
                    <FormsyInputWrapper
                        name="observations"
                        className="col-12"
                        value={this.props.observations}>
                        <Textarea
                            label="Observations"
                            value={this.props.observations}
                            onChange={this.onObservationsChange}
                        />
                    </FormsyInputWrapper>
                </div>
            </Formsy>
        );
    }
}

LogScoreForm.propTypes = {
    onScoresChange: PropTypes.func,
    wod: PropTypes.object.isRequired,
    scores: PropTypes.array,
    canSubmit: PropTypes.func
};
