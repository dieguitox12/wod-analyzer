import React from 'react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router';
import { mockWodState } from 'mock/wod.mock';

import WodScoreCreateContainer from './';

describe('Wod Score Create Container', () => {

    const mockStore = configureStore();

    let store = mockStore({ wodReducer: mockWodState });

    test('renders without crashing without error', () => {
        const tree = renderer
            .create(
                <Provider store={store}>
                    <MemoryRouter>
                        <WodScoreCreateContainer />
                    </MemoryRouter>
                </Provider>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    test('renders without crashing with error', () => {
        let newStore = mockStore({ wodReducer: Object.assign({}, mockWodState, {error: 'error'}) });

        const tree = renderer
            .create(
                <Provider store={newStore}>
                    <MemoryRouter>
                        <WodScoreCreateContainer error={'error'} />
                    </MemoryRouter>
                </Provider>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

