'use strict';

import React from 'react';
import PropTypes from 'prop-types';

import {
    Select
} from 'common/components/inputs';
import minutesToSeconds from 'common/helpers/minutes-to-seconds';
import secondsToMinutes from 'common/helpers/seconds-to-minutes';
import wodToString from 'common/helpers/wod-to-string';

import LogScoreForm from '../log-score-form';

export default class KnownWodLogScoreContainer extends React.Component {
    /**
     * Set the states and makes the binds as needed.
     */
    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.onWodChange = this.onWodChange.bind(this);
        this.canSubmit = this.canSubmit.bind(this);
        this.onFieldChange = this.onFieldChange.bind(this);

        this.state = {
            wodId: props.score ? props.score.id : null,
            wod: props.score ? props.score : null,
            canSubmit: false,
            scores: props.score ? props.score.wodScore.scores.map(element => {
                return {
                    value: props.score.rounds ? secondsToMinutes(element.value) : element.value,
                    part: element.part
                };
            }).concat([
                {
                    value: '',
                    part: props.score.wodScore.scores.length + 1
                }
            ]): [
                {
                    value: '',
                    part: 1
                }
            ],
            observations: props.score && props.score.wodScore.observations ? props.score.wodScore.observations : ''
        };
    }

    onFieldChange(field, value) {
        if (field === 'scores' && value.length === 1) {
            this.canSubmit(false);
        } else if(field === 'scores') {
            this.canSubmit(true);
        }
        this.setState({[field]: value});
    }

    canSubmit(value) {
        this.setState({canSubmit: value});
    }

    /**
     * Fired once the email or the password changes.
     */
    onWodChange(value) {
        this.setState({
            wodId: value,
            canSubmit: false,
            scores: [
                {
                    value: '',
                    part: 1
                }
            ],
            wod: this.props.wods.filter(element => element.id === value)[0]
        });
    }

    onSubmit() {
        let data = {};
        data['wodId'] = this.state.wodId;
        data['scores'] = this.state.scores.map(element => {
            if (this.state.wod.rounds > 0) {
                return {
                    value: minutesToSeconds(element.value),
                    part: element.part
                };
            } else {
                return {
                    value: parseInt(element.value),
                    part: element.part
                };
            }
        });
        data['scores'].pop();
        data['observations'] = this.state.observations || null;
        this.props.onSubmit(data);
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <div className="layout-body__content white-background pd3">
                <div className="input-wrapper">
                    <Select
                        value={this.state.wodId}
                        searchable={true}
                        label="Search wod"
                        multi={false}
                        floatingLabel={'Wod'}
                        options={this.props.wods}
                        onChange={this.onWodChange}
                    />
                </div>
                {this.state.wod &&
                    <span className="col-12">
                        <p className="breakLine">{wodToString(this.state.wod)}</p>
                    </span>
                }

                {this.state.wodId &&
                    <LogScoreForm
                        wod={this.state.wod}
                        scores={this.state.scores}
                        canSubmit={this.canSubmit}
                        observations={this.state.observations}
                        onFieldChange={this.onFieldChange}
                    />
                }
                <div className="row footer pdB2 pdT2 ">
                    <div className="col-6 text--align-center">
                        <button
                            onClick={this.props.onCancel}
                            className="btn btn-outline">
                            Cancel
                        </button>
                    </div>
                    <div className="col-6 text--align-center">
                        <button
                            disabled={!this.state.canSubmit}
                            onClick={this.onSubmit}
                            className="btn btn-primary">
                            Log Score
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

KnownWodLogScoreContainer.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    wods: PropTypes.array,
    score: PropTypes.object
};
