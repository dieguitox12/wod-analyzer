'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';

import {
    FormsyInputWrapper,
    TextInput,
    Select,
    Checkbox,
    FloatingMaskedInput
} from 'common/components/inputs';
import {
    WOD_TYPES,
    MEASUREMENTS
} from 'common/constants/enums.constant';
import ClosableCard from 'common/components/closable-card';
import { PLUS_CIRCLE_ICON } from 'common/constants/icon.constant';
import minutesToSeconds from 'common/helpers/minutes-to-seconds';
import secondsToMinutes from 'common/helpers/seconds-to-minutes';

import LogScoreForm from '../log-score-form';

export default class NewWodLogScoreContainer extends React.Component {
    /**
     * Set the states and makes the binds as needed.
     */
    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.canSubmit = this.canSubmit.bind(this);
        this.onNameChange = this.onFieldChange.bind(this, 'name');
        this.onRoundsChange = this.onFieldChange.bind(this, 'rounds');
        this.onTypeChange = this.onFieldChange.bind(this, 'type');
        this.onTimeCapChange = this.onFieldChange.bind(this, 'timeCap');
        this.onMovementIdChange = this.onMovementChange.bind(this, 'id');
        this.onMovementRepsChange = this.onMovementChange.bind(this, 'reps');
        this.onExtraInfoChanged = this.onMovementChange.bind(this, 'extraInfo');
        this.onManMeasureChange = this.onMovementChange.bind(this, 'manMeasure');
        this.onManMeasureIdChange = this.onMovementChange.bind(this, 'menMeasurementId');
        this.onWomanMeasureChange = this.onMovementChange.bind(this, 'womanMeasure');
        this.onWomanMeasureIdChange = this.onMovementChange.bind(this, 'womenMeasurementId');
        this.onNameBlur = this.onNameBlur.bind(this);
        this.compareWods = this.compareWods.bind(this);

        this.onMovementChange = this.onMovementChange.bind(this);
        this.createMovementCard = this.createMovementCard.bind(this);
        this.onAddMovement = this.onAddMovement.bind(this);
        this.onMovementRemoved = this.onMovementRemoved.bind(this);

        this.onFieldChange = this.onFieldChange.bind(this);
        this.basicDataRef = React.createRef();

        this.canSubmitScores = this.canSubmitScores.bind(this);

        this.enableSubmit = this.canSubmit.bind(this, true);
        this.disableSubmit = this.canSubmit.bind(this, false);

        this.state = {
            usedName: false,
            sameWod: false,
            name: props.score ? props.score.name : '',
            rounds: props.score ? props.score.rounds : 1,
            timeCap: props.score && props.score.timeCap ? secondsToMinutes(props.score.timeCap) : '',
            type: props.score ?
                (props.score.rounds && props.score.rounds > 1 ?
                    WOD_TYPES.ROUNDS_FOR_TIME : (!props.score.rounds ? WOD_TYPES.AMRAP : WOD_TYPES.FOR_TIME)
                ) :
                WOD_TYPES.FOR_TIME,
            movements: props.score ? props.score.movements.map(element => {
                return {
                    id: element.movement.id,
                    reps: element.reps,
                    manMeasure: element.manMeasure ? element.manMeasure.value : null,
                    menMeasurementId: element.manMeasure ? element.manMeasure.id : null,
                    extraInfo: element.manMeasure || element.womanMeasure ? true : false,
                    womanMeasure: element.womanMeasure ? element.womanMeasure.value : '',
                    womenMeasurementId: element.womanMeasure ? element.womanMeasure.id : null
                };
            }) : [],
            canSubmit: false,
            canSubmitScores: false,
            scores: props.score ? props.score.wodScore.scores.map(element => {
                return {
                    value: props.score.rounds ? secondsToMinutes(element.value) : element.value,
                    part: element.part
                };
            }).concat([
                {
                    value: '',
                    part: props.score.wodScore.scores.length + 1
                }
            ]): [
                {
                    value: '',
                    part: 1
                }
            ],
            observations: props.score ? props.score.wodScore.observations : ''
        };
    }

    onNameBlur() {
        if (this.props.wods.filter(element => element.name === this.state.name).length > 0) {
            this.setState({usedName: true});
        } else {
            this.setState({usedName: false});
        }
    }

    onAddMovement() {
        let movements = this.state.movements.slice();
        movements.push({
            id: null,
            reps: '',
            manMeasure: '',
            menMeasurementId: null,
            extraInfo: false,
            womanMeasure: '',
            womenMeasurementId: null
        });
        this.onFieldChange('movements', movements);
    }

    canSubmitScores(value) {
        this.setState({canSubmitScores: value});
    }

    onMovementRemoved(index) {
        let movements = this.state.movements.slice();
        movements.splice(index, 1);

        this.onFieldChange('movements', movements);
    }

    // Return true if are equal otherwise false
    compareWods(wod1, wod2) {
        if (!wod2.timeCap && wod1.timeCap) {
            return false;
        }
        if (!wod2.timeCap && wod1.timeCap) {
            return false;
        }
        if (minutesToSeconds(wod1.timeCap) !== wod2.timeCap) {
            return false;
        }
        if (parseInt(wod1.rounds) !== parseInt(wod2.rounds)) {
            return false;
        }
        if (wod1.movements.length === wod2.movements.length) {
            for (let i = 0; i < wod1.movements.length; i++) {
                if (wod1.movements[i].id === wod2.movements[i].movement.id) {
                    if (parseInt(wod1.movements[i].reps) === parseInt(wod2.movements[i].reps)) {
                        if (wod2.movements[i].manMeasure) {
                            if (wod1.movements[i].menMeasurementId !== wod2.movements[i].manMeasure.id) {
                                return false;
                            }
                            if (parseFloat(wod1.movements[i].manMeasure) !== parseFloat(wod2.movements[i].manMeasure.value)) {
                                return false;
                            }
                        } else {
                            if (wod1.movements[i].menMeasurementId || wod1.movements[i].manMeasure) {
                                return false;
                            }
                        }
                        if (wod2.movements[i].womanMeasure) {
                            if (wod1.movements[i].womenMeasurementId !== wod2.movements[i].womanMeasure.id) {
                                return false;
                            }
                            if (parseFloat(wod1.movements[i].womanMeasure) !== parseFloat(wod2.movements[i].womanMeasure.value)) {
                                return false;
                            }
                        } else {
                            if (wod1.movements[i].womenMeasurementId || wod1.movements[i].womanMeasure) {
                                return false;
                            }
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    onMovementChange(field, value, id) {
        let movements = this.state.movements.slice();
        movements[id][field] = value;
        if (field === 'extraInfo' && value === false) {
            movements[id]['manMeasure'] = '';
            movements[id]['menMeasurementId'] = null;
            movements[id]['womanMeasure'] = '';
            movements[id]['womenMeasurementId'] = null;
        }

        this.onFieldChange('movements', movements);
    }

    onFieldChange(field, value) {
        if (field === 'scores' && value.length === 1) {
            this.canSubmit(false);
        } else if(field === 'scores') {
            this.canSubmit(true);
        }
        if (field === 'type' && value === WOD_TYPES.FOR_TIME) {
            this.setState({rounds: 1});
        }
        if (field === 'type' && value === WOD_TYPES.AMRAP) {
            this.setState({rounds: null});
        }

        let sameWod = false;
        for (let index = 0; index < this.props.wods.length; index++) {
            if (this.props.score) {
                if (this.props.score.id === this.props.wods[index].id) {
                    continue;
                }
            }
            sameWod = sameWod || this.compareWods({
                movements: field === 'movements' ? value : this.state.movements,
                rounds: field === 'rounds' ? value : (field === 'type' && value === WOD_TYPES.FOR_TIME ? 1 : (field === 'type' && value === WOD_TYPES.AMRAP ? null : this.state.rounds)),
                timeCap: field === 'timeCap' ? value : this.state.timeCap
            }, this.props.wods[index]);
        }
        this.setState({sameWod: sameWod});

        this.setState({[field]: value});
    }

    canSubmit(value) {
        this.setState({canSubmit: value});
    }

    onSubmit() {
        let data = {
            name: this.state.name,
            rounds: this.state.rounds,
            timeCap: minutesToSeconds(this.state.timeCap)
        };
        if (data['rounds'] === '') {
            delete data['rounds'];
        }
        if (data['timeCap'] === '') {
            delete data['timeCap'];
        }
        data['scores'] = this.state.scores.map(element => {
            if (this.state.rounds > 0) {
                return {
                    value: minutesToSeconds(element.value),
                    part: element.part
                };
            } else {
                return {
                    value: parseInt(element.value),
                    part: element.part
                };
            }
        });
        data['movements'] = this.state.movements.map(element => {
            let newElement = {id: element.id};
            if (element.reps) {
                newElement['reps'] = element.reps;
            }
            if (element.manMeasure) {
                newElement['manMeasure'] = parseFloat(element.manMeasure);
            }
            if (element.menMeasurementId) {
                newElement['menMeasurementId'] = element.menMeasurementId;
            }
            if (element.womanMeasure) {
                newElement['womanMeasure'] = parseFloat(element.womanMeasure);
            }
            if (element.womenMeasurementId) {
                newElement['womenMeasurementId'] = element.womenMeasurementId;
            }
            return newElement;
        });
        data['scores'].pop();
        data['observations'] = this.state.observations || null;
        this.props.onSubmit(data);
    }

    createMovementCard(movement, index) {
        return (
            <ClosableCard
                key={index}
                id={index}
                className="col-12 mgB2"
                onClose={this.onMovementRemoved}>
                <div className="row">
                    <FormsyInputWrapper
                        name="type"
                        required
                        className="col-6"
                        value={movement.id > 0}
                        validations="isTrue">
                        <Select
                            value={movement.id}
                            floatingLabel="Movement"
                            id={index}
                            label="Select movement"
                            searchable={true}
                            options={this.props.movements}
                            onChange={this.onMovementIdChange}
                        />
                    </FormsyInputWrapper>
                    <FormsyInputWrapper
                        name="name"
                        className="col-6"
                        value={movement.reps}>
                        <TextInput
                            label={'Reps/Cals'}
                            numbers={true}
                            placeholder="How many reps ?"
                            id={index}
                            onlyNumbers={true}
                            value={movement.reps}
                            onChange={this.onMovementRepsChange}
                        />
                    </FormsyInputWrapper>
                    <div className="col-12 mgB d-flex">
                        <h5 className="mgR">Extra information ?</h5>
                        <Checkbox
                            squared={true}
                            id={index}
                            checked={movement.extraInfo}
                            value={movement.extraInfo}
                            onChange={this.onExtraInfoChanged}/>
                    </div>

                </div>
                {movement.extraInfo &&
                    <div className="row">
                        <div className="col-12 input-wrapper mgB0">
                            <label className="form-label">
                                <span className="label">Info 1</span>
                            </label>
                        </div>
                        <FormsyInputWrapper
                            className="col-6"
                            name="manMeasure"
                            value={movement.manMeasure}>
                            <TextInput
                                numbers={true}
                                onlyNumbers={true}
                                id={index}
                                value={movement.manMeasure}
                                onChange={this.onManMeasureChange}
                            />
                        </FormsyInputWrapper>
                        <FormsyInputWrapper
                            className="col-6"
                            name="menMeasurementId"
                            value={movement.menMeasurementId > 0}>
                            <Select
                                value={movement.menMeasurementId}
                                searchable={false}
                                id={index}
                                options={MEASUREMENTS}
                                onChange={this.onManMeasureIdChange}
                            />
                        </FormsyInputWrapper>
                        <div className="col-12 input-wrapper mgB0">
                            <label className="form-label">
                                <span className="label">Info 2</span>
                            </label>
                        </div>
                        <FormsyInputWrapper
                            className="col-6"
                            name="womanMeasure"
                            value={movement.womanMeasure}>
                            <TextInput
                                numbers={true}
                                id={index}
                                onlyNumbers={true}
                                value={movement.womanMeasure}
                                onChange={this.onWomanMeasureChange}
                            />
                        </FormsyInputWrapper>
                        <FormsyInputWrapper
                            className="col-6"
                            name="womenMeasurementId"
                            value={movement.womenMeasurementId > 0}>
                            <Select
                                value={movement.womenMeasurementId}
                                searchable={false}
                                id={index}
                                options={MEASUREMENTS}
                                onChange={this.onWomanMeasureIdChange}
                            />
                        </FormsyInputWrapper>
                    </div>
                }

            </ClosableCard>
        );
    }

    componentDidMount() {
        if (this.basicDataRef.current) {
            this.basicDataRef.current.reset();
        }
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <div className="layout-body__content white-background pd3">
                <Formsy
                    ref={this.basicDataRef}
                    onValid={this.enableSubmit}
                    onInvalid={this.disableSubmit}>
                    <div className="row">
                        <FormsyInputWrapper
                            required
                            name="name"
                            customError={`${this.state.usedName ? 'There is a Wod named like this' : ''}`}
                            className="col-6"
                            value={this.state.name}>
                            <TextInput
                                label={'Name'}
                                onBlur={this.onNameBlur}
                                numbers={true}
                                value={this.state.name}
                                onChange={this.onNameChange}
                            />
                        </FormsyInputWrapper>
                        <FormsyInputWrapper
                            name="type"
                            required
                            className="col-6"
                            value={this.state.type !== null}
                            validations="isTrue">
                            <Select
                                value={this.state.type}
                                floatingLabel="Type"
                                searchable={false}
                                options={[
                                    {id: WOD_TYPES.FOR_TIME, name: WOD_TYPES.FOR_TIME},
                                    {id: WOD_TYPES.ROUNDS_FOR_TIME, name: WOD_TYPES.ROUNDS_FOR_TIME},
                                    {id: WOD_TYPES.AMRAP, name: WOD_TYPES.AMRAP}
                                ]}
                                onChange={this.onTypeChange}
                            />
                        </FormsyInputWrapper>
                    </div>
                    <div className="row">
                        {this.state.type === WOD_TYPES.ROUNDS_FOR_TIME &&
                            <FormsyInputWrapper
                                required
                                name="rounds"
                                className="col-6"
                                value={this.state.rounds}>
                                <TextInput
                                    numbers={true}
                                    onlyNumbers={true}
                                    label={'Rounds'}
                                    value={this.state.rounds}
                                    onChange={this.onRoundsChange}
                                />
                            </FormsyInputWrapper>
                        }
                        <FormsyInputWrapper
                            required={this.state.type === WOD_TYPES.AMRAP}
                            name="timeCap"
                            className="col-6"
                            validations={this.state.type === WOD_TYPES.AMRAP ? 'isValidTime' : 'isValidTimeOptional'}
                            value={this.state.timeCap}>
                            <FloatingMaskedInput
                                label={'Time Cap'}
                                mask={[/[0-9]/, /[0-9]/, ':', /[0-9]/, /[0-9]/]}
                                placeholder={`mm:ss ${this.state.type === WOD_TYPES.AMRAP ? '' : '(Optional)'}`}
                                value={this.state.timeCap}
                                onChange={this.onTimeCapChange}
                            />
                        </FormsyInputWrapper>
                    </div>
                    <div>
                        {this.state.movements.map((element, index) => { return this.createMovementCard(element, index); })}
                    </div>
                    <button
                        onClick={this.onAddMovement}
                        className = "btn btn-link display--block pl-0 mgT mgB3 btn-transparent" >
                        <i className={PLUS_CIRCLE_ICON} /> Add Movement
                    </button>
                </Formsy>
                {this.state.movements.length > 0 &&
                    <LogScoreForm
                        wod={{
                            name: this.state.name,
                            rounds: this.state.rounds,
                            timeCap: this.state.timeCap,
                            movements: this.state.movements
                        }}
                        scores={this.state.scores}
                        canSubmit={this.canSubmitScores}
                        observations={this.state.observations}
                        onFieldChange={this.onFieldChange}
                    />
                }
                {this.state.sameWod &&
                    <div className='has-error col-12'>
                        <span className='help-block'>There is already a Wod with the same movements.</span>
                    </div>
                }
                <div className="row footer pdB2 pdT2 ">
                    <div className="col-6 text--align-center">
                        <button
                            onClick={this.props.onCancel}
                            className="btn btn-outline">
                            Cancel
                        </button>
                    </div>
                    <div className="col-6 text--align-center">
                        <button
                            disabled={!(this.state.canSubmit && this.state.canSubmitScores && !this.state.usedName && !this.state.sameWod)}
                            onClick={this.onSubmit}
                            className="btn btn-primary">
                            {`${this.props.score ? 'Save changes' : 'Log Score'}`}
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

NewWodLogScoreContainer.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    movements: PropTypes.array,
    wods: PropTypes.array,
    score: PropTypes.object
};
