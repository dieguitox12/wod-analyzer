'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Media from 'react-media';

import {
    SearchInput,
    DatetimePicker
} from 'common/components/inputs';
import * as wodSelectors from 'common/store/selectors/wod.selector';
import * as wodActions from 'common/store/actions/wod/wod.actions';
import EmptyState from 'common/components/empty-state';
import LoadingComponent from 'common/components/loading';

import WodScoreListView from './wod-score-list-view';

const makeMapStateToProps = () => {
    const scores = wodSelectors.getFilteredScores();
    const mapStateToProps = (state) => {
        return {
            scores: scores(state),
            filters: wodSelectors.getFilters(state),
            loading: wodSelectors.getIsLoading(state),
            error: wodSelectors.getWodReducerError(state),
            scoreDeleted: wodSelectors.getScoreDeleted(state)
        };
    };
    return mapStateToProps;
};

const mapDispatchToProps = (dispatch) => {
    return {
        getScores: () => {
            dispatch(wodActions.getScores());
        },
        setFilters: (filters) => {
            dispatch(wodActions.setFilters(filters));
        },
        resetScoreMessages: () => {
            dispatch(wodActions.resetScoreMessages());
        }
    };
};

/**
 * Container for the dispensaries.
 */
@withRouter
@connect(makeMapStateToProps, mapDispatchToProps)
export default class WodScoreListContainer extends React.Component {

    /**
    * Initializes the component.
    */
    constructor(props) {
        super(props);

        this.onSearchQueryChange = this.onFilterChange.bind(this, 'q');
        this.onFromChange = this.onFilterChange.bind(this, 'from');
        this.onToChange = this.onFilterChange.bind(this, 'to');
    }

    componentWillUnmount() {
        this.props.resetScoreMessages();
    }

    onFilterChange(field, value) {
        let filters = Object.assign({}, this.props.filters);
        filters[field] = value;
        this.props.setFilters(filters);
    }

    componentWillMount() {
        this.props.getScores();
        this.props.setFilters({
            q: '',
            from: '',
            to: ''
        });
    }

    componentWillReceiveProps() {
        if (this.props.scoreDeleted) {
            setTimeout(() => {
                this.props.resetScoreMessages();
            }, 6000);
        }
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <div className="layout-body">
                <div className="layout-body__header">
                    <div className="row align-items-center title-zone">
                        <div className="col-12 mgB2">
                            <h2 className="section-headline">Scores</h2>
                        </div>
                        <Media query='(max-width: 999px)'>
                            {matches =>
                                matches ? (
                                    <div className="col-12">
                                        <div className="mgB2">
                                            <SearchInput
                                                weight={500}
                                                value={this.props.filters.q}
                                                label="Search"
                                                onChange={this.onSearchQueryChange}
                                            />
                                        </div>
                                        <div className="mgB2 row">
                                            <div className="col-6">
                                                <DatetimePicker
                                                    label="From"
                                                    floating={true}
                                                    withIcon={true}
                                                    placeholder="Select date"
                                                    className="full-width"
                                                    onChange={this.onFromChange}
                                                    value={this.props.filters.from}
                                                />
                                            </div>
                                            <div className="col-6">
                                                <DatetimePicker
                                                    label="To"
                                                    floating={true}
                                                    withIcon={true}
                                                    placeholder="Select date"
                                                    className="full-width"
                                                    onChange={this.onToChange}
                                                    value={this.props.filters.to}
                                                />
                                            </div>
                                        </div>

                                    </div>
                                ) : (
                                    null
                                )
                            }
                        </Media>
                    </div>
                    {this.props.scoreDeleted &&
                        <div className='has-success'>
                            <span className='help-block'>Score deleted succesfully.</span>
                        </div>
                    }
                </div>
                {this.props.loading &&
                    <LoadingComponent
                        height={160}
                        width={120} />
                }
                {!this.props.loading &&
                    this.props.scores.length < 1 &&
                    !this.props.error &&
                    <EmptyState
                        imgStyle = {
                            {
                                backgroundColor: 'rgba(0, 125, 195, 0.08)',
                                borderRadius: '150px',
                                margin: 'auto',
                                width: '170px',
                                padding: '20px'
                            }
                        }
                        image="/images/ic_search.svg"
                        title=""
                        text={'No results found.'}
                    />
                }
                {this.props.error &&
                    <EmptyState
                        height={100}
                        image="/images/error.svg"
                        title="Error"
                        text={this.props.error}
                    />
                }
                {!this.props.loading &&
                    this.props.scores.length > 0 &&
                    !this.props.error &&
                    <div>
                        <WodScoreListView
                            items={this.props.scores}
                            history={this.props.history}
                        />
                    </div>

                }

            </div>
        );
    }
}

WodScoreListContainer.propTypes = {
    scores: PropTypes.array,
    error: PropTypes.string,
    loading: PropTypes.bool,
    filters: PropTypes.object,
    history: PropTypes.shape({ push: PropTypes.func }),
    location: PropTypes.shape({ search: PropTypes.string })
};
