import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { createMemoryHistory } from 'history';

import WodScoreListView from './wod-score-list-view';

configure({ adapter: new Adapter() });

describe('WodScoreListView Container', () => {

    const history = createMemoryHistory('/scores');

    test('renders without wods', () => {
        const tree = renderer
            .create(
                <MemoryRouter>
                    <WodScoreListView
                        history={history}
                        items={[]}
                    />
                </MemoryRouter>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    /*test('renders with wods', () => {
        const tree = renderer
            .create(
                <MemoryRouter>
                    <WodScoreListView
                        history={history}
                        items={mockWodsDetails}
                    />
                </MemoryRouter>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });*/
});

