'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import Media from 'react-media';

import Pagination from 'common/components/pagination';
import { PER_PAGE } from 'common/constants/enums.constant';
import WodScoreCard from 'common/components/wod-score-card';

/**
 * Component responsible of rendering the list depending on it's type.
 */
export default class WodScoreListView extends React.PureComponent {
    /**
     * Initializes the component.
     */
    constructor(props) {
        super(props);

        this.state = {page: 0};

        this.onPageChange = this.onPageChange.bind(this);
        this.onCardClicked = this.onCardClicked.bind(this);
    }

    onCardClicked(id) {
        this.props.history.push('/scores/' + id);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.items.length !== this.props.items.length) {
            this.setState({page: 0});
        }

    }

    /**
     * Fired once the selected page has changed.
     *
     * @param  {object} pageObject  an object which contains the selected page.
     */
    onPageChange(pageObject) {
        this.setState({page: pageObject.selected}, () => {
            window.scrollTo(0, 0);
        });
    }

    /**
    * Renders the element.
    *
    * @return {ReactComponent}
    */
    render() {
        const beginning = this.state.page * PER_PAGE;
        const end = (this.state.page + 1) * PER_PAGE;
        const pageCount = Math.ceil(this.props.items.length / PER_PAGE);
        return (
            <div>
                <div className={`layout-body__content`}>
                    <div style={{marginTop: '0px'}} className="pagination">
                        <Pagination
                            onPageChange={this.onPageChange}
                            forcePage={this.state.page}
                            pageCount={pageCount} />
                    </div>
                    <div className="row">
                        {this.props.items
                            .slice(beginning, end)
                            .map((wodScore) => {
                                return (
                                    <Media
                                        key={"wod-score-key-" + wodScore.wodScore.id}
                                        query='(max-width: 999px)'>
                                        {matches =>
                                            matches ? (
                                                <div
                                                    style={{marginBottom: '36px'}}
                                                    className="col-12">
                                                    <WodScoreCard
                                                        wodScore={wodScore}
                                                        onClick={this.onCardClicked}
                                                    />
                                                </div>
                                            ) : (
                                                <div
                                                    style={{marginBottom: '36px'}}
                                                    className="col-6">
                                                    <WodScoreCard
                                                        wodScore={wodScore}
                                                        onClick={this.onCardClicked}
                                                    />
                                                </div>
                                            )
                                        }
                                    </Media>
                                );
                            })}
                    </div>
                    <div className="pagination">
                        <Pagination
                            onPageChange={this.onPageChange}
                            forcePage={this.state.page}
                            pageCount={pageCount} />
                    </div>
                </div>
            </div>
        );
    }
}

/**
 * Properties validation.
 */
WodScoreListView.propTypes = {
    items: PropTypes.array.isRequired,
    history: PropTypes.object.isRequired
};
