'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import QueryParser from 'common/helpers/query-parser';
import { resetPassword } from 'common/store/actions/user/user.actions';
import {
    getAuthUser,
    getUserReducerError,
    getPasswordChanged
} from 'common/store/selectors/user.selector';

import ResetPasswordForm from './reset-password-form';

const mapDispatchToProps = (dispatch) => {
    return {
        resetPassword: (password, passwordConfirmation, token) => {
            dispatch(resetPassword(password, passwordConfirmation, token));
        }
    };
};

const mapStateToProps = (state) => {
    return {
        authUser: getAuthUser(state),
        error: getUserReducerError(state),
        passwordChanged: getPasswordChanged(state)
    };
};

/**
* Container which handles the reset password logic.
*/
@connect(mapStateToProps, mapDispatchToProps)
export default class ResetPasswordContainer extends React.Component {

    /**
     * Initializes the container.
     */
    constructor(props) {
        super(props);

        this.state = {
            token: '',
            password: '',
            email: '',
            passwordConfirmation: '',
            passwordChanged: false,
            error: '',
            canSubmit: false
        };

        this.onSend = this.onSend.bind(this);
        this.canSubmit = this.canSubmit.bind(this);
        this.changeField = this.changeField.bind(this);
    }

    componentWillMount() {
        let token = QueryParser(this.props.location.search)['t'];
        let email = QueryParser(this.props.location.search)['email'];

        if (token) {
            this.setState({
                token: token,
                email: email
            });
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({ passwordChanged: newProps.passwordChanged, error: newProps.error });
    }

    /**
     * Fired once any of the form fields changes.
     */
    changeField(field, value) {
        this.setState({
            [field]: value,
            error: '',
            passwordChanged: false
        });
    }

    /**
     * Fired after formsy validations
     */
    canSubmit(canSubmit) {
        this.setState({ canSubmit });
    }

    /**
     * Fired once the send button has been clicked.
     */
    onSend() {
        const password = this.state.password;
        const passwordConfirmation = this.state.passwordConfirmation;
        const token = this.state.token;

        this.props.resetPassword(
            password,
            passwordConfirmation,
            token
        );
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        if (this.state.error === 'Token not found try again' ) {
            return (<h5>This link to reset your password is not valid anymore. Request another one <Link to="/forgot-password">here</Link>.</h5>);
        }

        return (
            <ResetPasswordForm
                email={this.state.email}
                password={this.state.password}
                passwordConfirmation={this.state.passwordConfirmation}
                canSubmit={this.state.canSubmit}
                changeField={this.changeField}
                error={this.state.error}
                passwordChanged={this.state.passwordChanged}
                onSend={this.onSend}
                onChangeSubmit={this.canSubmit}
            />
        );
    }
}

/**
 * ResetPassword form properties validation.
 */
ResetPasswordContainer.WrappedComponent.propTypes = {
    match: PropTypes.shape({ params: PropTypes.shape({ t: PropTypes.string }).isRequired }).isRequired,
    location: PropTypes.shape({ search: PropTypes.string }).isRequired,
    history: PropTypes.shape({ replace: PropTypes.func.isRequired }).isRequired,
    resetPassword: PropTypes.func.isRequired
};
