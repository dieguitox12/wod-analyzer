'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';
import { Link } from 'react-router-dom';

import { PasswordInput, FormsyInputWrapper } from 'common/components/inputs';

/**
* Component responsible of the reset password form.
*/
export default class ResetPasswordForm extends React.Component {
    /**
  * Initializes the form.
  */
    constructor(props) {
        super(props);

        this.changePassword = this.changeField.bind(this, 'password');
        this.changePasswordConfirmation = this.changeField.bind(
            this,
            'passwordConfirmation'
        );
        this.enableSubmit = this.canSubmit.bind(this, true);
        this.disableSubmit = this.canSubmit.bind(this, false);
    }

    changeField(field, value) {
        this.props.changeField(field, value);
    }

    /**
     * Fired after formsy validations
     */
    canSubmit(canSubmit) {
        this.props.onChangeSubmit(canSubmit);
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <Formsy
                onValidSubmit={this.props.onSend}
                onValid={this.enableSubmit}
                onInvalid={this.disableSubmit}>
                <h3> Change Password </h3>
                <hr />
                <h5 className="mgB3"><p>You have requested a password change for the email address {this.props.email}.</p><p>Enter your new password down below.</p></h5>
                <div className="col-12 pdL0 pdR0">
                    <FormsyInputWrapper
                        required
                        name="password"
                        value={this.props.password}>
                        <PasswordInput
                            value={this.props.password}
                            onChange={this.changePassword}
                            label="New password" />
                    </FormsyInputWrapper>
                </div>
                <div className="col-12 pdL0 pdR0">
                    <FormsyInputWrapper
                        required
                        name="passwordConfirmation"
                        value={this.props.passwordConfirmation === this.props.password}
                        validations="isTrue"
                        validationError="Passwords must match">
                        <PasswordInput
                            value={this.props.passwordConfirmation}
                            onChange={this.changePasswordConfirmation}
                            label="Repeat new password" />
                    </FormsyInputWrapper>
                </div>

                {this.props.passwordChanged &&
                    <div className='has-success'>
                        <span className='help-block'>Password updated correctly</span>
                    </div>
                }
                {this.props.error &&
                    <div className='has-error'>
                        <span className='help-block'>{this.props.error}</span>
                    </div>
                }
                <div className="mgT5 clearfix" />
                <div className="row">
                    <div className="col-6 text-center">
                        <Link to="/login">
                            <button type="button" className="btn btn-outline">Back</button>
                        </Link>
                    </div>
                    <div className="col-6 text-center">
                        <button
                            type="submit"
                            className="btn btn-primary"
                            disabled={!this.props.canSubmit}>
                            Change
                        </button>
                    </div>
                </div>
            </Formsy>
        );
    }
}

ResetPasswordForm.propTypes = {
    onChangeSubmit: PropTypes.func.isRequired,
    changeField: PropTypes.func.isRequired,
    onSend: PropTypes.func.isRequired,
    password: PropTypes.string.isRequired,
    passwordConfirmation: PropTypes.string.isRequired,
    error: PropTypes.string,
    email: PropTypes.string.isRequired,
    passwordChanged: PropTypes.bool.isRequired,
    canSubmit: PropTypes.bool
};
