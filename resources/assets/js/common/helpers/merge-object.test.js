import merge from 'common/helpers/merge-object';

describe('Merge Object', () => {

    test('merge obj2 to obj1 correctly', () => {
        const obj1 = {
            id: 1,
            name: 'A name',
            age: 21,
            birthday: '1998-02-02'
        };

        const obj2 = {
            age: 22,
            birthday: '1999-01-01'
        };

        const mergedObject = merge(obj1, obj2);

        expect(mergedObject).toEqual({
            id: 1,
            name: 'A name',
            age: 22,
            birthday: '1999-01-01'
        });
    });

    test('merge obj1 to obj2 correctly', () => {
        const obj2 = {
            id: 1,
            name: 'A name',
            age: 2,
            birthday: null
        };

        const obj1 = {
            id: null,
            name: null,
            age: 22,
            birthday: '1999-01-01'
        };

        const mergedObject = merge(obj1, obj2);

        expect(mergedObject).toEqual({
            id: 1,
            name: 'A name',
            age: 2,
            birthday: null
        });
    });
});

