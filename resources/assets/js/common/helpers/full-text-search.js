export default function (list, field, q, arrayField = null) {
    let filteredData = list;
    filteredData = [field].reduce(function (list, field) {
        return list.filter((item) => {
            item.toString();
            if (q.includes('é') || q.includes('í') || q.includes('á') ||
                q.includes('ó') || q.includes('ú')) {
                return eval('item.' + field).toString().toLowerCase().includes(q.toLowerCase());
            }

            // TODO: ALLOW TO SEARCH WITHIN AN OBJECT ARRAY
            if (Array.isArray(eval('item.' + field))) {
                return eval('item.' + field).filter((item2) => {
                    item2.toString();
                    if (q.includes('é') || q.includes('í') || q.includes('á') ||
                        q.includes('ó') || q.includes('ú')) {
                        return item2[arrayField].toString().toLowerCase().includes(q.toLowerCase());
                    }
                    return eval('item2.' + arrayField).toString().replace(new RegExp('é', 'g'), 'e').replace(new RegExp('á', 'g'), 'a').replace(new RegExp('í', 'g'), 'i').replace(new RegExp('ó', 'g'), 'o').replace(new RegExp('ú', 'g'), 'u').toLowerCase().includes(q.toLowerCase());
                }).length > 0;
            }
            return (eval('item.' + field).toString().replace(new RegExp('é', 'g'), 'e').replace(new RegExp('á', 'g'), 'a').replace(new RegExp('í', 'g'), 'i').replace(new RegExp('ó', 'g'), 'o').replace(new RegExp('ú', 'g'), 'u').toLowerCase().includes(q.toLowerCase()));
        });
    }, filteredData);

    return filteredData;
}
