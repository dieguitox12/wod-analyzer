import {addValidationRule} from 'formsy-react';
import * as Regex from 'common/constants/regex.constant';

/**
 * Adds all the formsy rules that we'll need.
 */
export default function addFormsyRules() {
    addValidationRule('isValidTime', (values, value) => {
        return Regex.isValidTime.test(value);
    });

    addValidationRule('isValidTimeOptional', (values, value) => {
        return Regex.isValidTime.test(value) || value === '';
    });
}

