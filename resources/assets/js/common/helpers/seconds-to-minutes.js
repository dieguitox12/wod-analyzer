import moment from 'moment';

export default function (seconds) {
    return moment('2015-01-01').startOf('day').seconds(seconds).format('mm:ss');
}
