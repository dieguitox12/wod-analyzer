import querystring from 'query-string';

/**
 * Parses a query string into an object.
 *
 * @param  string query  a query string. Example: "?foo=bar"
 * @return object     the given query's properties represented by an object.
 */
export default function (url) {
    return querystring.parse(url);
}
