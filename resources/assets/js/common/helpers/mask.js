export default function(value, pattern) {
    var i = 0,
        v = value.toString();
    return pattern.replace(/#/g, () => v[i++]);
}
