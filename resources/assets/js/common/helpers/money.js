export default function (n, fixed = true) {
    if (fixed) {
        return parseFloat(n).toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    }
    return parseFloat(n).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}
