import secondsToMinutes from './seconds-to-minutes';

export default function (wodScore) {
    let totalScore = '';
    if (wodScore.rounds) {
        totalScore = wodScore.wodScore.scores.reduce((prev, curr) => {
            return {value: prev.value + curr.value};
        }).value;
        totalScore = secondsToMinutes(totalScore);
    } else if (!wodScore.rounds && wodScore.timeCap) {
        let totalRepsPerRound = wodScore.movements.reduce((prev, curr) => {
            return {reps: prev.reps + curr.reps};
        }).reps;
        let totalReps = wodScore.wodScore.scores.reduce((prev, curr) => {
            return {value: prev.value + curr.value};
        }).value;
        if (totalRepsPerRound) {
            let rounds = Math.floor(totalReps / totalRepsPerRound);
            totalScore = `${rounds} rounds `;
            if (totalReps % totalRepsPerRound !== 0) {
                totalScore += `+ ${totalReps - (rounds * totalRepsPerRound)} reps`;
            }
        } else {
            totalScore = `${totalReps} reps`;
        }
    }
    return totalScore;
}
