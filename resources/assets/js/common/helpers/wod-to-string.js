import secondsToMinutes from './seconds-to-minutes';

export default function (wod) {
    let description = '';
    if (wod.rounds) {
        description = 'For Time: \n\n';
        if (wod.rounds > 1) {
            description = `${wod.rounds} Rounds For Time:\n\n`;
        }
    } else {
        description = `${secondsToMinutes(wod.timeCap)} min AMRAP:\n\n`;
    }

    wod.movements.forEach(element => {
        description += `${element.reps ? element.reps + ' ' : ''}${element.movement.name}`;
        if (element.manMeasure || element.womanMeasure) {
            if (element.manMeasure) {
                description += `, ${element.manMeasure.value} ${element.manMeasure.symbol}`;
            }
            if (element.womanMeasure) {
                description += `, ${element.womanMeasure.value} ${element.womanMeasure.symbol}\n`;
            } else {
                description += '\n';
            }
        } else {
            description += '\n';
        }
    });

    if (wod.rounds && wod.timeCap) {
        description += `\nTime Cap: ${secondsToMinutes(wod.timeCap)} min\n`;
    }

    return description;
}
