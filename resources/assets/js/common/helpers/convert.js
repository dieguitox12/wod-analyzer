export const downloadBlob = (data) => {
    var blob = new Blob([data], {type: 'text/csv'});
    let a = document.createElement("a");
    a.style = "display: none";
    document.body.appendChild(a);
    let url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = 'Agentes.csv';
    a.click();
    window.URL.revokeObjectURL(url);
};
