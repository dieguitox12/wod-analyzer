import mask from './mask';

describe('Mask', () => {

    test('phone mask', () => {
        expect(mask(8888888888, '(###) ###-####')).toEqual('(888) 888-8888');
    });

    test('identification mask', () => {
        expect(mask(34244553452, '###-#######-#')).toEqual('342-4455345-2');
    });

    test('rnc mask', () => {
        expect(mask(234334521, '###-#####-#')).toEqual('234-33452-1');
    });
});

