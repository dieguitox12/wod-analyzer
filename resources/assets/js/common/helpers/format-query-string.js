export default function (obj) {
    return Object.keys.reduce(function (str, key, i) {
        let delimiter, val;
        delimiter = (i === 0) ? '?' : '&';
        let newKey = encodeURIComponent(key);
        val = encodeURIComponent(obj[newKey]);
        return [str, delimiter, newKey, '=', val].join('');
    }, '');
}
