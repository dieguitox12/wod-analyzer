import moment from 'moment';

export default function (time) {
    if (time.length === 0) {
        return null;
    }
    return moment('00:' + time, 'HH:mm:ss').diff(moment().startOf('day'), 'seconds');
}
