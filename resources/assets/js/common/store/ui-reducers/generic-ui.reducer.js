/* User-related state changes goes here */

import PropTypes from 'prop-types';

import * as userActionsLabels from 'common/store/actions/user/user.enum';
import * as wodActionsLabels from 'common/store/actions/wod/wod.enum';

import merge from 'common/helpers/merge-object';

const initialState = Object.seal({
    loading: false,

    // Properties validation.
    propTypes: {loading: PropTypes.bool}
});

export const genericUIReducer = (state = initialState, action) => {
    let newState = state;
    switch (action.type) {
    case userActionsLabels.AUTH_USER:
    case userActionsLabels.EDIT_USER:
    case userActionsLabels.CREATE_USER:
    case wodActionsLabels.UPDATE_SCORE:
    case wodActionsLabels.CREATE_WOD:
    case wodActionsLabels.GET_OPTIMIZATIONS:
    case wodActionsLabels.DELETE_SCORE:
    case wodActionsLabels.LOG_SCORE:
    case wodActionsLabels.GET_MOVEMENTS:
    case userActionsLabels.LOGOUT_USER:
    case userActionsLabels.FORGOT_PASSWORD:
    case userActionsLabels.RESET_PASSWORD:
    case wodActionsLabels.GET_WODS: {
        const newStateObject = Object.assign({}, { loading: true });
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.EDIT_USER_FAILURE:
    case userActionsLabels.EDIT_USER_SUCCESS:
    case userActionsLabels.CREATE_USER_FAILURE:
    case userActionsLabels.CREATE_USER_SUCCESS:
    case wodActionsLabels.UPDATE_SCORE_FAILURE:
    case wodActionsLabels.UPDATE_SCORE_SUCCESS:
    case wodActionsLabels.CREATE_WOD_FAILURE:
    case wodActionsLabels.CREATE_WOD_SUCCESS:
    case wodActionsLabels.GET_OPTIMIZATIONS_FAILURE:
    case wodActionsLabels.GET_OPTIMIZATIONS_SUCCESS:
    case wodActionsLabels.DELETE_SCORE_FAILURE:
    case wodActionsLabels.DELETE_SCORE_SUCCESS:
    case wodActionsLabels.LOG_SCORE_FAILURE:
    case wodActionsLabels.LOG_SCORE_SUCCESS:
    case wodActionsLabels.GET_MOVEMENTS_FAILURE:
    case wodActionsLabels.GET_MOVEMENTS_SUCCESS:
    case userActionsLabels.FORGOT_PASSWORD_FAILURE:
    case userActionsLabels.FORGOT_PASSWORD_SUCCESS:
    case userActionsLabels.LOGOUT_USER_FAILURE:
    case userActionsLabels.LOGOUT_USER_SUCCESS:
    case userActionsLabels.RESET_PASSWORD_FAILURE:
    case userActionsLabels.RESET_PASSWORD_SUCCESS:
    case userActionsLabels.AUTH_USER_FAILURE:
    case userActionsLabels.AUTH_USER_SUCCESS:
    case wodActionsLabels.GET_WODS_SUCCESS:
    case wodActionsLabels.GET_WODS_FAILURE: {
        const newStateObject = Object.assign({}, { loading: false });
        newState = merge(state, newStateObject);
        break;
    }
    default:
        break;
    }
    return newState;
};
