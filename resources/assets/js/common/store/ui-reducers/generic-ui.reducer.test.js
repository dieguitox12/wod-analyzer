import * as userActions from 'common/store/actions/user/user.actions';
import { mockGenericUIState } from 'mock/generic-ui.mock';
import { mockUsers } from 'mock/user.mock';

import { genericUIReducer } from './generic-ui.reducer';

describe('Generi Ui Reducer', () => {

    test('authUser action', () => {
        expect(genericUIReducer(mockGenericUIState, userActions.authUser('username', 'password')))
            .toEqual({loading: true});
    });

    test('authUserSuccess action', () => {
        expect(genericUIReducer(mockGenericUIState, userActions.authUserSuccess(mockUsers[0])))
            .toEqual({loading: false});
    });

    test('authUserFailure action', () => {
        expect(genericUIReducer(mockGenericUIState, userActions.authUserFailure({ errors: { message: 'Error' } })))
            .toEqual({loading: false});
    });

    test('logoutUser action', () => {
        expect(genericUIReducer(mockGenericUIState, userActions.logoutUser()))
            .toEqual({loading: true});
    });

    test('logoutUserSuccess action', () => {
        expect(genericUIReducer(mockGenericUIState, userActions.logoutUserSuccess()))
            .toEqual({loading: false});
    });

    test('logoutUserFailure action', () => {
        expect(genericUIReducer(mockGenericUIState, userActions.logoutUserFailure({ errors: { message: 'Error' } })))
            .toEqual({loading: false});
    });

    test('forgotPassword action', () => {
        expect(genericUIReducer(mockGenericUIState, userActions.forgotPassword('email@example.com')))
            .toEqual({ loading: true });
    });

    test('forgotPasswordSuccess action', () => {
        expect(genericUIReducer(mockGenericUIState, userActions.forgotPasswordSuccess()))
            .toEqual({ loading: false });
    });

    test('forgotPasswordFailure action', () => {
        expect(genericUIReducer(mockGenericUIState, userActions.forgotPasswordFailure({ errors: { message: 'Error' } })))
            .toEqual({ loading: false });
    });

    test('resetPassword action', () => {
        expect(genericUIReducer(mockGenericUIState, userActions.resetPassword('email@example.com')))
            .toEqual({ loading: true });
    });

    test('resetPasswordSuccess action', () => {
        expect(genericUIReducer(mockGenericUIState, userActions.resetPasswordSuccess()))
            .toEqual({ loading: false });
    });

    test('resetPasswordFailure action', () => {
        expect(genericUIReducer(mockGenericUIState, userActions.resetPasswordFailure({ errors: { message: 'Error' } })))
            .toEqual({ loading: false });
    });

    test('createUser action', () => {
        expect(genericUIReducer(mockGenericUIState, userActions.createUser({name: 'John Doe'})))
            .toEqual({loading: true});
    });

    test('createUserSuccess action', () => {
        expect(genericUIReducer(mockGenericUIState, userActions.createUserSuccess(mockUsers[0])))
            .toEqual({loading: false});
    });

    test('createUserFailure action', () => {
        expect(genericUIReducer(mockGenericUIState, userActions.createUserFailure({ errors: { message: 'Error' } })))
            .toEqual({loading: false});
    });
});

