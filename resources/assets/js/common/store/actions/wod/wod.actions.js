
import * as wodActionsLabels from './wod.enum';

export const resetScoreMessages = () => ({type: wodActionsLabels.RESET_WOD_MESSAGES});

export const resetOptimizations = () => ({type: wodActionsLabels.RESET_OPTIMIZATIONS});

export const logScore = (data) => ({
    type: wodActionsLabels.LOG_SCORE,
    payload: data
});

export const logScoreSuccess = (data) => ({type: wodActionsLabels.LOG_SCORE_SUCCESS, payload: data});

export const logScoreFailure = (error) => ({
    type: wodActionsLabels.LOG_SCORE_FAILURE,
    payload: error
});

export const createWod = (data) => ({
    type: wodActionsLabels.CREATE_WOD,
    payload: data
});

export const createWodSuccess = (data) => ({
    type: wodActionsLabels.CREATE_WOD_SUCCESS,
    payload: data
});

export const createWodFailure = (error) => ({
    type: wodActionsLabels.CREATE_WOD_FAILURE,
    payload: error
});

export const updateScore = (id, data) => ({
    type: wodActionsLabels.UPDATE_SCORE,
    payload: {id, data}
});

export const updateScoreSuccess = (data) => ({
    type: wodActionsLabels.UPDATE_SCORE_SUCCESS,
    payload: data
});

export const updateScoreFailure = (error) => ({
    type: wodActionsLabels.UPDATE_SCORE_FAILURE,
    payload: error
});

export const getScore = (id) => ({
    type: wodActionsLabels.GET_SCORE,
    payload: id
});

export const getScoreSuccess = (score) => ({
    type: wodActionsLabels.GET_SCORE_SUCCESS,
    payload: score
});

export const getScoreFailure = (error) => ({
    type: wodActionsLabels.GET_SCORE_FAILURE,
    payload: error
});

export const getScores = (params) => ({
    type: wodActionsLabels.GET_SCORES,
    payload: params
});

export const getScoresSuccess = (scores) => ({
    type: wodActionsLabels.GET_SCORES_SUCCESS,
    payload: scores
});

export const getScoresFailure = (error) => ({
    type: wodActionsLabels.GET_SCORES_FAILURE,
    payload: error
});

export const getWods = () => ({type: wodActionsLabels.GET_WODS});

export const getWodsSuccess = (wods) => ({
    type: wodActionsLabels.GET_WODS_SUCCESS,
    payload: wods
});

export const getWodsFailure = (error) => ({
    type: wodActionsLabels.GET_WODS_FAILURE,
    payload: error
});

export const setFilters = (filters) => ({
    type: wodActionsLabels.SET_FILTERS,
    payload: filters
});

export const getMovements = () => ({type: wodActionsLabels.GET_MOVEMENTS});

export const getMovementsSuccess = (wods) => ({
    type: wodActionsLabels.GET_MOVEMENTS_SUCCESS,
    payload: wods
});

export const getMovementsFailure = (error) => ({
    type: wodActionsLabels.GET_MOVEMENTS_FAILURE,
    payload: error
});

export const deleteScore = (id) => ({
    type: wodActionsLabels.DELETE_SCORE,
    payload: id
});

export const deleteScoreSuccess = () => ({type: wodActionsLabels.DELETE_SCORE_SUCCESS});

export const deleteScoreFailure = (error) => ({
    type: wodActionsLabels.DELETE_SCORE_FAILURE,
    payload: error
});

export const getOptimizations = (id) => ({
    type: wodActionsLabels.GET_OPTIMIZATIONS,
    payload: id
});

export const getOptimizationsSuccess = (optimizations) => ({
    type: wodActionsLabels.GET_OPTIMIZATIONS_SUCCESS,
    payload: optimizations
});

export const getOptimizationsFailure = (error) => ({
    type: wodActionsLabels.GET_OPTIMIZATIONS_FAILURE,
    payload: error
});
