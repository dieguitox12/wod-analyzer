
import * as userActionsLabels from './user.enum';

export const authUser = (email, password) => ({
    type: userActionsLabels.AUTH_USER,
    payload: {email: email, password: password}
});

export const logoutUser = () => ({type: userActionsLabels.LOGOUT_USER});

export const authUserSuccess = (user) => ({ type: userActionsLabels.AUTH_USER_SUCCESS, payload: user });

export const authUserFailure = (error) => ({ type: userActionsLabels.AUTH_USER_FAILURE, payload: error });

export const logoutUserSuccess = () => ({ type: userActionsLabels.LOGOUT_USER_SUCCESS });

export const logoutUserFailure = (error) => ({ type: userActionsLabels.LOGOUT_USER_FAILURE, payload: error });

export const forgotPassword = (email) => ({
    type: userActionsLabels.FORGOT_PASSWORD,
    payload: { email: email }
});
export const forgotPasswordSuccess = () => ({ type: userActionsLabels.FORGOT_PASSWORD_SUCCESS });

export const forgotPasswordFailure = (error) => ({ type: userActionsLabels.FORGOT_PASSWORD_FAILURE, payload: error });


export const resetPassword = (password, passwordConfirmation, token) => ({
    type: userActionsLabels.RESET_PASSWORD,
    payload: {
        password: password,
        passwordConfirmation: passwordConfirmation,
        token: token
    }
});
export const resetPasswordSuccess = () => ({ type: userActionsLabels.RESET_PASSWORD_SUCCESS });

export const resetPasswordFailure = (error) => ({ type: userActionsLabels.RESET_PASSWORD_FAILURE, payload: error });

export const getNotifications = () => ({type: userActionsLabels.GET_NOTIFICATIONS});

export const getNotificationsSuccess = (notifications) => ({ type: userActionsLabels.GET_NOTIFICATIONS_SUCCESS, payload: notifications });

export const getNotificationsFailure = (error) => ({ type: userActionsLabels.GET_NOTIFICATIONS_FAILURE, payload: error });


export const markNotificationsAsSeen = () => ({type: userActionsLabels.MARK_NOTIFICATIONS_AS_SEEN});

export const markNotificationsAsSeenSuccess = () => ({ type: userActionsLabels.MARK_NOTIFICATIONS_AS_SEEN_SUCCESS });

export const markNotificationsAsSeenFailure = (error) => ({ type: userActionsLabels.MARK_NOTIFICATIONS_AS_SEEN_FAILURE, payload: error });


export const resetUserMessages = () => ({type: userActionsLabels.RESET_USER_MESSAGES});

export const createUser = (data) => ({
    type: userActionsLabels.CREATE_USER,
    payload: data
});

export const createUserSuccess = (user) => ({type: userActionsLabels.CREATE_USER_SUCCESS, payload: user});

export const createUserFailure = (error) => ({
    type: userActionsLabels.CREATE_USER_FAILURE,
    payload: error
});
