import { combineEpics } from 'redux-observable';
import * as userEpics from './user.epic';
import * as wodEpics from './wod.epic';

export const rootEpics = combineEpics(
    userEpics.authUserEpic,
    userEpics.logoutUserEpic,
    userEpics.forgotPasswordEpic,
    userEpics.resetPasswordEpic,
    userEpics.createUserEpic,
    wodEpics.logScoreEpic,
    wodEpics.getScoreEpic,
    wodEpics.getScoresEpic,
    wodEpics.getMovementsEpic,
    wodEpics.getWodsEpic,
    wodEpics.createWodEpic,
    wodEpics.deleteScoreEpic,
    wodEpics.getOptimizationsEpic,
    wodEpics.updateScoreEpic
);
