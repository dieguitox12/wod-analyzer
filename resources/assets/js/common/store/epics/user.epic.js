import { Observable } from 'rxjs/Observable';
import 'rxjs';
import 'rxjs/add/observable/of';

import * as userActions from 'common/store/actions/user/user.actions';
import * as userActionsLabels from 'common/store/actions/user/user.enum';
import * as userRequests from 'common/store/services/user.service';

export const authUserEpic = (action$, store) =>
    action$.ofType(userActionsLabels.AUTH_USER)
        .mergeMap(action =>
            userRequests.authUserRequest(action.payload.email, action.payload.password)
                .map(response => userActions.authUserSuccess(response.response))
                .catch(error => {
                    if (error.status === 403 || error.status === 401) {
                        store.dispatch(userActions.logoutUserSuccess());
                        return Observable.of(null);
                    }
                    return Observable.of(
                        userActions.authUserFailure(error.response)
                    );
                })
        );

export const logoutUserEpic = (action$, store) =>
    action$.ofType(userActionsLabels.LOGOUT_USER)
        .mergeMap(() =>
            userRequests.logoutUserRequest()
                .map(() => userActions.logoutUserSuccess())
                .catch(error => {
                    if (error.status === 403 || error.status === 401) {
                        store.dispatch(userActions.logoutUserSuccess());
                        return Observable.of(null);
                    }
                    return Observable.of(
                        userActions.logoutUserFailure(error.response)
                    );
                })
        );

export const forgotPasswordEpic = (action$, store) =>
    action$.ofType(userActionsLabels.FORGOT_PASSWORD)
        .mergeMap(action =>
            userRequests.forgotPasswordRequest(action.payload.email)
                .map(() => userActions.forgotPasswordSuccess())
                .catch(error => {
                    if (error.status === 403 || error.status === 401) {
                        store.dispatch(userActions.logoutUserSuccess());
                        return Observable.of(null);
                    }
                    return Observable.of(
                        userActions.forgotPasswordFailure(error.response)
                    );
                })
        );

export const resetPasswordEpic = (action$, store) =>
    action$.ofType(userActionsLabels.RESET_PASSWORD)
        .mergeMap(action =>
            userRequests.resetPasswordRequest(
                action.payload.password,
                action.payload.passwordConfirmation,
                action.payload.token
            )
                .map(() => userActions.resetPasswordSuccess())
                .catch(error => {
                    if (error.status === 403 || error.status === 401) {
                        store.dispatch(userActions.logoutUserSuccess());
                        return Observable.of(null);
                    }
                    return Observable.of(
                        userActions.resetPasswordFailure(error.response)
                    );
                })
        );

export const createUserEpic = (action$, store) =>
    action$.ofType(userActionsLabels.CREATE_USER)
        .mergeMap((action) =>
            userRequests.createUserRequest(action.payload)
                .map((result) => userActions.createUserSuccess(result.response))
                .catch(error => {
                    if (error.status === 403 || error.status === 401) {
                        store.dispatch(userActions.logoutUserSuccess());
                        return Observable.of(null);
                    }
                    return Observable.of(
                        userActions.createUserFailure(error.response)
                    );
                })
        );
