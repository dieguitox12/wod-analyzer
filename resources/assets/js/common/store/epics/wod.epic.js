import { Observable } from 'rxjs/Observable';
import 'rxjs';
import 'rxjs/add/observable/of';

import * as wodActions from 'common/store/actions/wod/wod.actions';
import * as wodActionsLabels from 'common/store/actions/wod/wod.enum';
import * as wodRequests from 'common/store/services/wod.service';
import * as userActions from 'common/store/actions/user/user.actions';

export const logScoreEpic = (action$, store) =>
    action$.ofType(wodActionsLabels.LOG_SCORE)
        .mergeMap((action) =>
            wodRequests.logScoreRequest(action.payload)
                .map((result) => wodActions.logScoreSuccess(result.response))
                .catch(error => {
                    if (error.status === 403 || error.status === 401) {
                        store.dispatch(userActions.logoutUserSuccess());
                        return Observable.of(null);
                    }
                    return Observable.of(
                        wodActions.logScoreFailure(error.response)
                    );
                })
        );

export const createWodEpic = (action$, store) =>
    action$.ofType(wodActionsLabels.CREATE_WOD)
        .mergeMap((action) =>
            wodRequests.createWodRequest(action.payload)
                .map((result) => wodActions.createWodSuccess(result.response))
                .catch(error => {
                    if (error.status === 403 || error.status === 401) {
                        store.dispatch(userActions.logoutUserSuccess());
                        return Observable.of(null);
                    }
                    return Observable.of(
                        wodActions.createWodFailure(error.response)
                    );
                })
        );

export const getScoreEpic = (action$, store) =>
    action$.ofType(wodActionsLabels.GET_SCORE)
        .mergeMap((action) =>
            wodRequests.getScoreRequest(action.payload)
                .map((result) => wodActions.getScoreSuccess(result))
                .catch(error => {
                    if (error.status === 403 || error.status === 401) {
                        store.dispatch(userActions.logoutUserSuccess());
                        return Observable.of(null);
                    }
                    return Observable.of(
                        wodActions.getScoreFailure(error.response)
                    );
                })
        );

export const getScoresEpic = (action$, store) =>
    action$.ofType(wodActionsLabels.GET_SCORES)
        .mergeMap((action) =>
            wodRequests.getScoresRequest(action.payload)
                .map((results) => wodActions.getScoresSuccess(results))
                .catch(error => {
                    if (error.status === 403 || error.status === 401) {
                        store.dispatch(userActions.logoutUserSuccess());
                        return Observable.of(null);
                    }
                    return Observable.of(
                        wodActions.getScoresFailure(error.response)
                    );
                })
        );

export const getWodsEpic = (action$, store) =>
    action$.ofType(wodActionsLabels.GET_WODS)
        .mergeMap(() =>
            wodRequests.getWodsRequest()
                .map((results) => wodActions.getWodsSuccess(results))
                .catch(error => {
                    if (error.status === 403 || error.status === 401) {
                        store.dispatch(userActions.logoutUserSuccess());
                        return Observable.of(null);
                    }
                    return Observable.of(
                        wodActions.getWodsFailure(error.response)
                    );
                })
        );

export const getMovementsEpic = (action$, store) =>
    action$.ofType(wodActionsLabels.GET_MOVEMENTS)
        .mergeMap(() =>
            wodRequests.getMovementsRequest()
                .map((results) => wodActions.getMovementsSuccess(results))
                .catch(error => {
                    if (error.status === 403 || error.status === 401) {
                        store.dispatch(userActions.logoutUserSuccess());
                        return Observable.of(null);
                    }
                    return Observable.of(
                        wodActions.getMovementsFailure(error.response)
                    );
                })
        );

export const deleteScoreEpic = (action$, store) =>
    action$.ofType(wodActionsLabels.DELETE_SCORE)
        .mergeMap((action) =>
            wodRequests.deleteScoreRequest(action.payload)
                .map(() => wodActions.deleteScoreSuccess())
                .catch(error => {
                    if (error.status === 403 || error.status === 401) {
                        store.dispatch(userActions.logoutUserSuccess());
                        return Observable.of(null);
                    }
                    return Observable.of(
                        wodActions.deleteScoreFailure(error.response)
                    );
                })
        );

export const getOptimizationsEpic = (action$, store) =>
    action$.ofType(wodActionsLabels.GET_OPTIMIZATIONS)
        .mergeMap((action) =>
            wodRequests.getOptimizationsRequest(action.payload)
                .map((result) => wodActions.getOptimizationsSuccess(result))
                .catch(error => {
                    if (error.status === 403 || error.status === 401) {
                        store.dispatch(userActions.logoutUserSuccess());
                        return Observable.of(null);
                    }
                    return Observable.of(
                        wodActions.getOptimizationsFailure(error.response)
                    );
                })
        );

export const updateScoreEpic = (action$, store) =>
    action$.ofType(wodActionsLabels.UPDATE_SCORE)
        .mergeMap((action) =>
            wodRequests.updateScoreRequest(action.payload)
                .map((data) => wodActions.updateScoreSuccess(data))
                .catch(error => {
                    if (error.status === 403 || error.status === 401) {
                        store.dispatch(userActions.logoutUserSuccess());
                        return Observable.of(null);
                    }
                    return Observable.of(
                        wodActions.updateScoreFailure(error.response)
                    );
                })
        );
