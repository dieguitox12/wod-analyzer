import { combineReducers } from 'redux';
import { userReducer } from './user.reducer';
import { wodReducer } from './wod.reducer';

import { genericUIReducer } from 'common/store/ui-reducers/generic-ui.reducer';

export const rootReducers = combineReducers({
    userReducer,
    wodReducer,
    genericUIReducer
});
