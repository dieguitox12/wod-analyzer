import PropTypes from 'prop-types';
import moment from 'moment';

import * as wodActionsLabels from 'common/store/actions/wod/wod.enum';
import merge from 'common/helpers/merge-object';

export const wodInitialState = Object.seal({
    loading: false,
    singleScore: null,
    error: null,
    filters: {
        q: '',
        from: moment().subtract(1, 'months').format('YYYY-MM-DD'),
        to: moment().format('YYYY-MM-DD')
    },
    simpleError: null,
    movements: [],
    optimizations: null,
    singleWods: [],
    scores: [],
    scoreCreated: null,
    scoreDeleted: null,
    scoreUpdated: null,

    // Properties validation.
    propTypes: {
        simpleError: PropTypes.string,
        error: PropTypes.string,
        loading: PropTypes.bool,
        filters: PropTypes.object,
        optimizations: PropTypes.object,
        singleScore: PropTypes.object,
        scores: PropTypes.array,
        movements: PropTypes.array,
        singleWods: PropTypes.array,
        scoreCreated: PropTypes.number
    }
});

export const wodReducer = (state = wodInitialState, action) => {
    let newState = state;
    switch (action.type) {
    case wodActionsLabels.SET_FILTERS: {
        const newStateObject = Object.assign({}, {filters: action.payload});
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.RESET_WOD_MESSAGES: {
        const newStateObject = Object.assign({}, { scoreCreated: null, scoreUpdated: null, scoreDeleted: null});
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.RESET_OPTIMIZATIONS: {
        const newStateObject = Object.assign({}, { optimizations: null });
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.CREATE_WOD: {
        const newStateObject = Object.assign({}, { simpleError: null, scoreCreated: null });
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.CREATE_WOD_SUCCESS: {
        const newStateObject = Object.assign({}, { simpleError: null, scoreCreated: action.payload});
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.CREATE_WOD_FAILURE: {
        let error = 'Internal error. Try later.';
        if (action.payload.error) {
            error = action.payload.error;
        } else {
            error = action.payload;
        }
        const newStateObject = Object.assign({}, { scoreCreated: null, simpleError: error});
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.LOG_SCORE: {
        const newStateObject = Object.assign({}, { simpleError: null, scoreCreated: null });
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.LOG_SCORE_SUCCESS: {
        const newStateObject = Object.assign({}, { simpleError: null, scoreCreated: action.payload});
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.LOG_SCORE_FAILURE: {
        let error = 'Internal error. Try later.';
        if (action.payload.error) {
            error = action.payload.error;
        } else {
            error = action.payload;
        }
        const newStateObject = Object.assign({}, { scoreCreated: null, simpleError: error});
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.GET_SCORE: {
        const newStateObject = Object.assign({}, { error: null, loading: true});
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.GET_SCORE_SUCCESS: {
        const newStateObject = Object.assign({}, {
            error: null,
            loading: false,
            singleScore: action.payload
        });
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.GET_SCORE_FAILURE: {
        let error = 'Internal error. Try later.';
        if (action.payload.error) {
            error = action.payload.error;
        } else {
            error = action.payload;
        }
        const newStateObject = Object.assign({}, {
            loading: false,
            error: error
        });
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.GET_SCORES: {
        const newStateObject = Object.assign({}, { error: null, loading: true});
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.GET_SCORES_SUCCESS: {
        let newScores = action.payload.slice().map(element => {
            let newScore = Object.assign({}, element);
            newScore['previousScores'] = [];
            action.payload.forEach(element2 => {
                if (element2.wodScore.id !== element.wodScore.id && element2.id === element.id) {
                    newScore['previousScores'].push(element2);
                }
            });
            return newScore;
        });
        const newStateObject = Object.assign({}, {
            error: null,
            loading: false,
            scores: newScores,
            scoreCreated: false
        });
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.GET_SCORES_FAILURE: {
        let error = 'Internal error. Try later.';
        if (action.payload.error) {
            error = action.payload.error;
        } else {
            error = action.payload;
        }
        const newStateObject = Object.assign({}, {
            loading: false,
            scoreCreated: false,
            error: error
        });
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.GET_WODS: {
        const newStateObject = Object.assign({}, { simpleError: null });
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.GET_WODS_SUCCESS: {
        const newStateObject = Object.assign({}, {
            simpleError: null,
            singleWods: action.payload.map(element => {
                let newElement = Object.assign({}, element);
                // newElement.name = newElement.name + ' (' + newElement.description + ')';
                return newElement;
            })
        });
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.GET_WODS_FAILURE: {
        let error = 'Internal error. Try later.';
        if (action.payload.error) {
            error = action.payload.error;
        } else {
            error = action.payload;
        }
        const newStateObject = Object.assign({}, {simpleError: error});
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.GET_MOVEMENTS: {
        const newStateObject = Object.assign({}, { simpleError: null });
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.GET_MOVEMENTS_SUCCESS: {
        const newStateObject = Object.assign({}, {
            simpleError: null,
            movements: action.payload
        });
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.GET_MOVEMENTS_FAILURE: {
        let error = 'Internal error. Try later.';
        if (action.payload.error) {
            error = action.payload.error;
        } else {
            error = action.payload;
        }
        const newStateObject = Object.assign({}, {simpleError: error});
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.DELETE_SCORE: {
        const newStateObject = Object.assign({}, { simpleError: null, scoreDeleted: false});
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.DELETE_SCORE_SUCCESS: {
        const newStateObject = Object.assign({}, {
            simpleError: null,
            scoreDeleted: true
        });
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.DELETE_SCORE_FAILURE: {
        let error = 'Internal error. Try later.';
        if (action.payload.error) {
            error = action.payload.error;
        } else {
            error = action.payload;
        }
        const newStateObject = Object.assign({}, {simpleError: error});
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.GET_OPTIMIZATIONS: {
        const newStateObject = Object.assign({}, { simpleError: null });
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.GET_OPTIMIZATIONS_SUCCESS: {
        const newStateObject = Object.assign({}, {
            simpleError: null,
            optimizations: action.payload
        });
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.GET_OPTIMIZATIONS_FAILURE: {
        let error = 'Internal error. Try later.';
        if (action.payload.error) {
            error = action.payload.error;
        } else {
            error = action.payload;
        }
        const newStateObject = Object.assign({}, {simpleError: error});
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.UPDATE_SCORE: {
        const newStateObject = Object.assign({}, { simpleError: null, scoreUpdated: false});
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.UPDATE_SCORE_SUCCESS: {
        const newStateObject = Object.assign({}, {
            singleScore: action.payload.data,
            simpleError: null,
            scoreUpdated: true
        });
        newState = merge(state, newStateObject);
        break;
    }
    case wodActionsLabels.UPDATE_SCORE_FAILURE: {
        let error = 'Internal error. Try later.';
        if (action.payload.error) {
            error = action.payload.error;
        } else {
            error = action.payload;
        }
        const newStateObject = Object.assign({}, {simpleError: error});
        newState = merge(state, newStateObject);
        break;
    }
    default:
        break;
    }
    return newState;
};
