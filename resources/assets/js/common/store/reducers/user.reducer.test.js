import * as userActions from 'common/store/actions/user/user.actions';
import { mockUserState, mockUsers } from 'mock/user.mock';

import { userReducer } from './user.reducer';

describe('User Reducer', () => {

    test('authUser action', () => {
        expect(userReducer(mockUserState, userActions.authUser('username', 'password')))
            .toMatchObject({
                authUser: null,
                error: null
            });
    });

    test('authUserSuccess action', () => {
        let state = Object.assign({}, mockUserState);
        expect(userReducer(state, userActions.authUserSuccess(mockUsers[0])))
            .toMatchObject({
                authUser: mockUsers[0],
                error: null
            });
    });

    test('authUserFailure action', () => {
        expect(userReducer(mockUserState, userActions.authUserFailure({error: 'Error'})))
            .toMatchObject({
                authUser: null,
                error: 'Error'
            });
    });

    test('logoutUser action', () => {
        let state = Object.assign({}, mockUserState);
        state['authUser'] = Object.assign({}, mockUsers[0]);
        expect(userReducer(state, userActions.logoutUser()))
            .toMatchObject({
                authUser: mockUsers[0],
                error: null
            });
    });

    test('logoutUserSuccess action', () => {
        expect(userReducer(mockUserState, userActions.logoutUserSuccess()))
            .toMatchObject({
                authUser: null,
                error: null
            });

        expect(JSON.parse(localStorage.getItem('logoutUser'))).toEqual(null);
    });

    test('logoutUserFailure action', () => {
        let state = Object.assign({}, mockUserState);
        state['authUser'] = Object.assign({}, mockUsers[0]);
        expect(userReducer(state, userActions.logoutUserFailure({ error: 'Error' })))
            .toMatchObject({
                authUser: mockUsers[0],
                error: 'Error'
            });
    });

    test('forgotPassword action', () => {
        expect(userReducer(mockUserState, userActions.forgotPassword('email@example.com')))
            .toMatchObject({
                error: null,
                emailSent: false
            });
    });

    test('forgotPasswordSuccess action', () => {
        expect(userReducer(mockUserState, userActions.forgotPasswordSuccess()))
            .toMatchObject({
                error: null,
                emailSent: true
            });
    });

    test('forgotPasswordFailure action', () => {
        expect(userReducer(mockUserState, userActions.forgotPasswordFailure({ error: 'Error' })))
            .toMatchObject({
                error: 'Error',
                emailSent: false
            });
    });

    test('resetPassword action', () => {
        expect(userReducer(mockUserState, userActions.resetPassword('password', 'password', 'token')))
            .toMatchObject({
                error: null,
                passwordChanged: false
            });
    });

    test('resetPasswordSuccess action', () => {
        expect(userReducer(mockUserState, userActions.resetPasswordSuccess()))
            .toMatchObject({
                error: null,
                passwordChanged: true
            });
    });

    test('resetPasswordFailure action', () => {
        expect(userReducer(mockUserState, userActions.resetPasswordFailure({ error:  'Error' })))
            .toMatchObject({
                error: 'Error',
                passwordChanged: false
            });
    });

    test('createUser action', () => {
        let state = Object.assign({}, mockUserState);
        expect(userReducer(state, userActions.createUser({name: 'John Doe'})))
            .toMatchObject({
                error: null,
                authUser: null
            });
    });

    test('createUserSuccess action', () => {
        let state = Object.assign({}, mockUserState);
        expect(userReducer(state, userActions.createUserSuccess({id: 1})))
            .toMatchObject({
                error: null,
                authUser: {id: 1}
            });
    });

    test('createUserFailure action', () => {
        let state = Object.assign({}, mockUserState);
        expect(userReducer(state, userActions.createUserFailure({ error: 'Error' })))
            .toMatchObject({
                error: 'Error',
                authUser: null
            });
    });
});

