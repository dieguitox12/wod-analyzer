/* User-related state changes goes here */

import PropTypes from 'prop-types';

import * as userActionsLabels from 'common/store/actions/user/user.enum';
import merge from 'common/helpers/merge-object';

export const userInitialState = Object.seal({
    loading: false,
    authUser: null,
    error: null,
    simpleError: null,
    emailSent: false,
    userUpdated: false,
    notifications: [],
    passwordChanged: false,

    // Properties validation.
    propTypes: {
        loading: PropTypes.bool,
        authUser: PropTypes.object,
        error: PropTypes.string,
        simpleError: PropTypes.string,
        emailSent: PropTypes.bool,
        passwordChanged: PropTypes.bool
    }
});

export const userReducer = (state = userInitialState, action) => {
    let newState = state;
    switch (action.type) {
    case userActionsLabels.AUTH_USER: {
        const newStateObject = Object.assign({}, { error: null });
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.AUTH_USER_SUCCESS: {
        const newStateObject = Object.assign({}, { authUser: action.payload, error: null });
        // localStorage.setItem('authUser', JSON.stringify(action.payload));
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.AUTH_USER_FAILURE: {
        let error = 'Internal error. Try later.';
        if (action.payload.error) {
            error = action.payload.error;
        } else {
            error = action.payload;
        }
        const newStateObject = Object.assign({}, {error: error});
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.LOGOUT_USER: {
        const newStateObject = Object.assign({}, { error: null });
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.LOGOUT_USER_SUCCESS: {
        const newStateObject = Object.assign({}, { authUser: null, error: null });
        newState = merge(state, newStateObject);
        // location.reload();
        break;
    }
    case userActionsLabels.LOGOUT_USER_FAILURE: {
        let error = 'Internal error. Try later.';
        if (action.payload.error) {
            error = action.payload.error;
        } else {
            error = action.payload;
        }
        const newStateObject = Object.assign({}, {error: error});
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.FORGOT_PASSWORD: {
        const newStateObject = Object.assign({}, { error: null, emailSent: false });
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.FORGOT_PASSWORD_SUCCESS: {
        const newStateObject = Object.assign({}, { emailSent: true, error: null });
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.FORGOT_PASSWORD_FAILURE: {
        let error = 'Internal error. Try later.';
        if (action.payload.error) {
            error = action.payload.error;
        } else {
            error = action.payload;
        }
        const newStateObject = Object.assign({}, {error: error, emailSent: false});
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.RESET_PASSWORD: {
        const newStateObject = Object.assign({}, { error: null, passwordChanged: false });
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.RESET_PASSWORD_SUCCESS: {
        const newStateObject = Object.assign({}, { passwordChanged: true, error: null });
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.RESET_PASSWORD_FAILURE: {
        let error = 'Internal error. Try later.';
        if (action.payload.error) {
            error = action.payload.error;
        } else {
            error = action.payload;
        }
        const newStateObject = Object.assign({}, {error: error, passwordChanged: false});
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.RESET_USER_MESSAGES: {
        const newStateObject = Object.assign({}, { userUpdated: false });
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.GET_NOTIFICATIONS_SUCCESS: {
        let notifications = [];
        if (action.payload !== null) {
            notifications = action.payload;
        }

        const newStateObject = Object.assign({}, { notifications: notifications });
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.MARK_NOTIFICATIONS_AS_SEEN_SUCCESS: {
        let notifications = state.notifications.slice().map(element => {
            let newElement = Object.assign({}, element);
            newElement.seen = true;
            return newElement;
        });

        const newStateObject = Object.assign({}, { notifications: notifications });
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.EDIT_USER: {
        const newStateObject = Object.assign({}, { simpleError: null, userUpdated: false });
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.EDIT_USER_SUCCESS: {
        const newStateObject = Object.assign({}, { simpleError: null, userUpdated: true });
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.EDIT_USER_FAILURE: {
        const newStateObject = Object.assign({}, { userUpdated: false, simpleError: action.payload ? action.payload.error : 'Internal error. Try later.'});
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.CREATE_USER: {
        const newStateObject = Object.assign({}, { error: null });
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.CREATE_USER_SUCCESS: {
        const newStateObject = Object.assign({}, { error: null, authUser: action.payload});
        newState = merge(state, newStateObject);
        break;
    }
    case userActionsLabels.CREATE_USER_FAILURE: {
        let error = 'Internal error. Try later.';
        if (action.payload.error) {
            error = action.payload.error;
        } else {
            error = action.payload;
        }
        const newStateObject = Object.assign({}, { authUser: null, error: error});
        newState = merge(state, newStateObject);
        break;
    }
    default:
        break;
    }
    return newState;
};
