import * as wodActions from 'common/store/actions/wod/wod.actions';
import {
    mockWodState,
    mockWodsDetails
} from 'mock/wod.mock';

import { wodReducer } from './wod.reducer';

describe('Wod Reducer', () => {
    test('getScoresSuccess action', () => {
        let state = Object.assign({}, mockWodState);
        expect(wodReducer(state, wodActions.getScoresSuccess(mockWodsDetails)))
            .toMatchObject({
                scores: mockWodsDetails,
                error: null
            });
    });
});

