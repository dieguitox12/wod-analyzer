import { ajax } from 'rxjs/observable/dom/ajax';

export function post(url, body) {
    return ajax.post(
        url,
        body
    );
}

export function deleteRequest(url) {
    return ajax.delete(
        url
    );
}

export function get(url) {
    return ajax.getJSON(
        url
    );
}

export function downloadPost(url, body) {
    let ajaxObject = {
        body: body,
        url: url,
        method: 'POST',
        responseType: 'blob'
    };

    return ajax(ajaxObject);
}

