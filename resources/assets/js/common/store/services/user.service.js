import parseBody from 'common/helpers/parse-body';

import {
    post
} from './api.service';


export function authUserRequest(email, password) {
    const formData = new FormData();

    parseBody(formData, {
        email: email,
        password: password,
        rememberMe: true
    });

    return post(
        'json/auth/login',
        formData
    );
}

export function createUserRequest(data) {
    const formData = new FormData();

    parseBody(formData, data);

    return post(
        'json/auth/register',
        formData
    );
}

export function logoutUserRequest() {
    return post(
        'json/auth/logout'
    );
}

export function forgotPasswordRequest(email) {
    const formData = new FormData();

    parseBody(formData, {email: email});

    return post(
        'json/auth/forgot',
        formData
    );
}

export function resetPasswordRequest(password, passwordConfirmation, token) {
    const formData = new FormData();

    parseBody(formData, {
        password: password,
        token: token,
        passwordConfirmation: passwordConfirmation
    });


    return post(
        `json/auth/reset`,
        formData
    );
}
