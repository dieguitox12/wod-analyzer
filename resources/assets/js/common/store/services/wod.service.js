import parseBody from 'common/helpers/parse-body';
import formatQueryString from 'common/helpers/format-query-string';

import {
    post,
    get,
    deleteRequest
} from './api.service';


export function logScoreRequest(data) {
    const formData = new FormData();

    parseBody(formData, data);

    return post(
        '/json/wod/score',
        formData
    );
}

export function createWodRequest(data) {
    const formData = new FormData();

    parseBody(formData, data);

    return post(
        '/json/wod/create',
        formData
    );
}

export function updateScoreRequest(data) {
    const formData = new FormData();

    parseBody(formData, data.data);

    return post(
        `/json/wod/score/edit/${data.id}`,
        formData
    );
}

export function deleteScoreRequest(id) {
    return deleteRequest(
        `/json/wod/score/${id}`
    );
}

export function getOptimizationsRequest(id) {
    return get(
        `/json/wod/optimize/${id}`
    );
}

export function getScoreRequest(id) {
    return get(
        `/json/wod/score/${id}`
    );
}

export function getScoresRequest(params) {
    let queryString = '';
    if (params) {
        queryString = formatQueryString(params);
    }

    return get(
        `/json/wod/search/${queryString}`
    );
}


export function getMovementsRequest() {
    return get(
        `/json/wod/movements`
    );
}

export function getWodsRequest() {
    return get(
        `/json/wod/all`
    );
}
