import { createSelector } from 'reselect';
import * as _ from 'lodash';
import moment from 'moment';

import fullTextSearch from 'common/helpers/full-text-search';

export const getScoreCreated = state => state.wodReducer.scoreCreated;
export const getScoreDeleted = state => state.wodReducer.scoreDeleted;
export const getScoreUpdated = state => state.wodReducer.scoreUpdated;
export const getWodReducerSimpleError = state => state.wodReducer.simpleError;
export const getWodReducerError = state => state.wodReducer.error;
export const getIsLoading = state => state.wodReducer.loading;
export const getFilters = state => state.wodReducer.filters;
export const getScoreProfile = state => state.wodReducer.singleScore;
export const getOptimizations = state => state.wodReducer.optimizations;
export const getMovements = state => state.wodReducer.movements;
export const getWods = state => state.wodReducer.singleWods;
export const getAllScores = state => state.wodReducer.scores;

export const getFilteredScores = () => {
    return createSelector(
        [getFilters, getAllScores],
        (filters, scores) => {
            let newScores = scores.slice();
            let filteredScores1 = [];
            let filteredScores2 = [];

            filteredScores1 = fullTextSearch(newScores, 'name', filters.q);
            // filteredScores2 = fullTextSearch(newScores, 'movements', filters.q, 'movement.name');

            let filteredScores = _.union(
                filteredScores1,
                filteredScores2
            );

            if (filters.from) {
                filteredScores = filteredScores.fastFilter(element => !moment(element.createdAt).isBefore(moment(filters.from)));
            }

            if (filters.to) {
                filteredScores = filteredScores.fastFilter(element => !moment(element.createdAt).isAfter(moment(filters.to)));
            }
            return filteredScores;
        }
    );
};
