export const getAuthUser = state => state.userReducer.authUser;
export const getUserReducerError = state => state.userReducer.error;
export const getUserReducerSimpleError = state => state.userReducer.simpleError;
export const getNewPasswordLink = state => state.userReducer.newPasswordLink;
export const getIsLoading = state => state.userReducer.loading;
export const getUserReducerEmailSent = state => state.userReducer.emailSent;
export const getPasswordChanged = state => state.userReducer.passwordChanged;
export const getUserUpdated = state => state.userReducer.userUpdated;
