import React from 'react';
import renderer from 'react-test-renderer';

import Badge from './';

describe('Badge Component', () => {
    test('renders without crashing without count', () => {
        const tree = renderer
            .create(
                <Badge />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    test('renders without crashing with count', () => {
        const tree = renderer
            .create(
                <Badge value={3}/>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    test('renders without crashing with count and icon', () => {
        const tree = renderer
            .create(
                <Badge value={3} icon={'icon'} />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

