import React from 'react';
import PropTypes from 'prop-types';

const Badge = props => (
    <div>
        <div style={props.style} className={`badge ${props.value ? 'badge--active' : ''}`}>
            {props.value}
        </div>
        {props.icon &&
            <i className={`mdi--badge ${props.icon}`} />
        }
    </div>
);

Badge.propTypes = {
    value: PropTypes.number,
    icon: PropTypes.string
};

export default Badge;
