import React from 'react';
import renderer from 'react-test-renderer';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import moment from 'moment';

import ColoredBullet from './';

configure({ adapter: new Adapter() });

describe('ColoredBullet Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <ColoredBullet hasPayed={true} />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
    test('getColor method if has payed this year', () => {
        const wrapper = shallow(
            <ColoredBullet date={moment().format('YYYY-MM-DD')} hasPayed={true}/>
        );
        expect(wrapper.instance().getColor()).toEqual('green');
    });
    test('getColor method if has payed previous year', () => {
        const wrapper = shallow(
            <ColoredBullet date={moment().subtract(1, 'years').format('YYYY-MM-DD')} hasPayed={true}/>
        );
        expect(wrapper.instance().getColor()).toEqual('yellow');
    });

    test('getColor method if has not payed and have more than 3 days to pay', () => {
        var d = new Date();
        d.setDate(d.getDate() + 4);

        const wrapper = shallow(
            <ColoredBullet
                hasPayed={false}
                date={d} />
        );
        expect(wrapper.instance().getColor()).toEqual('green');
    });

    test('getColor method if has not payed and have less than 3 days to pay', () => {
        var d = new Date();
        d.setDate(d.getDate() + 2);
        const wrapper = shallow(
            <ColoredBullet
                hasPayed={false}
                date={d} />
        );
        expect(wrapper.instance().getColor()).toEqual('yellow');
    });

    test('getColor method if has not payed and is in the last day to pay', () => {
        var d = new Date();
        const wrapper = shallow(
            <ColoredBullet
                hasPayed={false}
                date={d} />
        );
        expect(wrapper.instance().getColor()).toEqual('yellow');
    });

    test('getColor method if has not payed and is delayed', () => {
        var d = new Date();
        d.setDate(d.getDate() - 2);
        const wrapper = shallow(
            <ColoredBullet
                hasPayed={false}
                date={d} />
        );
        expect(wrapper.instance().getColor()).toEqual('red');
    });

    test('getColor method if has payed and is delayed', () => {
        var d = new Date();
        d.setDate(d.getDate() - 2);
        const wrapper = shallow(
            <ColoredBullet
                hasPayed={true}
                date={d} />
        );
        expect(wrapper.instance().getColor()).toEqual('green');
    });
});

