import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

export default class ColoredBullet extends React.PureComponent {

    constructor(props) {
        super(props);

        this.getColor = this.getColor.bind(this);
    }

    getColor() {
        let now = moment();
        let paymentDate = moment(this.props.date);
        if (this.props.hasPayed && paymentDate.year() === now.year()) {
            return 'green';
        }
        paymentDate.year(now.year());
        let diff = paymentDate.diff(now, 'days');

        if (diff < 0) {
            if (this.props.hasPayed) {
                return 'green';
            }
            return 'red';
        } else if (diff >= 0 && diff < 3) {
            return 'yellow';
        }
        return 'green';
    }

    render() {
        return (
            <div className="layout-bullet">
                <div className="bullet" style={{backgroundColor: `${this.getColor()}`}}></div>
                { this.props.children }
            </div>
        );
    }
}

ColoredBullet.propTypes = {
    date: PropTypes.string,
    hasPayed: PropTypes.bool
};
