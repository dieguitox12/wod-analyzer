import React from 'react';
import PropTypes from 'prop-types';
import ReactDropzone from 'react-dropzone';

import ImagePreview from 'common/components/image-preview';


/**
 * Extract basic information from the dispensary.
 */
export default class Dropzone extends React.PureComponent {

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <div>
                <ReactDropzone
                    maxSize={this.props.maxSize}
                    onDropRejected={this.props.onDropRejected}
                    accept={this.props.accept}
                    className={`dropzone__instance ${this.props.className} ${this.props.multiple ? "dropzone__instance--multiple" : "dropzone__instance--single"} ${this.props.selectedImage ? "dropzone__instance--selected" : ""}`}
                    activeClassName="dropzone__instance--active"
                    onDrop={this.props.onDrop}
                    multiple={this.props.multiple}
                    disabled={this.props.disabled}>
                    <div className="dropzone__message">
                        {this.props.children}
                    </div>

                </ReactDropzone>
                {this.props.showSlick &&
                this.props.selectedImage && <ImagePreview image={this.props.selectedImage} />}
            </div>
        );
    }
}

Dropzone.defaultProps = {
    className: "",
    multiple: true,
    disabled: false,
    showSlick: true
};

Dropzone.propTypes = {
    onDropRejected: PropTypes.func,
    maxSize: PropTypes.number,
    showSlick: PropTypes.bool,
    multiple: PropTypes.bool,
    accept: PropTypes.string,
    className: PropTypes.string,
    onDrop: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
    disabled: PropTypes.bool.isRequired,
    selectedImage: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(File)])
};
