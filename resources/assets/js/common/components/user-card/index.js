import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Toggle from 'react-toggle';

import {
    USERTYPES_LABELS,
    NOTES
} from 'common/constants/enums.constant';
import {
    WORLD_ICON,
    SYSTEM_ICON
} from 'common/constants/icon.constant';
import UserSuspensionNotesForm from 'containers/users/user-detail/user-account-details/user-status-form/user-suspension-notes-form';

import {
    ConfirmModal,
    BaseModal
} from '../modals';

export default class UserCard extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
            note: ''
        };

        this.onClick = this.onClick.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onAcceptUser = this.onAcceptUser.bind(this);
        this.onUserIsActiveChange = this.onUserIsActiveChange.bind(this);
        this.onUserPublishedChange = this.onUserPublishedChange.bind(this);
        this.onShowSuspensionNotesModal = this.onShowSuspensionNotesModal.bind(this);
        this.onSendNote = this.onSendNote.bind(this);
        this.onNoteChange = this.onNoteChange.bind(this);
    }

    onUserPublishedChange(e) {
        e.persist();
        const checked = e.currentTarget.checked;
        ReactDOM.render(
            <ConfirmModal
                title="Publicar en RE/MAX RD e Internacional"
                text="¿Está seguro que desea cambiar el estado de publicación de este usuario?"
                onSuccess={() => this.props.publishUser(this.props.value, checked ? 1 : 0)}
                onCancel={this.onCancel}
            />
            , document.getElementById('modals')
        );
    }

    onCancel() {
        ReactDOM.unmountComponentAtNode(document.getElementById('modals'));
    }

    onShowSuspensionNotesModal(e) {
        if (e.currentTarget.checked) {
            this.onUserIsActiveChange(e);
        } else {
            this.setState({showModal: !this.state.showModal});
        }
    }

    onSendNote() {
        const e = {currentTarget: {checked: false}};
        this.onUserIsActiveChange(e);
        this.props.onSendNote(this.props.value, this.state.note);
        this.onShowSuspensionNotesModal(e);
        this.onNoteChange('');
    }

    onNoteChange(value) {
        this.setState({note: value});
    }

    renderSuspensionNotesModal() {
        return (
            <BaseModal
                onModalClose={this.onShowSuspensionNotesModal}
                title="Acceso al sistema"
                showModal={this.state.showModal}>
                <UserSuspensionNotesForm
                    note={this.state.note}
                    options={[
                        {id: NOTES.NO_PAYMENT, name: NOTES.NO_PAYMENT},
                        {id: NOTES.HEALTH, name: NOTES.HEALTH},
                        {id: NOTES.PERSONAL_ISSUES, name: NOTES.PERSONAL_ISSUES},
                        {id: NOTES.CHANGE_OF_CARREER, name: NOTES.CHANGE_OF_CARREER},
                        {id: NOTES.REPEATED, name: NOTES.REPEATED},
                        {id: NOTES.INDEPENDENT, name: NOTES.INDEPENDENT}
                    ]}
                    label="Seleccione la razón de la suspensión"
                    onNoteChange={this.onNoteChange}
                    onCloseNoteForm={this.onShowSuspensionNotesModal}
                    onSendNote={this.onSendNote}
                />
            </BaseModal>
        );
    }

    onUserIsActiveChange(e) {
        this.props.activateUser(this.props.value, e.currentTarget.checked ? 1 : 0);
    }

    onClick() {
        this.props.onClick(this.props.value);
    }

    onAcceptUser(e) {
        e.stopPropagation();
        this.props.onAcceptUser(this.props.value);
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <div>
                <div className="card card--user" onClick={this.onClick}>
                    <div className="card-img-top" style={{backgroundImage: `url(${this.props.image})`}} />
                    <div className="card__header text-center">
                        <h4 className="card-title">{this.props.name}</h4>
                        <span className="card-subheading">{this.props.isAssistant ? 'Asistente' : USERTYPES_LABELS[this.props.userType]}</span>
                        <span className="card-subtitle">{this.props.agency}</span>
                    </div>
                    <div className={`card__description ${this.props.isPending ? 'text--align-center' : ''}`}>
                        {this.props.isPending &&
                            <button
                                disabled={this.props.oneSelected}
                                className="btn btn-primary full-width"
                                onClick={this.onAcceptUser}>
                                {'Aceptar'}
                            </button>
                        }
                        {(!this.props.isActive || !this.props.isPublished) && !this.props.isPending &&
                            <div
                                onClick={(e) => e.stopPropagation()}
                                className={`${this.props.oneSelected ? 'disabled' : ''}`}>
                                <div className="row align-items-center no-gutters">
                                    <span className="basic-details__icon col-md-1"><i className={WORLD_ICON} /></span>
                                    <div className="col-md-7">
                                        <p className="user-system-desc">RE/MAX RD e Int.</p>
                                    </div>
                                    <div className="col-md-4">
                                        <Toggle
                                            className="md-switch"
                                            icons={false}
                                            disabled={this.props.oneSelected}
                                            checked={this.props.isPublished}
                                            onChange={this.onUserPublishedChange} />
                                    </div>
                                </div>
                                <div className="row align-items-center no-gutters">
                                    <span className="basic-details__icon col-md-1"><i className={SYSTEM_ICON} /></span>
                                    <div className="col-md-7">
                                        <p className="user-system-desc">Sistema RE/MAX</p>
                                    </div>
                                    <div className="col-md-4">
                                        <Toggle
                                            className="md-switch"
                                            icons={false}
                                            disabled={this.props.oneSelected}
                                            checked={this.props.isActive}
                                            onChange={this.onShowSuspensionNotesModal} />
                                    </div>
                                </div>
                            </div>
                        }
                        {!this.props.isPending && this.props.isActive && this.props.isPublished &&
                            <div>
                                <p style={{marginBottom: '24px'}}><strong className="user-label-desc">Correo electrónico:</strong><span className="text--blue">{this.props.email}</span></p>
                                <p className={`${!this.props.phone ? 'invisible' : ''}`}><strong className="user-label-desc">Oficina:</strong><span className="text--blue">{this.props.phone || '-'}</span></p>
                            </div>
                        }
                    </div>
                </div>
                {this.renderSuspensionNotesModal()}
            </div>
        );
    }
}

UserCard.defaultProps = {
    image: '/images/user-placeholder.svg',
    className: ''
};


/**
 * Properties validations.
 */
UserCard.propTypes = {
    publishUser: PropTypes.func,
    activateUser: PropTypes.func,
    isPublished: PropTypes.bool,
    onAcceptUser: PropTypes.func,
    className: PropTypes.string,
    isPending: PropTypes.bool,
    isActive: PropTypes.bool,
    value: PropTypes.number,
    onClick: PropTypes.func,
    name: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
    agency: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    userType: PropTypes.number.isRequired,
    isAssistant: PropTypes.bool.isRequired
};
