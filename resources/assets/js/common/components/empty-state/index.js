import React from 'react';
import PropTypes from 'prop-types';

export default class EmptyState extends React.Component {
    constructor(props) {
        super(props);

        this.state = { image: null };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.image !== this.state.image) {
            this.setState({image: nextProps.image});
        }
    }

    render() {
        return (
            <div className={`empty-state ${this.props.small ? 'empty-state--small': 'layout-body__content'}`}>
                <div className="row">
                    <div className="col-12">
                        <div className="row">
                            <div className="col-12 text-center">
                                <div style={this.props.imgStyle}>
                                    <img src={this.props.image} height={this.props.height} alt="Item image" className="empty-state__image" />
                                </div>
                                <h2 className="empty-state__headline">{this.props.title}</h2>
                                {typeof this.props.text === 'function' &&
                                    this.props.text()
                                }
                                {typeof this.props.text === 'string' &&
                                    <h3 className="empty-state__headline">{this.props.text}</h3>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

EmptyState.propTypes = {
    imgStyle: PropTypes.object,
    image: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    text: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
    small: PropTypes.bool
};
