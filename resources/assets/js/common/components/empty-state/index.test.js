import React from 'react';
import renderer from 'react-test-renderer';

import EmptyState from './';

describe('EmptyState Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <EmptyState
                    image={'image.jpg'}
                    title={'Title'}
                    text={'This is a text'}>
                </EmptyState>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

