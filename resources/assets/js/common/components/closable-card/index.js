import React from 'react';
import PropTypes from 'prop-types';

import {
    CLOSE_CIRCLE_ICON
} from 'common/constants/icon.constant';

export default class ClosableCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

        this.onClose = this.onClose.bind(this);
    }

    onClose(e) {
        this.props.onClose(e.currentTarget.getAttribute('value'));
    }

    render() {
        if (this.props.variant) {
            return (
                <div style={this.props.style} className={`d-flex closable-card-variant ${this.props.className || ''}`}>
                    <div style={{width: this.props.widthCard}}>
                        {this.props.children}
                    </div>
                    <span
                        style={{width: this.props.widthIcon}}
                        value={this.props.id}
                        onClick={this.onClose}
                        className="align-center text--align-right mgT2 cursor-pointer closable-card-variant__icon fs-28 text--blue">
                        {this.props.deleteIcon &&
                            this.props.deleteIcon
                        }
                        {!this.props.deleteIcon &&
                            <i className={CLOSE_CIRCLE_ICON} />
                        }
                    </span>
                </div>
            );
        } else {
            return (
                <div style={this.props.style} className={`row no-gutters closable-card ${this.props.className || ''}`}>
                    {this.props.children}
                    <span
                        style={this.props.iconStyle}
                        value={this.props.id}
                        onClick={this.onClose}
                        className={`fl__icon text--blue ${!this.props.hover ? 'no-hover' : '' } closable-card__icon cursor-pointer`}>
                        {this.props.deleteIcon &&
                            this.props.deleteIcon
                        }
                        {!this.props.deleteIcon &&
                            <i className={CLOSE_CIRCLE_ICON} />
                        }
                    </span>
                </div>
            );
        }
    }
}

ClosableCard.defaultProps = {
    widthCard: '95%',
    widthIcon: '5%',
    hover: true
};

/**
 * Statistics Card properties validations.
 */
ClosableCard.propTypes = {
    className: PropTypes.string,
    id: PropTypes.any.isRequired,
    onClose: PropTypes.func.isRequired
};
