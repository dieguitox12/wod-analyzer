import React from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';
import { CREVRON_RIGHT_ICON } from 'common/constants/icon.constant';

/**
 * A reutilizable breadcrumb component.
 */
export default class Breadcrumb extends React.PureComponent {
    constructor(props) {
        super(props);

        this.renderItems = this.renderItems.bind(this);
    }

    renderItems() {

        const items = this.props.items;
        const newItems = [];

        if (items.length === 0) {
            return <li></li>;
        }

        // Gets the first item.

        newItems.push(
            <li
                key="nav-item-0"
                className={'breadcrumb-item ' + (items.length === 1 ? 'active' : '')}>
                <Link to={items[0].link}>
                    {items[0].name}
                </Link>
            </li>
        );

        // If there's only one item, return.
        if (items.length === 1) {
            return newItems;
        }

        // Get the 2 to last-1 items of the list.
        for(let i=1; i < items.length-1; i++) {
            newItems.push(
                <li
                    key={`nav-separator-${i}`}
                    className="breadcrumb-item breadcrumb-item--separator">
                    <i className={CREVRON_RIGHT_ICON} />
                </li>
            );
            newItems.push(
                <li key={`nav-item-${i}`} className='breadcrumb-item'>
                    <Link to={items[i].link}>
                        {items[i].name}
                    </Link>
                </li>
            );
        }

        // Get the last item (this one is active.)
        newItems.push(
            <li
                key="nav-separator-last"
                className="breadcrumb-item breadcrumb-item--separator">
                <i className={CREVRON_RIGHT_ICON} />
            </li>
        );
        newItems.push(
            <li
                key="nav-item-last"
                className='breadcrumb-item active'>
                {items[items.length-1].name}
            </li>
        );

        return newItems;
    }

    /**
      * Renders the element.
      *
      * @return {ReactComponent}
      */
    render() {
        return (
            <nav aria-label="breadcrumb" role="navigation">
                <ol className="breadcrumb">
                    {this.renderItems()}
                </ol>
            </nav>
        );
    }
}

Breadcrumb.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({
        link: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired
    }))
};
