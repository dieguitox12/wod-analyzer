import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router';

import Breadcrumb from './';

describe('Breadcrumb Component', () => {
    test('renders without crashing', () => {

        let items = [
            { name: 'Hello', link: 'hello' },
            { name: 'Bye', link: 'bye' },
            { name: 'About', link: 'about' }
        ];
        const tree = renderer
            .create(
                <MemoryRouter>
                    <Breadcrumb items={items}/>
                </MemoryRouter>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

