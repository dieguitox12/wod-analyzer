import React, { Component } from 'react';

class PrintableComponent extends Component {

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}

export default PrintableComponent;
