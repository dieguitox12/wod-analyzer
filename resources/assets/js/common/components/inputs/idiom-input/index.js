import React from 'react';
import PropTypes from 'prop-types';

import TextInput from 'common/components/inputs/text-input';
import Select from 'common/components/inputs/select-default';

export default class IdiomInput extends React.Component {
    /**
    * Sets the state and make the binds as needed.
    *
    * @param {props}  the required React properties.
    */
    constructor(props) {
        super(props);

        this.state = {
            id: props.id,
            idiom: props.idiom,
            levelId: props.levelId
        };

        this.onTextChange = this.onTextChange.bind(this);
        this.onSelectIdiom = this.onSelectIdiom.bind(this);
    }

    /**
    * Fired once the input text changes.
    *
    * @param {Proxy.dispatcher} event  contains the input's new value.
    */
    onTextChange(value) {
        this.setState({idiom: value});

        if (this.props.onChange) {
            this.props.onChange({
                levelId: this.state.levelId,
                idiom: value,
                id: this.state.id
            });

        }
    }

    onSelectIdiom(value) {
        this.setState({levelId: value});
        if (this.props.onChange) {
            this.props.onChange({
                levelId: value,
                idiom: this.state.idiom,
                id: this.state.id
            });
        }
    }

    /**
    * Renders the element.
    *
    * @return {ReactComponent}
    */
    render() {
        return (
            <div className="row">
                <div className="col-6">
                    <TextInput
                        placeholder="Idioma"
                        value={this.state.idiom}
                        onChange={this.onTextChange}
                    />
                </div>
                <div className="col-6">
                    <Select
                        icon={this.state.icon}
                        disabled={this.props.disabled}
                        value={this.state.levelId}
                        floatingLabel={this.props.label}
                        searchable={false}
                        options={this.props.options}
                        onChange={this.onSelectIdiom}
                    />
                </div>
            </div>
        );
    }
}

/**
 * Phone input properties validation.
 */
IdiomInput.propTypes = {
    disabled: PropTypes.bool,
    options: PropTypes.array,
    label: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func
};
