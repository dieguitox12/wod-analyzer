import React from 'react';
import renderer from 'react-test-renderer';

import Textarea from './';

describe('Textarea Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <Textarea
                    label={'Description'}
                    value={'This is a description'}
                    onChange={() => { return 0; }}
                />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

