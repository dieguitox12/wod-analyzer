import React from 'react';
import PropTypes from 'prop-types';

import money from 'common/helpers/money';

/**
* Represents a simple text input.
*/
export default class MoneyInput extends React.PureComponent {
    /**
     * Sets the state and make the binds as needed.
     *
     * @param {props} the required React properties.
     */
    constructor(props) {
        super(props);

        this.state = {labelFocused: false};

        this.onTextChange = this.onTextChange.bind(this);
        this.onFocus = this.changeFocus.bind(this, true);
        this.onBlur = this.changeFocus.bind(this, false);
    }

    /**
    * Fired once the input focus changes.
    *
    * @param {bool} labelFocused determines if the element is focused or not.
    */
    changeFocus(labelFocused) {
        this.setState({labelFocused});
    }

    /**
    * Fired once the input text changes.
    *
    * @param {Proxy.dispatcher} event  contains the input's new value.
    */
    onTextChange(event) {
        const value = event.currentTarget.value.replace(/[^0-9.]/g, '');
        if ( isNaN(value)) { return; }

        if (this.props.onChange) {
            this.props.onChange(value);
        }
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        let value = '';
        if (this.props.value) {
            value = this.props.symbol + (money(this.props.value, false) + '');
        }
        return (
            <label className="form-label">
                <span className="label">
                    {this.props.icon && (
                        <span className="fl__icon fl__icon--left">
                            <i className={this.props.icon} />
                        </span>
                    )}
                    {this.props.floatingLabel}
                </span>
                <input
                    name={this.props.name}
                    type="text"
                    className="form-control text--align-left"
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                    onChange={this.onTextChange}
                    value={value}
                    disabled={this.props.disabled}
                />
            </label>
        );
    }
}

MoneyInput.defaultProps = { symbol: '$' };

/**
 * Text input properties validation.
 */
MoneyInput.propTypes = {
    floatingLabel: PropTypes.string,
    symbol: PropTypes.string,
    disabled: PropTypes.bool,
    name: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func
};
