import React from 'react';
import renderer from 'react-test-renderer';

import MoneyInput from './';

describe('MoneyInput Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <MoneyInput
                    onChange={() => { return 0; }}
                />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

