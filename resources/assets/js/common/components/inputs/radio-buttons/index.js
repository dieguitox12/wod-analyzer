import React from 'react';
import PropTypes from 'prop-types';

/**
 * Represents a radio group.
 */
export default class Radio extends React.Component {
    /**
     * Sets the state and make the binds as needed.
     *
     * @param {props}  the required React properties.
     */
    constructor(props) {
        super(props);

        this.state = { selectedValue: props.selectedValue };

        this.changeRadio = this.changeRadio.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.selectedValue) {
            this.setState({ selectedValue: nextProps.selectedValue });
        }
    }

    /**
     * Fired once the checked status changes.
     */
    changeRadio(e) {
        if (this.props.onChange) {
            this.props.onChange(parseInt(e.currentTarget.value), e);
        }
        this.setState({ selectedValue: parseInt(e.currentTarget.value) });
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <div className={`md-radio ${this.props.className || ''}`}>
                {this.props.options.map(e => (
                    <div className="pdG" key={e.value} disabled={this.props.disabled}>
                        <input
                            id={`rdr-${e.label}-${e.value}`.split(' ').join('-')}
                            name={`rdr-${e.label}-${e.value}`.split(' ').join('-')}
                            type="radio"
                            value={e.value}
                            checked={e.value === this.state.selectedValue}
                            onChange={this.changeRadio}
                            disabled={this.props.disabled}
                        />
                        <label htmlFor={`rdr-${e.label}-${e.value}`.split(' ').join('-')}>
                            {e.label}
                        </label>
                    </div>
                ))}
            </div>
        );
    }
}

/**
 * Password properties validation.
 */
Radio.propTypes = {
    variant: PropTypes.bool,
    squared: PropTypes.bool,
    value: PropTypes.number,
    options: PropTypes.array,
    className: PropTypes.string,
    label: PropTypes.string,
    selectedValue: PropTypes.number.isRequired,
    onChange: PropTypes.func
};
