import React from 'react';
import renderer from 'react-test-renderer';

import PasswordInput from './';

describe('PasswordInput Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <PasswordInput
                    label={'Password'}
                    onChange={() => { return 0; }}
                />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

