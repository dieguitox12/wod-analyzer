import React from 'react';
import PropTypes from 'prop-types';



/**
 * Represents a simple text input.
 */
export default class TextInput extends React.PureComponent {
    /**
     * Sets the state and make the binds as needed.
     *
     * @param {props} the required React properties.
     */
    constructor(props) {
        super(props);

        this.state = { labelFocused: false };

        this.onTextChange = this.onTextChange.bind(this);
        this.onFocus = this.changeFocus.bind(this, true);
        this.onBlur = this.onBlur.bind(this);
    }

    onBlur(e) {
        this.changeFocus(false);
        if (this.props.onBlur) {
            this.props.onBlur(e);
        }
    }

    /**
     * Fired once the input focus changes.
     *
     * @param {bool} labelFocused determines if the element is focused or not.
     */
    changeFocus(labelFocused) {
        this.setState({ labelFocused });
    }

    /**
     * Fired once the input text changes.
     *
     * @param {Proxy.dispatcher} event  contains the input's new value.
     */
    onTextChange(event) {
        if (this.props.onChange) {
            if (this.props.onlyNumbers) {
                if (isNaN(event.currentTarget.value)) {
                    return;
                }
            }
            if (!this.props.numbers) {
                this.props.onChange(event.target.value.replace(/[0-9]/g, ''), this.props.id);
            } else {
                this.props.onChange(event.target.value, this.props.id);
            }
        }
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <label className="form-label">
                <span className="label">
                    {this.props.icon && (
                        <span className="fl__icon fl__icon--left mgR">
                            <i className={this.props.icon} />
                        </span>
                    )}
                    {this.props.label}
                </span>
                <input
                    placeholder={this.props.placeholder}
                    name={this.props.name}
                    type="text"
                    className="form-control "
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                    onChange={this.onTextChange}
                    value={this.props.value}
                    disabled={this.props.disabled}
                />
            </label>
        );
    }
}

/**
 * Text input properties validation.
 */
TextInput.propTypes = {
    placeholder: PropTypes.string,
    disabled: PropTypes.bool,
    name: PropTypes.string,
    icon: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    onChange: PropTypes.func,
    label: PropTypes.string
};
