import React from 'react';
import renderer from 'react-test-renderer';

import TextInput from './';

describe('TextInput Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <TextInput
                    label={'Name'}
                    value={'JohnDoe'}
                    onChange={() => { return 0; }}
                />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

