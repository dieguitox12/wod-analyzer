import React from 'react';
import PropTypes from 'prop-types';

import TextInput from 'common/components/inputs/text-input';
import Select from 'common/components/inputs/select-default';
import { SOCIAL_NETWORKS_ICONS } from 'common/constants/icon.constant';

export default class SocialNetworkInput extends React.Component {
    /**
     * Sets the state and make the binds as needed.
     *
     * @param {props}  the required React properties.
     */
    constructor(props) {
        super(props);

        this.state = {
            id: props.id,
            address: props.address,
            networkId: props.networkId,
            icon: props.icon
        };

        this.onTextChange = this.onTextChange.bind(this);
        this.onSelectSocialNetwork = this.onSelectSocialNetwork.bind(this);
    }

    /**
     * Fired once the input text changes.
     *
     * @param {Proxy.dispatcher} event  contains the input's new value.
     */
    onTextChange(value) {
        this.setState({ address: value });

        if (this.props.onChange) {
            this.props.onChange({
                networkId: this.state.networkId,
                address: value,
                id: this.state.id
            });
        }
    }

    onSelectSocialNetwork(value) {
        this.setState({
            networkId: value,
            icon: SOCIAL_NETWORKS_ICONS[value]
        });
        if (this.props.onChange) {
            this.props.onChange({
                networkId: value,
                address: this.state.address,
                id: this.state.id
            });
        }
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <div className="row mgB">
                <div className={'col-md-6'}>
                    <Select
                        disabled={this.props.disabled}
                        value={this.state.networkId}
                        label={this.props.label}
                        searchable={false}
                        options={this.props.options}
                        onChange={this.onSelectSocialNetwork}
                    />
                    {this.state.networkId <=0 &&
                        <div className='has-error'>
                            <span className='help-block'>Este campo es requerido.</span>
                            <div className="clearfix mgT5"></div>
                        </div>
                    }
                </div>
                <div className={'col-md-6'}>
                    <TextInput
                        placeholder="Enlace"
                        value={this.state.address}
                        onChange={this.onTextChange}
                    />
                    {this.state.address === '' &&
                        <div className='has-error'>
                            <span className='help-block'>Este campo es requerido.</span>
                            <div className="clearfix mgT5"></div>
                        </div>
                    }
                </div>
            </div>
        );
    }
}

SocialNetworkInput.defaultProps = { creating: false };

/**
 * Phone input properties validation.
 */
SocialNetworkInput.propTypes = {
    creating: PropTypes.bool,
    disabled: PropTypes.bool,
    icon: PropTypes.string,
    options: PropTypes.array,
    label: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func
};
