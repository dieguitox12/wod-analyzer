import React from 'react';
import renderer from 'react-test-renderer';

import SelectDefault from './';

describe('SelectDefault Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <SelectDefault
                    onChange={() => { return 0; }}
                />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

