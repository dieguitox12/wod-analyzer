import React from 'react';
import PropTypes from 'prop-types';
import ReactSelect from 'react-select';

import { MAGNIFY_ICON } from 'common/constants/icon.constant';

/**
 * Represents a simple select.
 */
export default class Select extends React.Component {
    /**
     * Sets the state and make the binds as needed.
     *
     * @param {props}  the required React properties.
     */
    constructor(props) {
        super(props);

        this.onSelectChange = this.onSelectChange.bind(this);
    }


    /**
     * Fired once the select changes.
     *
     * @param {int|null} value  contains the input's new value.
     */
    onSelectChange(value) {
        if (this.props.onChange) {
            if (Array.isArray(value)) {
                if (value) {
                    this.props.onChange( value.map(element => element.id), this.props.id );
                    return;
                }
            }
            if (value) {
                this.props.onChange( value.id, this.props.id );
                return;
            }
            this.props.onChange( value, this.props.id );
        }
    }
    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        let { options, label, value, searchable, floatingLabel, clearable, withSearchIcon } = this.props;
        return (
            <div className={`form-label select-container${this.props.icon || withSearchIcon ? '--with-icon' : ''}`}>
                <div className="label select-header">
                    {floatingLabel}
                </div>

                {this.props.withSearchIcon &&
                    <span className="search-input__icon"><i className={ MAGNIFY_ICON } /></span>
                }
                {!this.props.icon &&
                    <ReactSelect
                        className={`${this.props.className ? this.props.className : "select-default"}`}
                        disabled={this.props.disabled}
                        onChange={this.onSelectChange}
                        searchable={searchable}
                        onInputChange={this.props.onInputChange}
                        clearable={clearable}
                        multi={this.props.multi}
                        labelKey="name"
                        valueKey="id"
                        value={value}
                        placeholder={label}
                        options={options}
                    />
                }
                {this.props.icon &&
                    <div className="select--has-icon">
                        <span className="fl__icon select-container__icon"><i className={this.props.icon} /></span>
                        <div className="">
                            <ReactSelect
                                className={`form-control ${this.props.className ? this.props.className : "select-default"}`}
                                disabled={this.props.disabled}
                                onChange={this.onSelectChange}
                                searchable={searchable}
                                onInputChange={this.props.onInputChange}
                                clearable={searchable}
                                multi={this.props.multi}
                                labelKey="name"
                                valueKey="id"
                                value={value || undefined}
                                placeholder={label || floatingLabel}
                                options={options}
                            />
                        </div>

                    </div>
                }

            </div>
        );
    }
}

/**
 * Select properties validation.
 */
Select.defaultProps = {
    label: '',
    floatingLabel: '',
    searchable: true,
    clearable: false,
    multi: false,
    withSearchIcon: false
};

/**
 * Select properties validation.
 */
Select.propTypes = {
    withSearchIcon: PropTypes.bool,
    multi: PropTypes.bool,
    className: PropTypes.string,
    icon: PropTypes.string,
    value: PropTypes.oneOfType([ PropTypes.string, PropTypes.number, PropTypes.array ]),
    label: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
    options: PropTypes.array,
    disabled: PropTypes.bool,
    floatingLabel: PropTypes.string,
    onChange: PropTypes.func,
    searchable: PropTypes.bool,
    clearable: PropTypes.bool
};
