import React from 'react';
import renderer from 'react-test-renderer';

import PhoneInput from './';

describe('PhoneInput Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <PhoneInput
                    label={'Phone'}
                    onChange={() => { return 0; }}
                />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

