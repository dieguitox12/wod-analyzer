import React from 'react';
import PropTypes from 'prop-types';

import { MAGNIFY_ICON, CLOSE_ICON } from 'common/constants/icon.constant';

/**
* Represents a simple text input.
*/
export default class SearchInput extends React.Component {
    /**
     * Sets the state and make the binds as needed.
     *
     * @param {props} the required React properties.
     */
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.value,
            clearable: this.props.clearable
        };

        this.resetValue = this.resetValue.bind(this);
        this.onTextChange = this.onTextChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({value: nextProps.value});
    }

    /**
    * Fired once the input text changes.
    *
    * @param {Proxy.dispatcher} event  contains the input's new value.
    */
    onTextChange(event) {
        this.setState({ value: event.target.value });
        if (this.props.onChange) {
            this.props.onChange(event.target.value);
        }
    }


    /**
    * Reset the input value
    */
    resetValue() {
        // If input value is empty -> do nothing
        if ( this.state.value !== '') {
            this.setState({ value: '' });
            this.props.onChange('');
        }
    }


    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        const { value, clearable } = this.state;

        return (
            <div className="form-check" style={this.props.style}>
                <div className="search-input">
                    <input
                        style={{borderRadius: '0px'}}
                        className="form-control"
                        disabled={this.props.disabled}
                        name={this.props.name}
                        onChange={this.onTextChange}
                        placeholder={this.props.label}
                        type="text"
                        value={value}
                    />
                    {clearable && value &&
                        <span
                            className="search-input__reset-button search-input__icon search-input__icon--right"
                            onClick={this.resetValue}>
                            <i className={ CLOSE_ICON } />
                        </span>
                    }
                    {!value && <span className="search-input__icon search-input__icon--right"><i className={ MAGNIFY_ICON } /></span>}
                </div>
            </div>
        );
    }
}


SearchInput.defaultProps = { clearable: true };

/**
 * S input properties validation.
 */
SearchInput.propTypes = {
    style: PropTypes.object,
    disabled: PropTypes.bool,
    label: PropTypes.string.isRequired,
    name: PropTypes.string,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    clearable: PropTypes.bool
};
