import React from 'react';
import renderer from 'react-test-renderer';

import SearchInput from './';

describe('SearchInput Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <SearchInput
                    label={'Search'}
                    value={'searching'}
                    onChange={() => { return 0; }}
                />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

