import React from 'react';
import PropTypes from 'prop-types';
import MaskedInput from 'react-text-mask'

/**
* Represents the default maskedInput class.
*/
export default class CustomMaskedInput extends React.Component {

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <MaskedInput
                disabled={this.props.disabled}
                className={this.props.className}
                mask={this.props.mask}
                name={this.props.name}
                placeholder={this.props.placeholder ? this.props.placeholder : ''}
                value={this.props.value}
                onChange={this.props.onChange}
                onBlur={this.props.onBlur}
                onFocus={this.props.onFocus}
            />
        );
    }
}

/**
 * Masked input default properties.
 */
CustomMaskedInput.defaultProperties = {
    className: '',
    name: '',
    placeholder: '',
    value: 'NA',
    disabled: false
};

/**
 * Masked input properties validation.
 */
CustomMaskedInput.propTypes = {
    name: PropTypes.string,
    value: PropTypes.string,
    disabled: PropTypes.bool,
    className: PropTypes.string,
    placeholder: PropTypes.string,
    mask: PropTypes.array.isRequired,

    onBlur: PropTypes.func,
    onFocus: PropTypes.func,
    onChange: PropTypes.func
};
