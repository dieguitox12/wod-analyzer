import React from 'react';
import PropTypes from 'prop-types';
import Datetime from 'react-datetime';
import moment from 'moment';

import {
    PAGINATION_LEFT_ICON,
    PAGINATION_RIGHT_ICON,
    CALENDAR_ICON
} from 'common/constants/icon.constant';



/**
 * Represents a datetime picker component.
 */
export default class DatetimePicker extends React.Component {
    /**
     * Sets the state and make the binds as needed.
     *
     * @param {props} the required React properties.
     */
    constructor(props) {
        super(props);

        this.state = {labelFocused: false};

        this.onChange = this.onChange.bind(this);
        this.isValidDate = this.isValidDate.bind(this);
        this.onFocus = this.changeFocus.bind(this, true);
        this.onBlur = this.changeFocus.bind(this, false);
        this.leftIconClicked = this.leftIconClicked.bind(this);
        this.rightIconClicked = this.rightIconClicked.bind(this);
    }

    leftIconClicked() {
        let newValue = moment(this.props.value);
        newValue = newValue.subtract(1, this.props.viewMode);
        if (newValue.year() === moment().year()) {
            this.onChange(newValue);
        }
    }

    rightIconClicked() {
        let newValue = moment(this.props.value);
        newValue = newValue.add(1, this.props.viewMode);
        if (newValue.year() === moment().year()) {
            this.onChange(newValue);
        }
    }

    /**
     * Fired once the input focus changes.
     *
     * @param {bool} labelFocused determines if the element is focused or not.
     */
    changeFocus(labelFocused) {
        this.setState({labelFocused});
    }

    /**
     * Fired once the date changes.
     *
     * @param {Moment | string} value  contains the date time value.
     */
    onChange(value) {
        if (this.props.onChange) {
            if (typeof value !== 'string' && this.isValidDate(value)) {
                this.props.onChange(value.format('YYYY-MM-DD'));
                return;
            }
            this.props.onChange(value);
        }

    }

    /**
     * Determines if the selected date is valid.
     *
     * @param  {Moment}  current the current moment object.
     *
     * @return {Boolean} TRUE if the date is valid, FALSE otherwise.
     */
    isValidDate(current) {
        let valid = true;

        if (this.props.minDate) {
            valid = valid && current.isAfter(this.props.minDate);
        }

        if (this.props.maxDate) {
            valid = valid && current.isBefore(this.props.maxDate);
        }

        return valid;
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <span>
                {this.props.alternativeView &&
                    <ul className="pdL0">
                        <li className="datetime-input-alternative datetime-input-alternative__left">
                            <a
                                className = {
                                    `${moment(this.props.value).subtract(1, this.props.viewMode).year() !== moment().year() ? 'disabled' : ''}`
                                }
                                onClick={this.leftIconClicked}>
                                <span className={PAGINATION_LEFT_ICON}></span>
                            </a>
                        </li>
                        <li className="datetime-input-alternative">
                            <label className="form-label">
                                <span className="label">{this.props.label}</span>
                                <Datetime
                                    className={`datetime-input display--inline-block ${this.props.className}`}
                                    inputProps={{className: 'form-control' }}
                                    timeFormat={false}
                                    viewMode={this.props.viewMode}
                                    dateFormat={this.props.format}
                                    value={moment(this.props.value).format(this.props.format)}
                                    onFocus={this.onFocus}
                                    onBlur={this.onBlur}
                                    onChange={this.onChange}
                                    isValidDate={this.isValidDate}
                                />
                            </label>
                        </li>
                        <li className="datetime-input-alternative datetime-input-alternative__right">
                            <a
                                className = {
                                    `${moment(this.props.value).add(1, this.props.viewMode).year() !== moment().year() ? 'disabled' : ''}`
                                }
                                onClick={this.rightIconClicked}>
                                <span className={PAGINATION_RIGHT_ICON}></span>
                            </a>
                        </li>
                    </ul>
                }
                {!this.props.alternativeView &&
                    <label className="form-label">
                        <span className="label">{this.props.label}</span>
                        <Datetime
                            className={`datetime-input display--inline-block ${this.props.className}`}
                            inputProps={{className: 'form-control', placeholder: this.props.placeholder }}
                            timeFormat={false}
                            viewMode={this.props.viewMode}
                            dateFormat={this.props.format}
                            value={this.props.value ? moment(this.props.value).format(this.props.format) : ''}
                            onFocus={this.onFocus}
                            onBlur={this.onBlur}
                            onChange={this.onChange}
                            isValidDate={this.isValidDate}
                        />
                        {this.props.withIcon && <span className="datetime-input__icon"><i className={ CALENDAR_ICON } /></span>}
                    </label>
                }
            </span>
        );
    }
}


/**
 * Datetime picker default props.
 */
DatetimePicker.defaultProps = {
    positionClass: '',
    className: '',
    floating: false,
    value: '',
    viewMode: 'days',
    format: 'MM/DD/YYYY',
    alternativeView: false
};

/**
 * Select properties validation.
 */

DatetimePicker.propTypes = {
    label: PropTypes.string,
    className: PropTypes.string,
    alternativeView: PropTypes.bool,
    floating: PropTypes.bool,
    value: PropTypes.oneOfType([ PropTypes.string, PropTypes.object ]),
    options: PropTypes.array,
    disabled: PropTypes.bool,
    maxDate: PropTypes.instanceOf(moment),
    minDate: PropTypes.instanceOf(moment),
    onChange: PropTypes.func,
    positionClass: PropTypes.string
};
