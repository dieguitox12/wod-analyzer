import React from 'react';
import PropTypes from 'prop-types';
import { Async } from 'react-select';

/**
* Represents a multiselect dropdown.
*/
export default class MultiSelect extends React.Component {

    /**
     * Sets the state and make the binds as needed.
     *
     * @param {props} the required React properties.
     */
    constructor(props) {
        super(props);

        this.onSelectChange = this.onSelectChange.bind(this);
    }

    /**
     * Fired once the select changes.
     *
     * @param {array} value  contains the input's new value.
     */
    onSelectChange(value = []) {
        if (this.props.onChange) {
            this.props.onChange(value);
        }
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render () {
        return (
            <div className="section">
                <Async
                    multi
                    autoload
                    className={`${this.props.className ? this.props.className : "select-default"}`}
                    closeOnSelect={this.props.closeOnSelect}
                    options={this.props.options}
                    placeholder={this.props.placeholder}
                    value={this.props.value}
                    loadOptions={this.props.loadOptions}
                    onChange={this.onSelectChange}
                    onMenuScrollToBottom={this.props.onMenuScrollToBottom}
                    filterOptions={(options) => options}
                />
            </div>
        );
    }
}

MultiSelect.propTypes = {
    className: PropTypes.string,
    label: PropTypes.string,
    options: PropTypes.array,
    value: PropTypes.array,
    onChange: PropTypes.func,
    closeOnSelect: PropTypes.bool,
    placeholder: PropTypes.string,
    onMenuScrollToBottom: PropTypes.func,
    loadOptions: PropTypes.func.isRequired
};
