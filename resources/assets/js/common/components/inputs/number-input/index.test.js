import React from 'react';
import renderer from 'react-test-renderer';

import NumberInput from './';

describe('NumberInput Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <NumberInput
                    label={'Number'}
                    value={0}
                    onChange={() => { return 0; }}
                />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

