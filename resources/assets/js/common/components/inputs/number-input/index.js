import React from 'react';
import PropTypes from 'prop-types';



/**
* Represents a simple text input which only accepts numeric input.
*/
export default class NumberInput extends React.Component {
    /**
     * Sets the state and make the binds as needed.
     *
     * @param {props} the required React properties.
     */
    constructor(props) {
        super(props);

        this.state = {labelFocused: false};

        this.onTextChange = this.onTextChange.bind(this);
        this.onFocus = this.changeFocus.bind(this, true);
        this.onBlur = this.changeFocus.bind(this, false);
    }

    /**
     * Fired once the input focus changes.
     *
     * @param {bool} labelFocused determines if the element is focused or not.
     */
    changeFocus(labelFocused) {
        this.setState({labelFocused});
    }

    /**
     * Fired once the input text changes.
     *
     * @param {Proxy.dispatcher} event  contains the input's new value.
     */
    onTextChange(event) {
        const value = Number(event.currentTarget.value);

        if (isNaN(value)) { return; }
        if (this.props.onChange) {
            this.props.onChange(value);
        }
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {


        return (
            <label className="form-label">
                <span className="label">
                    {this.props.icon && (
                        <span className="fl__icon fl__icon--left">
                            <i className={this.props.icon} />
                        </span>
                    )}
                    {this.props.label}
                </span>
                <input
                    name={this.props.name}
                    type="text"
                    className="form-control"
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                    onChange={this.onTextChange}
                    value={this.props.value}
                    disabled={this.props.disabled}
                />
            </label>
        );
    }
}

/**
 * Number input properties validation.
 */
NumberInput.propTypes = {
    disabled: PropTypes.bool,
    name: PropTypes.string,
    icon: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    onChange: PropTypes.func,
    label: PropTypes.string.isRequired
};
