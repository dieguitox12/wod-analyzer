import React from 'react';
import PropTypes from 'prop-types';



/**
 * Component responsible of displaying a percentage input.
 */
export default class PercentageInput extends React.Component {
    /**
     * Sets the state and make the binds as needed.
     *
     * @param {props} the required React properties.
     */
    constructor(props) {
        super(props);

        this.state = {labelFocused: false};

        this.onChange = this.onChange.bind(this);
        this.onFocus = this.changeFocus.bind(this, true);
        this.onBlur = this.changeFocus.bind(this, false);
    }

    /**
    * Fired once the input focus changes.
    *
    * @param {bool} labelFocused determines if the element is focused or not.
    */
    changeFocus(labelFocused) {
        this.setState({labelFocused});
    }

    /**
     * Fired once the input text changes.
     *
     * @param {Proxy.dispatcher} event  contains the input's new value.
     */
    onChange(event) {
        if (this.props.onChange) {
            const value = Number(event.currentTarget.value);
            this.props.onChange(value, this.props.index);
        }
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {


        return (
            <label className="percentage-input form-label">
                <span className="label">
                    {this.props.icon && (
                        <span className="fl__icon fl__icon--left">
                            <i className={this.props.icon} />
                        </span>
                    )}
                    {this.props.label}
                </span>
                <input
                    type="number"
                    min="0"
                    max="100"
                    name={this.props.name}
                    className="form-control"
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                    onChange={this.onChange}
                    value={this.props.value}
                    disabled={this.props.disabled}
                />
            </label>
        );
    }
}

PercentageInput.defaultProps = { className: '' };

/**
 * PercentageInput properties validation.
 */
PercentageInput.propTypes = {
    value: PropTypes.number,
    index: PropTypes.number,
    disabled: PropTypes.bool,
    name: PropTypes.string,
    icon: PropTypes.string,
    onChange: PropTypes.func,
    label: PropTypes.string.isRequired
};
