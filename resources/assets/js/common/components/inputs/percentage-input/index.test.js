import React from 'react';
import renderer from 'react-test-renderer';

import PercentageInput from './';

describe('PercentageInput Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <PercentageInput
                    label={'Password'}
                    onChange={() => { return 0; }}
                />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

