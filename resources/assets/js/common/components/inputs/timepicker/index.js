import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import TimePicker from 'rc-time-picker';

const format = 'h:mm a';

/**
 * Represent a time input.
 */
export default class Timepicker extends React.Component {


    /**
     * Sets the state and make the binds as needed.
     *
     * @param {object} props   the required React properties.
     */
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }

    /**
     * Fired once the time changes.
     *
     * @param {Proxy.dispatcher} event  contains the time's new value.
     */
    onChange(value) {
        if (this.props.onChange) {
            if (value != null) {
                this.props.onChange(value);
            }
        }
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <TimePicker
                use12Hours
                format={format}
                showSecond={false}
                className={`time-input ${this.props.className}`}
                name={this.props.name}
                onChange={this.onChange}
                value={this.props.value}
                disabled={this.props.disabled}
                placeholder={this.props.placeholder}
            />
        );
    }
}

Timepicker.defaultProps = { className: '' };

/**
 * Time input properties validation.
 */
Timepicker.propTypes = {
    name: PropTypes.string,
    className: PropTypes.string,
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    placeholder: PropTypes.string,
    value: PropTypes.instanceOf(moment)
};
