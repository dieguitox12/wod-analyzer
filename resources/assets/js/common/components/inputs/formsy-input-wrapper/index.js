import React from 'react';
import PropTypes from 'prop-types';

import { withFormsy } from 'formsy-react';

/**
* Extends an input component by adding Formsy validations.
*/
class FormsyInputWrapper extends React.Component {
    /**
    * Renders the element.
    *
    * @return {ReactComponent}
    */
    render() {

        const hasError = this.props.customError
                || (!this.props.isPristine() && !this.props.isValid());
        const errorMessage = this.props.customError || this.props.getErrorMessage();

        return (
            <div className={`input-wrapper ${this.props.className} ${hasError ? ' has-error' : ''}`.trim()}>
                {this.props.children}
                <span className={`form-text text-muted ${this.props.errorClassName || ''}`}>{hasError ? errorMessage : ''}</span>
            </div>
        );
    }
}


FormsyInputWrapper.defaultProps = { className: '' };


/**
 * Formsy Wrapper properties validation.
 */
FormsyInputWrapper.propTypes = {
    className: PropTypes.string,
    customError: PropTypes.string,
    isValid: PropTypes.func.isRequired,
    isPristine: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
    getErrorMessage: PropTypes.func.isRequired,
    errorClassName: PropTypes.string
};

export default withFormsy(FormsyInputWrapper);
