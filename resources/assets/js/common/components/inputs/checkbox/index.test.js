import React from 'react';
import renderer from 'react-test-renderer';

import Checkbox from './';

describe('Checkbox Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <Checkbox
                    onChange={() => { return 0; }}
                />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

