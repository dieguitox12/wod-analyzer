import React from 'react';
import PropTypes from 'prop-types';

/**
* Represents a rounded checkbox.
*/
export default class Checkbox extends React.Component {
    /**
    * Sets the state and make the binds as needed.
    *
    * @param {props}  the required React properties.
    */
    constructor(props) {
        super(props);

        this.changeCheckbox = this.changeCheckbox.bind(this);
    }

    /**
    * Fired once the checked status changes.
    */
    changeCheckbox() {
        if (this.props.onChange) {
            this.props.onChange(!this.props.checked, this.props.id);
        }
    }

    /**
    * Renders the element.
    *
    * @return {ReactComponent}
    */
    render() {
        return (
            <div
                className={`checkbox ${this.props.className ? this.props.className : ""} ${this.props.checked ? "checkbox--checked" : ""} ${this.props.label ? '' : 'checkbox--no-label'}`}>
                <label className="checkbox__label">
                    <input
                        value={this.props.value || this.props.checked}
                        checked={this.props.checked}
                        className="checkbox__input"
                        onChange={this.changeCheckbox}
                        type="checkbox"
                    />
                    <span className={`checkbox__check ${this.props.squared ? 'checkbox--squared' : ''}`}></span>
                    <span className="checkbox__text">{this.props.label}</span>
                </label>
            </div>
        );
    }
}

/**
 * Password properties validation.
 */
Checkbox.propTypes = {
    squared: PropTypes.bool,
    value: PropTypes.any,
    checked: PropTypes.bool,
    className: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func
};
