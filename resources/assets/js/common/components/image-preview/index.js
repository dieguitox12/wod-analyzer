import React from 'react';
import PropTypes from 'prop-types';

export default class ImagePreview extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            image: null,
            placeholder: null
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.image) {
            this.loadImage(nextProps.image);
        } else {
            this.setState({image: ''});
        }
        if (nextProps.placeholder) {
            this.loadPlaceholder(nextProps.placeholder);
        }
    }

    loadPlaceholder(placeholder) {

        if (typeof placeholder === 'string') {
            this.setState({ placeholder });
            return;
        }

        let reader = new FileReader();

        reader.onloadend = (event) => {
            this.setState({ placeholder: event.target.result });
        };
        reader.readAsDataURL(placeholder);
    }

    loadImage(image) {

        if (typeof image === 'string') {
            this.setState({ image });
            return;
        }

        let reader = new FileReader();

        reader.onloadend = (event) => {
            this.setState({image: event.target.result});
        };
        reader.readAsDataURL(image);
    }

    componentDidMount() {
        if (this.props.image) {
            this.loadImage(this.props.image);
        }
        if (this.props.placeholder) {
            this.loadPlaceholder(this.props.placeholder);
        }
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        if (!this.state.image && !this.state.placeholder) {
            return (<div />);
        }
        let render = this.state.image;
        if (!render) {
            render = this.state.placeholder;
        }
        return (<div onClick={this.props.onClick} className={this.props.className || `dropzone__image-preview dropzone__image-preview__xs`} style={{ backgroundImage: `url(${render})` }} />);
    }
}

/**
 * Upload input properties validation.
 */
ImagePreview.propTypes = {
    className: PropTypes.string,
    image: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(File)]),
    placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(File)])
};
