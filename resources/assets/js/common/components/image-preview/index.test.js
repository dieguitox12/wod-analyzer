import React from 'react';
import renderer from 'react-test-renderer';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import ImagePreview from './';

configure({ adapter: new Adapter() });

describe('ImagePreview Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <ImagePreview image={'image.jpg'} />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
    test('loadImage method if image is a string', () => {
        const wrapper = shallow(
            <ImagePreview image={'image.png'}/>
        );
        expect(wrapper.instance().state).toEqual({image: 'image.png', placeholder: null});

        wrapper.instance().loadImage('image.jpg');

        expect(wrapper.instance().state).toEqual({image: 'image.jpg', placeholder: null});
    });

    test('loadPlaceholder method if image is a string', () => {
        const wrapper = shallow(
            <ImagePreview placeholder={'image.png'} />
        );
        expect(wrapper.instance().state).toEqual({ placeholder: 'image.png', image: null });

        wrapper.instance().loadPlaceholder('image.jpg');

        expect(wrapper.instance().state).toEqual({ placeholder: 'image.jpg', image: null });
    });
});

