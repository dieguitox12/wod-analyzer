import React from 'react';
import PropTypes from 'prop-types';

/**
 * A simple, reusable component for displaying label-value pair of
 * informations.
 */
export default class InformationField extends React.PureComponent {
    /**
   * Renders the element.
   *
   * @return {ReactComponent}
   */
    render() {
        return (
            <div>
                <h5 className="display--block mgB">{this.props.label}</h5>
                <span style={this.props.valueStyle} className="">{this.props.value}</span>
            </div>
        );
    }
}

/**
 * Information Field properties validation.
 */
InformationField.propTypes = {
    label: PropTypes.string,
    valueStyle: PropTypes.object,
    value: PropTypes.string
};
