import React from 'react';
import renderer from 'react-test-renderer';

import InformationField from './';

describe('InformationField Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <InformationField
                    label={'Name'}
                    value={'John Doe'}
                />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

