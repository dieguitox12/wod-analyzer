import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { slide as Menu } from 'react-burger-menu';
import Media from 'react-media';

import Dropdown from 'common/components/dropdown';
import {
    CHART_LINE_ICON,
    PLUS_CIRCLE_ICON,
    CREVRON_DOWN_ICON
} from 'common/constants/icon.constant';
import ImagePreview from 'common/components/image-preview';
import { GENDERS } from '../../../constants/enums.constant';

/**
 * Displays the dashboard's header.
 */
export default class Header extends React.PureComponent {
    /**
     * Initializes the component.
     */
    constructor(props) {
        super(props);

        this.state = {};

        this.logOut = this.logOut.bind(this);
    }

    renderHamburguerMenu() {
        return (
            <Menu pageWrapId={'page-wrap'} right isOpen={false}>
                <div className="d-flex align-items-center mgB3">
                    <div>
                        <ImagePreview
                            className="user-image"
                            image={this.props.user.picture}
                            placeholder={this.props.user.gender.id === GENDERS.FEMALE ? '/images/female.svg' : '/images/male.svg'}/>
                    </div>
                    <div>
                        <span className="user-name">
                            {this.props.user.name}
                        </span>
                    </div>
                </div>
                <NavLink className={this.props.pathname === '/scores' ? 'bm-item--active' : ''} to="/scores">
                    <span className="label">
                        <span className="fl__icon fl__icon--left mgR">
                            <i className={CHART_LINE_ICON} />
                        </span>
                        Wods
                    </span>
                </NavLink>
                <NavLink className={this.props.pathname === '/scores/log' ? 'bm-item--active' : ''} to="/scores/log">
                    <span className="label">
                        <span className="fl__icon fl__icon--left mgR">
                            <i className={PLUS_CIRCLE_ICON} />
                        </span>
                        Log
                    </span>
                </NavLink>
                <NavLink className={this.props.pathname === '/my-profile' ? 'bm-item--active' : ''} to="/my-profile">
                    My profile
                </NavLink>
                <a href="#" onClick={this.logOut}>Sign out</a>
            </Menu>
        );
    }

    renderNormalMenu() {
        return (
            <ul className="layout-header__nav">
                <li>
                    <NavLink to="/scores">
                        <span className="label">
                            <span className="fl__icon fl__icon--left mgR">
                                <i className={CHART_LINE_ICON} />
                            </span>
                        </span>
                    </NavLink>
                </li>
                <li>
                    <NavLink to="/scores/log">
                        <span className="label">
                            <span className="fl__icon fl__icon--left mgR">
                                <i className={PLUS_CIRCLE_ICON} />
                            </span>
                        </span>
                    </NavLink>
                </li>
                <li className="divider" />
                <li className="layout-header__dropdown">
                    <Dropdown
                        align="right"
                        content={
                            <div className="d-flex align-items-center ">
                                <div>
                                    <ImagePreview
                                        className="user-image"
                                        image={this.props.user.picture}
                                        placeholder={this.props.user.gender.id === GENDERS.FEMALE ? '/images/female.svg' : '/images/male.svg'}/>
                                </div>
                                <div>
                                    <span className="user-name">
                                        {this.props.user.name}
                                        <i className={CREVRON_DOWN_ICON}></i>
                                    </span>
                                </div>
                            </div>
                        }>
                        <li>
                            <NavLink className="dropdown__item" to="/my-profile">
                                My Profile
                            </NavLink>
                        </li>
                        <li role="separator" className="divider" />
                        <li><a className="dropdown__item" href="#" onClick={this.logOut}>Sign out</a></li>
                    </Dropdown>
                </li>
            </ul>
        );
    }


    /**
     * Close the user's session and refreshes the page.
     */
    logOut() {
        this.props.onLogout();
    }

    /**
     * Renders the authenticated user's routes.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <header className="layout-header">
                <h1 className="layout-header__title">
                    <NavLink
                        to="/"
                        className="layout-header__logo">
                        {/*<strong className="fs-36 text--white">Wod Analyzer</strong>*/}
                    </NavLink>
                </h1>
                <span style={{flex: '1'}}></span>
                <Media query='(max-width: 700px)'>
                    {matches =>
                        matches ? (
                            this.renderHamburguerMenu()
                        ) : (
                            this.renderNormalMenu()
                        )
                    }
                </Media>
            </header>
        );
    }
}

Header.propTypes = {
    user: PropTypes.object.isRequired,
    onLogout: PropTypes.func.isRequired,
    pathname: PropTypes.string.isRequired
};
