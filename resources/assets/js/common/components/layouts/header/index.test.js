import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router';

import {
    mockUsers,
    mockNotifications
} from 'mock/user.mock';

import Header from './';

window.matchMedia = () => ({
    addListener: () => {},
    removeListener: () => {}
});

describe('Header Component', () => {
    test('renders without crashing without notifications', () => {

        const tree = renderer
            .create(
                <MemoryRouter initialEntries={['/']}>
                    <Header
                        notifications={[]}
                        user={mockUsers[0]}
                        onChange={() => { return 0; }}
                    />
                </MemoryRouter>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    test('renders without crashing with notifications', () => {

        const tree = renderer
            .create(
                <MemoryRouter initialEntries={['/']}>
                    <Header
                        notifications={mockNotifications}
                        user={mockUsers[0]}
                        onChange={() => { return 0; }}
                    />
                </MemoryRouter>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

