import React from 'react';
import PropTypes from 'prop-types';

import { NavLink } from 'react-router-dom';

export default class SidebarLink extends React.Component {

    render() {
        return (
            <li className="layout-sidebar__item">
                <NavLink
                    exact={this.props.exact}
                    to={this.props.href}
                    className="layout-sidebar__link"
                    activeClassName="layout-sidebar__link--active">
                    <i className={ `layout-sidebar__link__icon ${this.props.icon}` }></i> <span className="layout-sidebar__link__text">{ this.props.title }</span>
                </NavLink>
            </li>
        );
    }
}


SidebarLink.propTypes = {
    exact: PropTypes.bool,
    title: PropTypes.string,
    href: PropTypes.string,
    icon: PropTypes.string
};
