import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router';

import Sidebar from './';

describe('Sidebar Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <MemoryRouter>
                    <Sidebar
                        filters={{q: '', from: '', to: ''}}
                        onChange={() => { return 0; }}
                    />
                </MemoryRouter>

            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

