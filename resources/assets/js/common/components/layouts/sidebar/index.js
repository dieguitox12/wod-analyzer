import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import {
    SearchInput,
    DatetimePicker
} from 'common/components/inputs';

export default class Sidebar extends React.PureComponent {

    render() {
        return (
            <aside className="layout-sidebar">
                <nav className="layout-sidebar__nav">
                    <h1 className="layout-sidebar__title">
                        <NavLink
                            to="/"
                            className="layout-sidebar__logo">
                            {/*<strong className="fs-36">Wod Analyzer</strong>*/}
                        </NavLink>
                    </h1>
                    <div className="mgB2">
                        <SearchInput
                            weight={500}
                            value={this.props.filters.q}
                            label="Search"
                            onChange={this.props.onSearchQueryChange}
                        />
                    </div>
                    <div className="mgB2">
                        <DatetimePicker
                            label="From"
                            floating={true}
                            withIcon={true}
                            placeholder="Select date"
                            className="full-width"
                            onChange={this.props.onFromChange}
                            value={this.props.filters.from}
                        />
                    </div>
                    <div className="mgB2">
                        <DatetimePicker
                            label="To"
                            withIcon={true}
                            floating={true}
                            placeholder="Select date"
                            className="full-width"
                            onChange={this.props.onToChange}
                            value={this.props.filters.to}
                        />
                    </div>
                </nav>
            </aside>
        );
    }
}

Sidebar.propTypes = {
    filters: PropTypes.object,
    onToChange: PropTypes.func.isRequired,
    onFromChange: PropTypes.func.isRequired,
    onSearchQueryChange: PropTypes.func.isRequired
};
