import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';

import { NextArrow, PrevArrow } from './slick-arrows';

/**
 * A variant of the original Slick which includes custom prev and next buttons.
 */
export default class SlickCarroucel extends React.PureComponent {
    constructor(props) {
        super(props);

        this.nextArrow = <NextArrow slidesToShow={props.slidesToShow} />;
        this.prevArrow = <PrevArrow slidesToShow={props.slidesToShow} />;
    }

    componentWillReceiveProps(nextProps) {
        this.nextArrow = <NextArrow slidesToShow={nextProps.slidesToShow} />;
        this.prevArrow = <PrevArrow slidesToShow={nextProps.slidesToShow} />;
    }

    /**
     * Renders the element. If there's no children, then renders an
     * empty div.
     *
     * @return {ReactComponent}
     */
    render() {
        return React.Children.count(this.props.children) !== 0
            ? (
                <Slider
                    {...this.props.options}
                    nextArrow={this.nextArrow}
                    prevArrow={this.prevArrow}
                    slidesToShow={this.props.slidesToShow}
                    slidesToScroll={this.props.slidesToScroll}>
                    {this.props.children}
                </Slider>
            )
            : <div />;
    }
}

/**
 * Default props
 */
SlickCarroucel.defaultProps = {
    options : {
        dots: false,
        infinite: false,
        draggable: false,
        speed: 500
    },
    slidesToShow: 4,
    slidesToScroll: 4
};

/**
 * Slick properties validation.
 */
SlickCarroucel.propTypes = {
    options: PropTypes.object,
    slidesToShow: PropTypes.number,
    slidesToScroll: PropTypes.number,
    children: PropTypes.node.isRequired
};
