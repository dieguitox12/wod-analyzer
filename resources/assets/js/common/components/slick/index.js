import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';

export default class Slick extends React.Component {
    constructor(props) {
        super(props);

        // Sets the slicker's options
        this.options = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
        };

        // If we got option properties, then we'll merge them with the default ones.

        if (props.options) {
            this.options = Object.assign(this.options, props.options);
        }
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <Slider {...this.options}>
                {this.props.children}
            </Slider>
        );
    }
}

/**
 * Slick properties validation.
 */
Slick.propTypes = {
    options: PropTypes.object,
    children: PropTypes.node.isRequired
};
