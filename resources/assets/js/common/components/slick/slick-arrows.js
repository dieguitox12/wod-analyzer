import React from 'react';
import PropTypes from 'prop-types';

import {
    CREVRON_LEFT_ICON,
    CREVRON_RIGHT_ICON
} from 'common/constants/icon.constant';

/**
 * Next arrow component.
 */
class NextArrow extends React.PureComponent {

    render() {
        const render = this.props.slidesToShow + this.props.currentSlide < this.props.slideCount;

        if (!render) {
            return <div />;
        }

        return (
            <button
                onClick={this.props.onClick}
                type="button"
                data-role="none"
                className={this.props.className}>
                <i className={CREVRON_RIGHT_ICON} />
            </button>
        );
    }
}

/**
 * Previous arrow component.
 */
class PrevArrow extends React.PureComponent {
    render() {
        const render = this.props.currentSlide !== 0;

        if (!render) {
            return <div />;
        }

        return (
            <button
                onClick={this.props.onClick}
                type="button"
                data-role="none"
                className={this.props.className}>
                <i className={CREVRON_LEFT_ICON} />
            </button>
        );
    }
}


/** PropTypes **/
NextArrow.propTypes = PrevArrow.propTypes = {
    slidesToShow: PropTypes.number,
    currentSlide: PropTypes.number,
    slideCount: PropTypes.number,
    className: PropTypes.string,
    onClick: PropTypes.func
};

/** Export **/
export { NextArrow, PrevArrow };
