'use strict';

import React from 'react';
import PropTypes from 'prop-types';

const Tag = props => {
    return (
        <div className="Tag Select--multi">
            <div className="Select-value ">
                <span className="Select-value-label" role="option" aria-selected="true">
                    {props.label}
                    <span className="Select-aria-only">&nbsp;</span>
                </span>
                {props.onClose && (
                    <span
                        className="Select-value-icon"
                        value={props.value}
                        id={props.id}
                        onClick={e => props.onClose(e.currentTarget)}
                        aria-hidden="true"
                    >
                        ×
                    </span>
                )}
            </div>
        </div>
    );
};

/**
 * Statistics Card properties validations.
 */
Tag.propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    onClose: PropTypes.func
};

export default Tag;
