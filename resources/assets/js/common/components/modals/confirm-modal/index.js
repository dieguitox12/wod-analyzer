
import React from 'react';
import PropTypes from 'prop-types';
import SweetAlert from 'sweetalert-react';

/**
 * A simple modal to confirm an user's action.
 */
export default class ConfirmModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = { show: true };

        this.onCancel = this.onCancel.bind(this);
        this.onSuccess = this.onSuccess.bind(this);
    }

    /**
     * Fired once the user cancels the modal.
     */
    onCancel() {
        this.setState({ show: false }, this.props.onCancel);
        document.body.classList.remove('stop-scrolling');
    }

    /**
     * Fired once the user accepts the modal's action.
     */
    onSuccess() {
        this.setState({ show: false }, this.props.onSuccess);
        document.body.classList.remove('stop-scrolling');
    }

    /**
     * Fired once the component is about to receive new props.
     */
    componentWillReceiveProps() {
        this.setState({ show: true });
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <SweetAlert
                customClass={`confirmation-modal ${this.props.className}`}
                show={this.state.show}
                showCancelButton={this.props.onCancel != null}
                text={this.props.text}
                cancelButtonText={this.props.cancelButtonText || 'Cancel'}
                confirmButtonText={this.props.confirmButtonText || 'Yes'}
                title={this.props.title}
                type={this.props.type}
                onCancel={this.onCancel}
                onConfirm={this.onSuccess}
            />
        );
    }

}

ConfirmModal.defaultProps = {
    className: '',
    type: 'info',
    showCancelButton: false
};

/**
 * ConfirmModal properties validation.
 */
ConfirmModal.propTypes = {
    className: PropTypes.string,
    type: PropTypes.string,
    title: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    onSuccess: PropTypes.func.isRequired,
    onCancel: PropTypes.func
};
