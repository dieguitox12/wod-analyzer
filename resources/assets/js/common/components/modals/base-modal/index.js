import React from 'react';
import PropTypes from 'prop-types';
import ReactModal from 'react-modal';

import { CLOSE_ICON } from 'common/constants/icon.constant';

/**
 * A simple modal to confirm an user's action.
 */
export default class Modal extends React.Component {

    render () {
        return (
            <div>
                <ReactModal
                    className={`modal__content ${this.props.className}`}
                    overlayClassName={`modal__overlay ${this.props.overlayClassName}`}
                    bodyOpenClassName={`modal--is-open`}
                    isOpen={this.props.showModal}
                    ariaHideApp={false}
                    contentLabel="Minimal Modal Example">
                    {this.props.showHeader &&
                        <div>
                            <div className="row">
                                <div className="align-center col-md-6 text--align-left">
                                    <h4 className="">{this.props.title}</h4>
                                </div>
                                <div className="col-md-6 text--align-right">
                                    <span style={{fontSize: '26px'}} className="cursor-pointer" onClick={this.props.onModalClose}><i className={CLOSE_ICON}></i></span>
                                </div>
                            </div>
                            <hr />
                        </div>
                    }
                    <div className="col-md-12">
                        {this.props.children}
                    </div>
                </ReactModal>
            </div>
        );
    }

}

Modal.defaultProps = {
    className: '',
    showModal: false,
    overlayClassName: '',
    showHeader: true
};

/**
 * Modal properties validation.
 */
Modal.propTypes = {
    onModalClose: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
    showModal: PropTypes.bool,
    showHeader: PropTypes.bool,
    className: PropTypes.string,
    overlayClassName: PropTypes.string,
    bodyOpenClassName: PropTypes.string,
    onSuccess: PropTypes.func
};
