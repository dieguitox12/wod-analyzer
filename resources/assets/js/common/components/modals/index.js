import BaseModal from './base-modal';
import ConfirmModal from './confirm-modal';


export {
    BaseModal,
    ConfirmModal
};
