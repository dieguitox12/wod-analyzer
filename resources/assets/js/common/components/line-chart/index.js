import React from 'react';
import PropTypes from 'prop-types';
import { Line } from 'react-chartjs-2';
/* eslint-disable no-unused-vars */
import * as zoom from 'chartjs-plugin-zoom';
import 'chartjs-plugin-dragdata';

import Media from 'react-media';

import {
    getDefaultOptions,
    createColoredDataset
} from './chart.default';
import secondsToMinutes from 'common/helpers/seconds-to-minutes';

export default class LineChart extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            data: createColoredDataset(this.props.data, this.props.labels),
            options: Object.assign({}, getDefaultOptions(this.props.data.length > 1)),
            areas: [],
            objectiveArea: 0
        };

        this.state.options['lineTension'] = this.props.lineTension || 0;
        if (this.props.zoom) {
            // options['pan'] = {
            //     enabled: true,
            //     mode: 'y',
            // };
            // options['zoom'] = {
            //     enabled: true,
            //     mode: 'y'
            // };
        }
        this.state.options['tooltips'] = {
            callbacks: {
                label: (tooltipItem, data) => {
                    let label = ['', ''];
                    let sum = 0;
                    for (let index = 0; index < tooltipItem.index + 1; index++) {
                        sum += data.datasets[tooltipItem.datasetIndex].data[index];
                    }
                    if (this.props.isTime) {
                        label[0] += 'Time: ';
                        label[0] += secondsToMinutes(tooltipItem.yLabel) + ' mins';
                        sum = secondsToMinutes(sum) + ' mins';
                    } else {
                        label[0] += 'Reps:';
                        label[0] += tooltipItem.yLabel;
                    }
                    label[1] += 'Total: ' + sum;
                    return label;
                }
            }
        };
        this.state.options['dragData'] = true,
        this.state.options['dragX'] = false,
        this.state.options['dragDataRound'] = 1,
        this.state.options['onDragStart'] = (event, element) => {
            this.state.objectiveArea = 0;
            let value1 = this.state.data.datasets[element._datasetIndex].data[element._index];
            let value2 = this.state.data.datasets[element._datasetIndex].data[element._index + 1] ||
                this.state.data.datasets[element._datasetIndex].data[element._index - 1];
            if (value1 > value2) {
                this.state.objectiveArea = value2 + ((value1 - value2) / 2);
            } else if (value1 < value2) {
                this.state.objectiveArea = value1 + ((value2 - value1) / 2);
            } else {
                this.state.objectiveArea = value1;
            }
        };
        this.state.options['onDrag'] = (event, datasetIndex, index, value) => {
            if (this.state.data.datasets[datasetIndex].data[index + 1]) {
                this.state.data.datasets[datasetIndex].data[index + 1] = this.calculateMissingValue(
                    value,
                    this.state.data.datasets[datasetIndex].data[index + 1]
                );
            } else {
                this.state.data.datasets[datasetIndex].data[index - 1] = this.calculateMissingValue(
                    value,
                    this.state.data.datasets[datasetIndex].data[index - 1]
                );
            }
            return true;
        };
        let max = this.state.data.datasets.map(element => {
            return Math.max.apply(null, element.data.filter(element => !isNaN(element) && element));
        });
        let min = this.state.data.datasets.map(element => {
            return Math.min.apply(null, element.data.filter(element => !isNaN(element) && element));
        });
        if (props.detailed) {
            this.state.options.scales.yAxes[0].ticks.max = Math.max.apply(null, max) + 30;
            this.state.options.scales.yAxes[0].ticks.min = Math.min.apply(null, min) - 30;
        } else {
            delete this.state.options.scales.yAxes[0].ticks.max;
            delete this.state.options.scales.yAxes[0].ticks.min;
        }

    }

    calculateMissingValue(value1, value2) {
        let missingValue = 0;
        if (value1 !== value2) {
            missingValue = (this.state.objectiveArea * 2) - value1;
        } else {
            missingValue = value1;
        }

        return missingValue;
    }

    calculateArea(data) {
        let individualAreas = [0];
        for (let index = 0; index < data.length; index++) {
            if (data[index + 1]) {
                if (data[index] !== data[index + 1]) {
                    individualAreas.push(
                        (data[index + 1] + data[index]) / 2
                    );
                } else {
                    individualAreas.push(data[index]);
                }
            }
        }
        return individualAreas.reduce((prev, curr) => { return prev + curr; });
    }

    componentDidMount() {
        let clickCanvas = (e) => {
            e.preventDefault();
        };
        [].forEach.call(document.getElementsByClassName('chartjs-render-monitor'), (element) => {
            element.addEventListener('mousedown', clickCanvas);
            element.addEventListener('touchend', clickCanvas);
            element.onselectstart = () => {
                return false;
            };
        });
    }

    componentWillReceiveProps(nextProps) {
        let newData = createColoredDataset(nextProps.data, nextProps.labels);
        let areas = newData.datasets.map(element => {
            return this.calculateArea(element.data.filter(element => !isNaN(element) && element));
        });
        let max = newData.datasets.map(element => {
            return Math.max.apply(null, element.data.filter(element => !isNaN(element) && element));
        });
        let min = newData.datasets.map(element => {
            return Math.min.apply(null, element.data.filter(element => !isNaN(element) && element));
        });
        let options = Object.assign({}, getDefaultOptions(nextProps.data.length > 1), this.state.options);
        this.setState({
            options: options,
            data: newData,
            areas
        });
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <div>
                <p>{this.props.yLabel || ''}</p>
                <Media query='(max-width: 399px)'>
                    {matches =>
                        matches ? (
                            <Line
                                className={`chart line-chart ${this.props.className}`}
                                data={this.state.data}
                                options={Object.assign({}, this.state.options)}
                                width={2800}
                                height={2400}
                            />
                        ) : (
                            <Line
                                className={`chart line-chart ${this.props.className}`}
                                data={this.state.data}
                                options={Object.assign({}, this.state.options)}
                                width={800}
                                height={400}
                            />
                        )
                    }
                </Media>

                <p className='text--align-center'>{this.props.xLabel || ''}</p>
            </div>

        );
    }
}

LineChart.defaultProps = {className: '', isTime: false};


/**
 * Properties validations.
 */
LineChart.propTypes = {
    lineTension: PropTypes.number,
    className: PropTypes.string,
    isTime: PropTypes.bool,
    labels: PropTypes.arrayOf(PropTypes.string).isRequired,
    data: PropTypes.array.isRequired
};
