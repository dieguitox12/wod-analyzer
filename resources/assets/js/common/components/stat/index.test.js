import React from 'react';
import renderer from 'react-test-renderer';

import Stat from './';

describe('Stat Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <Stat
                    text={'Active Users'}
                    value={0}
                />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

