import React from 'react';
import PropTypes from 'prop-types';


const Stat = (props) => {
    return (
        <div className={`stat ${props.className}`}>
            <div className="stat__icon">
                <i className={`${props.icon}`}></i>
            </div>
            <div className="stat__description">
                <p className="text--weight-bold  fs-18">{props.value}</p>
                <p>{props.text}</p>
            </div>
        </div>
    );
};


Stat.defaultProps = {
    className: '',
    icon: 'mdi mdi-human',
    text: '',
    value: 0
};


/**
 * Statistics Card properties validations.
 */
Stat.propTypes = {
    className: PropTypes.string,
    icon: PropTypes.string,
    text: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
};


export default Stat;
