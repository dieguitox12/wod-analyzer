import React from 'react';
import ReactLoading from 'react-loading';
import PropTypes from 'prop-types';

export default class LoadingComponent extends React.Component {

    render() {
        return (
            <div className="layout-body__content loading">
                <ReactLoading
                    type={'bars'}
                    color={'#003833'}
                    height={this.props.height}
                    width={this.props.width} />
            </div>
        );
    }

}

LoadingComponent.propTypes = {
    height: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired
};
