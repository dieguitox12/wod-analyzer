'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import ReactDOM from 'react-dom';

import { WOD_TYPES } from 'common/constants/enums.constant';
import scoreToString from 'common/helpers/score-to-string';
import LineChart from 'common/components/line-chart';
import wodToString from 'common/helpers/wod-to-string';
import {
    ConfirmModal
} from 'common/components/modals';

export default class WodScoreCard extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            labels: [],
            data: [],
            comparing: false,
            optimizing: false,
            previousData: [],
            optimizations: []
        };

        this.state.labels = props.wodScore.wodScore.scores.map(element => {
            return `Part ${element.part}`;
        });
        this.state.labels.unshift('0');
        this.state.labels.push('');
        this.state.data.push({data: [], label: '', dragData: false});
        this.state.data[0].data = props.wodScore.wodScore.scores.map(element => {
            return element.value;
        });
        this.state.data[0].data.unshift(null);
        this.state.data[0].label = moment(props.wodScore.wodScore.createdAt).format('lll');

        this.onCompare = this.onCompare.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onOptimize = this.onOptimize.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        let optimizations = [];
        let newData = this.state.data.slice();
        if (nextProps.optimizations) {
            let better = {
                data: nextProps.optimizations.better.scores.map(element2 => {
                    return element2.value;
                }),
                label: 'Suggestion 2'
            };
            better.data.unshift(null);
            let same = {
                data: nextProps.optimizations.same.scores.map(element2 => {
                    return element2.value;
                }),
                label: 'Suggestion 1'
            };
            same.data.unshift(null);
            optimizations.push(same);
            optimizations.push(better);

            newData[0].label = 'Actual score';
        } else {
            newData[0].label = moment(this.props.wodScore.wodScore.createdAt).format('lll');
        }
        this.setState({optimizations: optimizations, data: newData});
    }

    onOptimize() {
        this.setState({optimizing: !this.state.optimizing});
        if (!this.state.optimizing) {
            this.props.onOptimize(this.props.wodScore.wodScore.id);
        } else {
            this.props.resetOptimizations();
        }
    }

    onCancel() {
        ReactDOM.unmountComponentAtNode(document.getElementById('modals'));
    }

    onCompare() {
        if (this.props.wodScore.previousScores.length === 0) {
            ReactDOM.render(
                <ConfirmModal
                    title="Sorry"
                    text="There are no previous scores that match the criteria of this one."
                    onSuccess={this.onCancel}
                />
                , document.getElementById('modals')
            );
        } else {
            let data = this.state.previousData.slice();
            if (data.length === 0) {
                this.props.wodScore.previousScores.forEach(element => {
                    let newData = {
                        data: element.wodScore.scores.map(element2 => {
                            return element2.value;
                        }),
                        label: moment(element.wodScore.createdAt).format('lll') + '--' + element.wodScore.observations,
                        dragData: false
                    };
                    newData.data.unshift(null);
                    data.push(newData);
                });
            }
            this.setState({
                previousData: data,
                comparing: !this.state.comparing
            });
        }
    }

    render() {
        let wodType = '';
        let xLabel = '';
        let yLabel = '';
        let totalScore = scoreToString(this.props.wodScore);
        if (this.props.wodScore.rounds && this.props.wodScore.rounds === 1) {
            wodType = WOD_TYPES.FOR_TIME;
            yLabel = 'Seconds';
            xLabel = 'Intervals';
        } else if (this.props.wodScore.rounds && this.props.wodScore.rounds > 1) {
            wodType = WOD_TYPES.ROUNDS_FOR_TIME;
            yLabel = 'Seconds';
            xLabel = 'Intervals';
        } else if (!this.props.wodScore.rounds && this.props.wodScore.timeCap) {
            wodType = WOD_TYPES.AMRAP;
            yLabel = 'Reps';
            xLabel = 'Intervals';
        }
        let data = this.state.data.slice();
        if (this.state.comparing) {
            data = data.concat(this.state.previousData);
        }
        if (this.state.optimizing) {
            data = data.concat(this.state.optimizations);
        }
        return (
            <div
                onClick={() => { this.props.onClick ? this.props.onClick(this.props.wodScore.wodScore.id) : null; }}
                className={`wod-score-card ${!this.props.detailed ? 'cursor-pointer' : ''}`} style={this.props.style}>
                <div className="wod-score-card__content">
                    <div className="d-flex mgB">
                        <span className="pdL0 col-8">
                            <h5>{this.props.wodScore.name} ({wodType}) </h5>
                            <p>{moment(this.props.wodScore.wodScore.createdAt).format('lll')}</p>
                        </span>
                        <span className="col-4 pdR0 text--align-right mgB">
                            <p>Total score:</p>
                            <p>{totalScore}</p>
                        </span>
                    </div>
                    {this.props.detailed &&
                        <div className="row">
                            <span className="col-6">
                                <p className="breakLine">{wodToString(this.props.wodScore)}</p>
                            </span>
                            <span className="col-6">
                                <p>Observations:</p>
                                <p className="breakLine">{this.props.wodScore.wodScore.observations}</p>
                            </span>
                        </div>
                    }
                    <div className="row">
                        <div
                            className={`col-12`}>
                            <div className="line-chart">
                                <LineChart
                                    xLabel={xLabel}
                                    yLabel={yLabel}
                                    isTime={this.props.wodScore.rounds > 0}
                                    zoom={this.props.detailed}
                                    detailed={this.props.detailed}
                                    labels={this.state.labels}
                                    data={data}
                                />
                            </div>
                            <div>
                                <div className="d-flex mgB">
                                    <span className="pdL0 col-6">
                                        {this.props.detailed &&
                                            <button
                                                onClick={this.onCompare}
                                                className="btn btn-outline">
                                                {`${!this.state.comparing ? 'Compare previous' : 'Stop comparing'}`}
                                            </button>
                                        }
                                    </span>
                                    <span className="col-6 pdR0 text--align-right">
                                        {this.props.detailed &&
                                            <button
                                                onClick={this.onOptimize}
                                                className="btn btn-primary">
                                                {`${!this.state.optimizing ? 'Optimize' : 'Stop optimization'}`}
                                            </button>
                                        }
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

WodScoreCard.propTypes = {wodScore: PropTypes.object.isRequired, style: PropTypes.object, optimizations: PropTypes.object};
