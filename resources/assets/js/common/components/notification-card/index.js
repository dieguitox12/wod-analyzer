'use strict';

import React from 'react';
import PropTypes from 'prop-types';

const NotificationCard = ({
    icon,
    variant,
    centered,
    seen,
    iconHeight,
    iconWidth,
    iconSize,
    content,
    contentIsHtml,
    className,
    ...props
}) => {
    let extraClasses = `${variant ? 'notification-card__variant' : ''}`;
    extraClasses = `${extraClasses} text-left ${centered ? 'notification-card__centered' : ''}`;
    extraClasses = `${extraClasses} ${seen ? 'notification-card__seen' : ''}`;
    const classNames = `row align-items-center no-gutters pd2 notification-card ${extraClasses} ${className}`.trim();
    return (
        <div className={classNames} style={props.style}>
            <div className="notification-card__image col-2">
                <div
                    className="notification-card__thumbnail"
                    style={{
                        height: `${iconHeight}`,
                        width: `${iconWidth}`,
                        fontSize: `${iconSize}`,
                        backgroundColor: 'rgba(0, 125, 195, 0.08)',
                        borderRadius: '50%',
                        margin: 'auto',
                        paddingTop: '20px',
                        paddingLeft: '13px',
                        paddingRight: '7px'
                    }}>
                    <i className={icon} />
                </div>
            </div>
            <div className="notification-card__content col-10">
                {contentIsHtml &&
                    <span className="notification-card__content" dangerouslySetInnerHTML={{__html: content}}></span>
                }
                {!contentIsHtml &&
                    <span className="notification-card__content"><p>{content}</p></span>
                }
            </div>
        </div>
    );
};

NotificationCard.defaultProps = {
    image: '/images/user-placeholder.svg',
    content: '',
    centered: false
};

/**
 * Statistics Card properties validations.
 */
NotificationCard.propTypes = {
    centered: PropTypes.bool,
    style: PropTypes.object,
    iconWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    imgMinHeight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    contentIsHtml: PropTypes.bool,
    content: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    image: PropTypes.string,
    variant: PropTypes.bool
};

export default NotificationCard;
