'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

const SuspensionNote = (props) => {
    return (
        <div className="text--normal">
            <small className="text--graymedium">Nota</small>
            <p className="text--italic">{props.note.note}</p>
            <small className="text--graymedium">Por <span className="text--weight-bold text--normal">{props.note.createdBy.name}</span></small>
            <br/>
            <small className="text--graymedium">{moment(props.note.createdAt).format('DD [de] MMMM[,] YYYY')}</small>
        </div>
    );
};


/**
 * Statistics Card properties validations.
 */
SuspensionNote.propTypes = {note: PropTypes.object.isRequired};


export default SuspensionNote;
