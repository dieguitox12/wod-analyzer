import React from "react";
import ReactPaginate from "react-paginate";
import PropTypes from "prop-types";

import {
    PAGINATION_LEFT_ICON,
    PAGINATION_RIGHT_ICON
} from 'common/constants/icon.constant';

export default class Pagination extends React.Component {

    render() {
        return (
            <ReactPaginate
                disabledClassName="pagination__item--no-display"
                pageClassName="pagination__item"
                nextClassName="pagination__item"
                previousClassName="pagination__item"
                breakClassName="pagination__item pagination__item--break"
                activeClassName="pagination__item--active"
                pageLinkClassName="pagination__item__archor"
                breakLabel="..."
                pageRangeDisplayed={3}
                marginPagesDisplayed={1}
                onPageChange={this.props.onPageChange}
                forcePage={this.props.forcePage}
                nextLabel={<span className={PAGINATION_RIGHT_ICON} />}
                previousLabel={<span className={PAGINATION_LEFT_ICON} />}
                pageCount={ this.props.pageCount } />
        );
    }
}


Pagination.propTypes = {
    onPageChange: PropTypes.func.isRequired,
    forcePage: PropTypes.number,
    pageCount: PropTypes.number.isRequired
};
