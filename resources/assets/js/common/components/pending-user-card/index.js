'use strict';

import React from 'react';
import PropTypes from 'prop-types';

const PendingUserCard = ({
    name,
    image,
    variant,
    centered,
    imgHeight,
    imgWidth,
    subject,
    actionValue,
    actionName,
    actionClass,
    onActionClick,
    actionText,
    className,
    ...props
}) => {
    let extraClasses = `${variant ? 'pending-user-card__variant' : ''}`;
    extraClasses = `${extraClasses} text-left ${centered ? 'pending-user-card__centered' : ''}`;
    const classNames = `row align-items-center no-gutters pending-user-card ${extraClasses} ${className}`.trim();
    return (
        <div className={classNames} style={props.style}>
            <div className="pending-user-card__image col-3">
                <div
                    className="pending-user-card__thumbnail"
                    style={{
                        backgroundImage: `url(${image || '/images/user-placeholder.svg'})`,
                        height: `${imgHeight}`,
                        width: `${imgWidth}`
                    }}
                >
                    <img className="hidden" src={image} alt="Item image" />
                </div>
            </div>
            <div className="pending-user-card__content col-5">
                <h3 className={`text--weight-bold pending-user-card__name ${variant ? 'variant' : ''}`}>
                    {name}
                </h3>
                <span className="pending-user-card__subject">{subject}</span>
            </div>
            <div className="pending-user-card__action col-4">
                <button
                    value={actionValue}
                    name={actionName}
                    className={actionClass}
                    onClick={onActionClick}
                >
                    {actionText}
                </button>
            </div>
        </div>
    );
};

PendingUserCard.defaultProps = {
    name: '',
    image: '/images/user-placeholder.svg',
    subject: '',
    centered: false
};

/**
 * Statistics Card properties validations.
 */
PendingUserCard.propTypes = {
    centered: PropTypes.bool,
    style: PropTypes.object,
    imgWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    imgMinHeight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    name: PropTypes.string,
    subject: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    image: PropTypes.string,
    variant: PropTypes.bool
};

export default PendingUserCard;
