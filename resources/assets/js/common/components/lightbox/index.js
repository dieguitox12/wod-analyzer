import React from 'react';
import PropTypes from 'prop-types';
import Lightbox from 'react-images';

/**
 * Represents a simple light box component which displays a gallery
 * of pictures.
 */
export default class LightBox extends React.PureComponent {

    /**
     * Initializes the component.
     */
    constructor(props) {
        super(props);

        this.state = {currentImage: 0};

        // Events.
        this.onSetImage = this.onSetImage.bind(this);
        this.onNextImage = this.onNextImage.bind(this);
        this.onPrevImage = this.onPrevImage.bind(this);
    }

    /**
     * Fired once the user clicks to see the previous image.
     */
    onPrevImage() {
        this.setState({ currentImage: this.state.currentImage - 1 });
    }

    /**
     * Fired once the user clicks to see the next image.
     */
    onNextImage() {
        this.setState({ currentImage: this.state.currentImage + 1 });
    }

    /**
     * Fired once the user clicks a thumbnail to see a particular image.
     *
     * @param {number} currentImage  the index of the new image.
     */
    onSetImage(currentImage) {
        this.setState({ currentImage });
    }

    /**
     * Renders the element.
     *
     * @return {ReactComponent}
     */
    render() {
        return (
            <Lightbox
                showThumbnails
                backdropClosesModal
                isOpen={this.props.isOpen}
                onClose={this.props.onClose}
                onClickNext={this.onNextImage}
                onClickPrev={this.onPrevImage}
                onClickThumbnail={this.onSetImage}
                currentImage={this.state.currentImage}
                images={this.props.images.map(src => ({src}) )}
            />
        );
    }
}

/**
 * Lightbox default properties.
 */
LightBox.defaultProps = { images: [] };

/**
 * Lightbox properties validation.
 */
LightBox.propTypes = {
    onClose: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired,
    images: PropTypes.arrayOf(PropTypes.string)
};
