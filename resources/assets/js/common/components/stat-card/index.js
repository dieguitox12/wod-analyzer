'use strict';

import React from 'react';
import PropTypes from 'prop-types';


const StatCard = (props) => {
    return (
        <div className={`stat-card ${props.variant ? 'stat-card__variant' : ''} ${props.centered ? 'stat-card__centered' : ''}`} style={props.style}>
            {props.image &&
                <div className="stat-card__image">
                    <div className="stat-card__thumbnail" style={{ width: `${props.imgWidth}`, minHeight: `${props.imgMinHeight}`, backgroundImage: `url(${props.image || '/images/user-placeholder.svg'})` }}>
                        <img className="hidden" src={props.image} alt="Item image" />
                    </div>
                </div>
            }
            <div className="stat-card__content paragraph">
                <h3 className={`text--weight-bold ${props.variant ? 'variant' : ''}`}>{props.name}</h3>
                <p>{props.subject}</p>
                {props.children}
            </div>
        </div>
    );
};


StatCard.defaultProps = {
    name: '',
    image: '/images/user-placeholder.svg',
    subject: '',
    centered: false
};


/**
 * Statistics Card properties validations.
 */
StatCard.propTypes = {
    centered: PropTypes.bool,
    style: PropTypes.object,
    imgWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    imgMinHeight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    name: PropTypes.string,
    subject: PropTypes.string,
    image: PropTypes.string,
    variant: PropTypes.bool
};


export default StatCard;
