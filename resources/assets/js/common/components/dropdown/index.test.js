import React from 'react';
import renderer from 'react-test-renderer';

import Dropdown from './';

describe('Dropdown Component', () => {
    test('renders without crashing', () => {

        const tree = renderer
            .create(
                <Dropdown>
                    <li>Option 1</li>
                    <li>Option 2</li>
                    <li>Option 3</li>
                    <li>Option 4</li>
                </Dropdown>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});

