/**
 * The idea is to place all the regex that the application will use here.
 */

// Matches from 00:00 to 99:99.
export const isValidTime = /([0-9][0-9]):[0-9][0-9]/;
