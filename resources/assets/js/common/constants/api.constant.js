export const API_URL = process.env.MIX_API_URL;

export const API_KEY = {'X-API-KEY': process.env.MIX_API_KEY};
