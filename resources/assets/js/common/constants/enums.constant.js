export const PER_PAGE = 4;

export const MOVEMENTS = {RUN: 17};

export const WOD_TYPES = {
    AMRAP: 'AMRAP',
    FOR_TIME: 'For Time',
    ROUNDS_FOR_TIME: 'Rounds For Time'
};

export const MEASUREMENTS = [
    {id: 1, name: 'm'},
    {id: 2, name: 'cm'},
    {id: 3, name: 'ft'},
    {id: 4, name: 'km'},
    {id: 5, name: 'lb'},
    {id: 6, name: 'kg'},
    {id: 7, name: 'poods'},
    {id: 9, name: 'secs'},
    {id: 10, name: 'mins'}
];

export const REMAX_MEMBERSHIP = '250.00';

export const USERTYPES = {
    REAL_ASSISTANT: 100,
    ACCOUNT_OWNER: 1,
    BROKER: 2,
    ASSISTANT: 3,
    OWNER: 4,
    CUSTOMER: 5,
    ADMINISTRATOR: 6,
    SYSTEM: 7,
    SUPER_ADMIN: 8
};

export const NOTES = {
    ALL: 'Todos',
    NO_PAYMENT: 'Falta de pago',
    HEALTH: 'Licencia médica',
    PERSONAL_ISSUES: 'Motivos personales',
    CHANGE_OF_CARREER: 'Cambio de carrera',
    REPEATED: 'Duplicado',
    INDEPENDENT: 'Se independizó'
};

export const INVOICE_REASONS = {MEMBERSHIP: 1};

export const NOTIFICATION_REASONS = {
    INVOICE: 1,
    PAYMENT: 2
};

export const CURRENCIES = {
    PESO: 'DOP',
    DOLLAR: 'USD'
};

export const CURRENCIES_IDS = {
    PESO: 1,
    DOLLAR: 2
};

export const CONVERSION_RATE = {
    PESO_TO_DOLLAR: 0.02013287698,
    DOLLAR_TO_PESO: 49.67
};

export const PAYMENT_METHODS = {
    AUTHORIZEDOTNET: 1,
    PAYPAL: 2,
    CASH: 3,
    TRANSFERENCE: 4,
    CHEQUE: 5
};

export const GENDERS = {
    MALE: 1,
    FEMALE: 2
};

export const ACHIEVEMENT_CATEGORIES = {
    INDIVIDUAL: 1,
    TEAM: 2,
    OFFICE: 3
};

export const MONTHS = [
    "Ene",
    "Feb",
    "Mar",
    "Abr",
    "May",
    "Jun",
    "Jul",
    "Ago",
    "Sep",
    "Oct",
    "Nov",
    "Dic"
];

export const IDIOM_LEVELS = {
    BASIC: 1,
    MEDIUM: 2,
    ADVANCE: 3,
    NATIVE: 4
};

export const MEMBERSHIP_STATUS = {
    PENDING: 1,
    INVOICED: 2,
    PAYED: 3
};

let IDIOM_LEVELS_LABELS = {};

IDIOM_LEVELS_LABELS[IDIOM_LEVELS.BASIC] = 'Básico';
IDIOM_LEVELS_LABELS[IDIOM_LEVELS.MEDIUM] = 'Intermedio';
IDIOM_LEVELS_LABELS[IDIOM_LEVELS.ADVANCE] = 'Avanzado';
IDIOM_LEVELS_LABELS[IDIOM_LEVELS.NATIVE] = 'Nativo';

export const ACHIEVEMENT_PLACES = {
    FIRST: 1,
    SECOND: 2,
    THIRD: 3
};

let ACHIEVEMENT_PLACES_LABELS = {};

ACHIEVEMENT_PLACES_LABELS[ACHIEVEMENT_PLACES.FIRST] = '1er Lugar';
ACHIEVEMENT_PLACES_LABELS[ACHIEVEMENT_PLACES.SECOND] = '2do Lugar';
ACHIEVEMENT_PLACES_LABELS[ACHIEVEMENT_PLACES.THIRD] = '3er Lugar';

let ACHIEVEMENT_CATEGORIES_LABELS = {};

ACHIEVEMENT_CATEGORIES_LABELS[ACHIEVEMENT_CATEGORIES.INDIVIDUAL] = 'Individual';
ACHIEVEMENT_CATEGORIES_LABELS[ACHIEVEMENT_CATEGORIES.TEAM] = 'Equipo';
ACHIEVEMENT_CATEGORIES_LABELS[ACHIEVEMENT_CATEGORIES.OFFICE] = 'Oficina';

let USERTYPES_LABELS = {};

USERTYPES_LABELS[USERTYPES.ACCOUNT_OWNER] = 'Broker';
USERTYPES_LABELS[USERTYPES.BROKER] = 'Asociado senior';
USERTYPES_LABELS[USERTYPES.ASSISTANT] = 'Asociado Junior';
USERTYPES_LABELS[USERTYPES.REAL_ASSISTANT] = 'Asistente';
USERTYPES_LABELS[USERTYPES.OWNER] = 'Propietario';
USERTYPES_LABELS[USERTYPES.CUSTOMER] = 'Cliente';
USERTYPES_LABELS[USERTYPES.ADMINISTRATOR] = 'Administrador';
USERTYPES_LABELS[USERTYPES.SYSTEM] = 'Soporte de Sistema';
USERTYPES_LABELS[USERTYPES.SUPER_ADMIN] = 'Super Administrador';

let STATUSES_LABELS = {};

let GENDERS_LABELS = {};

GENDERS_LABELS[GENDERS.MALE] = 'Male';
GENDERS_LABELS[GENDERS.FEMALE] = 'Female';


export {
    USERTYPES_LABELS,
    GENDERS_LABELS,
    STATUSES_LABELS,
    IDIOM_LEVELS_LABELS,
    ACHIEVEMENT_PLACES_LABELS
};

