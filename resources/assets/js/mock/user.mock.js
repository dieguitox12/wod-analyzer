import {
    USERTYPES,
    NOTIFICATION_REASONS
} from "common/constants/enums.constant";
import { userInitialState } from "common/store/reducers/user.reducer";

export const mockUserState = Object.assign({}, userInitialState);

export const mockUsers = [
    {
        id: 1,
        name: 'John Doe',
        email: 'j.doe@example.com',
        authToken: 'token',
        gender: {id: 1}
    }
];

export const mockNotifications = [
    {
        id: 1,
        content: 'John Doe',
        contentIsHtml: false,
        seen: false,
        createdAt: '2018-01-01',
        reason: {id: NOTIFICATION_REASONS.INVOICE}
    },
    {
        id: 2,
        content: '<p>Example</p>',
        contentIsHtml: true,
        seen: true,
        createdAt: '2018-01-01',
        reason: {id: NOTIFICATION_REASONS.PAYMENT}
    }
];

export const mockListUsers = [
    {
        id: 1,
        name: 'John Doe',
        membershipStatus: {id: 1, name: 'Pending'},
        email: 'j.doe@example.com',
        phone: '888 888 8888',
        gender: {id: 1},
        currentMembershipInvoice: null,
        agency: {name: 'Remax'},
        suspensionNotes: [
            {
                id: 1,
                note: 'Note',
                createdBy: {name: 'Name'},
                createdAt: '2018-01-01'
            }
        ],
        educations: [],
        idioms: [],
        achievements: [],
        userType: USERTYPES.SUPER_ADMIN,
        picture: null,
        identification: '00000000000',
        plan: {
            id: 1,
            name: 'Plan'
        },
        reportedTransactions: [],
        sectors: [
            {
                id: 1,
                name: 'Sector',
                city: {
                    id: 1,
                    name: 'City'
                }
            }
        ],
        realestateTypes: [],
        birthdate: '1994-02-02',
        createdAt: '2018-01-01',
        socialNetworks: [{id: 1, address: 'Address', networkId: 1}],
        address: {street: 'Street'},
        isAssistant: false,
        isActive: true,
        isPublished: true,
        isPending: false
    },
    {
        id: 2,
        name: 'Jane Doe',
        gender: {id: 2},
        membershipStatus: {
            id: 3,
            name: 'Payed'
        },
        suspensionNotes: [],
        email: 'ja.doe@example.com',
        phone: '888 888 8888',
        reportedTransactions: [],
        sectors: [
            {
                id: 1,
                name: 'Sector',
                city: {
                    id: 1,
                    name: 'City'
                }
            }
        ],
        realestateTypes: [],
        agency: {name: 'Remax'},
        createdAt: '2018-01-01',
        socialNetworks: [{id: 1, address: 'Address', networkId: 1}],
        identification: '00000000000',
        address: {street: 'Street'},
        plan: {
            id: 1,
            name: 'Plan'
        },
        userType: USERTYPES.BROKER,
        birthdate: '1994-02-02',
        picture: null,
        isActive: true,
        isAssistant: false,
        isPublished: true,
        isPending: false
    },
    {
        id: 3,
        name: 'Luis Alvarez',
        gender: {id: 1},
        membershipStatus: {
            id: 2,
            name: 'Invoiced'
        },
        suspensionNotes: [],
        email: 'l.alvarez@example.com',
        phone: '888 888 8888',
        birthdate: '1994-02-02',
        reportedTransactions: [],
        sectors: [
            {
                id: 1,
                name: 'Sector',
                city: {
                    id: 1,
                    name: 'City'
                }
            }
        ],
        realestateTypes: [],
        socialNetworks: [{id: 1, address: 'Address', networkId: 1}],
        address: {street: 'Street'},
        createdAt: '2018-01-01',
        agency: {name: 'Remax'},
        identification: '00000000000',
        plan: {
            id: 1,
            name: 'Plan'
        },
        userType: USERTYPES.BROKER,
        picture: null,
        isAssistant: false,
        isPublished: true,
        isActive: true,
        isPending: true
    },
    {
        id: 4,
        name: 'Pablo Perez',
        gender: {id: 1},
        membershipStatus: {
            id: 1,
            name: 'Pending'
        },
        suspensionNotes: [
            {
                id: 1,
                note: 'Note',
                createdBy: {name: 'Name'},
                createdAt: '2018-01-01'
            }
        ],
        email: 'p.perez@example.com',
        phone: '888 888 8888',
        reportedTransactions: [],
        sectors: [
            {
                id: 1,
                name: 'Sector',
                city: {
                    id: 1,
                    name: 'City'
                }
            }
        ],
        realestateTypes: [],
        socialNetworks: [{id: 1, address: 'Address', networkId: 1}],
        address: {street: 'Street'},
        createdAt: '2018-01-01',
        birthdate: '1994-02-02',
        agency: {name: 'Remax'},
        identification: '00000000000',
        plan: {
            id: 1,
            name: 'Plan'
        },
        userType: USERTYPES.ASSISTANT,
        picture: null,
        isActive: false,
        isPublished: true,
        isAssistant: false,
        isPending: false
    },
    {
        id: 5,
        name: 'Diego Mena',
        gender: {id: 1},
        suspensionNotes: [],
        membershipStatus: {
            id: 2,
            name: 'Invoiced'
        },
        email: 'd.mena@example.com',
        phone: '888 888 8888',
        reportedTransactions: [],
        sectors: [
            {
                id: 1,
                name: 'Sector',
                city: {
                    id: 1,
                    name: 'City'
                }
            }
        ],
        realestateTypes: [],
        socialNetworks: [{id: 1, address: 'Address', networkId: 1}],
        address: {street: 'Street'},
        createdAt: '2018-01-01',
        birthdate: '1994-02-02',
        identification: '00000000000',
        agency: {name: 'Remax'},
        plan: {
            id: 1,
            name: 'Plan'
        },
        userType: USERTYPES.SUPER_ADMIN,
        picture: null,
        isActive: true,
        isPublished: false,
        isAssistant: false,
        isPending: true
    }
];
