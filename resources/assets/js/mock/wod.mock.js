import { wodInitialState } from "common/store/reducers/wod.reducer";

export const mockWodState = Object.assign({}, wodInitialState);

export const mockWodsDetails = [
    {
        "name": "DT (Scaled)",
        "rounds": 5,
        "timeCap": null,
        "createdAt": "2019-01-11 03:33:19",
        "updatedAt": "2019-01-11 03:33:19",
        "movements": [
            {
                "movement": {
                    "name": "Deadlift",
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 16
                },
                "manMeasure": {
                    "name": "Pounds",
                    "symbol": "lb",
                    "value": 135,
                    "measurementType": {
                        "name": "Weight",
                        "createdAt": "2019-01-03 00:00:00",
                        "updatedAt": null,
                        "id": 2
                    },
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 5
                },
                "womanMeasure": {
                    "name": "Pounds",
                    "symbol": "lb",
                    "value": 75,
                    "measurementType": {
                        "name": "Weight",
                        "createdAt": "2019-01-03 00:00:00",
                        "updatedAt": null,
                        "id": 2
                    },
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 5
                },
                "createdAt": "2019-01-11 03:33:19",
                "updatedAt": "2019-01-11 03:33:19",
                "reps": 12,
                "id": 35
            }, {
                "movement": {
                    "name": "Hang Power Clean",
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 24
                },
                "manMeasure": {
                    "name": "Pounds",
                    "symbol": "lb",
                    "value": 135,
                    "measurementType": {
                        "name": "Weight",
                        "createdAt": "2019-01-03 00:00:00",
                        "updatedAt": null,
                        "id": 2
                    },
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 5
                },
                "womanMeasure": {
                    "name": "Pounds",
                    "symbol": "lb",
                    "value": 75,
                    "measurementType": {
                        "name": "Weight",
                        "createdAt": "2019-01-03 00:00:00",
                        "updatedAt": null,
                        "id": 2
                    },
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 5
                },
                "createdAt": "2019-01-11 03:33:19",
                "updatedAt": "2019-01-11 03:33:19",
                "reps": 9,
                "id": 36
            }, {
                "movement": {
                    "name": "Push Jerk",
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 33
                },
                "manMeasure": {
                    "name": "Pounds",
                    "symbol": "lb",
                    "value": 135,
                    "measurementType": {
                        "name": "Weight",
                        "createdAt": "2019-01-03 00:00:00",
                        "updatedAt": null,
                        "id": 2
                    },
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 5
                },
                "womanMeasure": {
                    "name": "Pounds",
                    "symbol": "lb",
                    "value": 75,
                    "measurementType": {
                        "name": "Weight",
                        "createdAt": "2019-01-03 00:00:00",
                        "updatedAt": null,
                        "id": 2
                    },
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 5
                },
                "createdAt": "2019-01-11 03:33:19",
                "updatedAt": "2019-01-11 03:33:19",
                "reps": 6,
                "id": 37
            }
        ],
        "id": 13,
        "wodScore": {
            "createdAt": "2019-01-22 15:18:01",
            "updatedAt": "2019-01-22 15:18:01",
            "scores": [
                {
                    "value": 90,
                    "part": 1,
                    "createdAt": "2019-01-22 15:18:01",
                    "updatedAt": "2019-01-22 15:18:01",
                    "id": 42
                }, {
                    "value": 100,
                    "part": 2,
                    "createdAt": "2019-01-22 15:18:01",
                    "updatedAt": "2019-01-22 15:18:01",
                    "id": 43
                }, {
                    "value": 110,
                    "part": 3,
                    "createdAt": "2019-01-22 15:18:01",
                    "updatedAt": "2019-01-22 15:18:01",
                    "id": 44
                }, {
                    "value": 125,
                    "part": 4,
                    "createdAt": "2019-01-22 15:18:01",
                    "updatedAt": "2019-01-22 15:18:01",
                    "id": 45
                }, {
                    "value": 140,
                    "part": 5,
                    "createdAt": "2019-01-22 15:18:01",
                    "updatedAt": "2019-01-22 15:18:01",
                    "id": 46
                }
            ],
            "id": 12
        }
    },
    {
        "name": "DT (Scaled)",
        "rounds": 5,
        "timeCap": null,
        "createdAt": "2019-01-11 03:33:19",
        "updatedAt": "2019-01-11 03:33:19",
        "movements": [
            {
                "movement": {
                    "name": "Deadlift",
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 16
                },
                "manMeasure": {
                    "name": "Pounds",
                    "symbol": "lb",
                    "value": 135,
                    "measurementType": {
                        "name": "Weight",
                        "createdAt": "2019-01-03 00:00:00",
                        "updatedAt": null,
                        "id": 2
                    },
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 5
                },
                "womanMeasure": {
                    "name": "Pounds",
                    "symbol": "lb",
                    "value": 75,
                    "measurementType": {
                        "name": "Weight",
                        "createdAt": "2019-01-03 00:00:00",
                        "updatedAt": null,
                        "id": 2
                    },
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 5
                },
                "createdAt": "2019-01-11 03:33:19",
                "updatedAt": "2019-01-11 03:33:19",
                "reps": 12,
                "id": 35
            }, {
                "movement": {
                    "name": "Hang Power Clean",
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 24
                },
                "manMeasure": {
                    "name": "Pounds",
                    "symbol": "lb",
                    "value": 135,
                    "measurementType": {
                        "name": "Weight",
                        "createdAt": "2019-01-03 00:00:00",
                        "updatedAt": null,
                        "id": 2
                    },
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 5
                },
                "womanMeasure": {
                    "name": "Pounds",
                    "symbol": "lb",
                    "value": 75,
                    "measurementType": {
                        "name": "Weight",
                        "createdAt": "2019-01-03 00:00:00",
                        "updatedAt": null,
                        "id": 2
                    },
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 5
                },
                "createdAt": "2019-01-11 03:33:19",
                "updatedAt": "2019-01-11 03:33:19",
                "reps": 9,
                "id": 36
            }, {
                "movement": {
                    "name": "Push Jerk",
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 33
                },
                "manMeasure": {
                    "name": "Pounds",
                    "symbol": "lb",
                    "value": 135,
                    "measurementType": {
                        "name": "Weight",
                        "createdAt": "2019-01-03 00:00:00",
                        "updatedAt": null,
                        "id": 2
                    },
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 5
                },
                "womanMeasure": {
                    "name": "Pounds",
                    "symbol": "lb",
                    "value": 75,
                    "measurementType": {
                        "name": "Weight",
                        "createdAt": "2019-01-03 00:00:00",
                        "updatedAt": null,
                        "id": 2
                    },
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 5
                },
                "createdAt": "2019-01-11 03:33:19",
                "updatedAt": "2019-01-11 03:33:19",
                "reps": 6,
                "id": 37
            }
        ],
        "id": 13,
        "wodScore": {
            "createdAt": "2019-01-11 03:33:19",
            "updatedAt": "2019-01-11 03:33:19",
            "scores": [
                {
                    "value": 100,
                    "part": 1,
                    "createdAt": "2019-01-11 03:33:19",
                    "updatedAt": "2019-01-11 03:33:19",
                    "id": 32
                }, {
                    "value": 120,
                    "part": 2,
                    "createdAt": "2019-01-11 03:33:19",
                    "updatedAt": "2019-01-11 03:33:19",
                    "id": 33
                }, {
                    "value": 140,
                    "part": 3,
                    "createdAt": "2019-01-11 03:33:19",
                    "updatedAt": "2019-01-11 03:33:19",
                    "id": 34
                }, {
                    "value": 170,
                    "part": 4,
                    "createdAt": "2019-01-11 03:33:19",
                    "updatedAt": "2019-01-11 03:33:19",
                    "id": 35
                }, {
                    "value": 198,
                    "part": 5,
                    "createdAt": "2019-01-11 03:33:19",
                    "updatedAt": "2019-01-11 03:33:19",
                    "id": 36
                }
            ],
            "id": 10
        }
    },
    {
        "name": "Grace",
        "rounds": 1,
        "timeCap": null,
        "createdAt": "2019-01-03 00:00:00",
        "updatedAt": null,
        "movements": [
            {
                "movement": {
                    "name": "Clean & Jerk",
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 6
                },
                "manMeasure": {
                    "name": "Pounds",
                    "symbol": "lb",
                    "value": 135,
                    "measurementType": {
                        "name": "Weight",
                        "createdAt": "2019-01-03 00:00:00",
                        "updatedAt": null,
                        "id": 2
                    },
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 5
                },
                "womanMeasure": {
                    "name": "Pounds",
                    "symbol": "lb",
                    "value": 95,
                    "measurementType": {
                        "name": "Weight",
                        "createdAt": "2019-01-03 00:00:00",
                        "updatedAt": null,
                        "id": 2
                    },
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 5
                },
                "createdAt": "2019-01-03 00:00:00",
                "updatedAt": null,
                "reps": 30,
                "id": 4
            }
        ],
        "id": 2,
        "wodScore": {
            "createdAt": "2019-01-04 23:01:07",
            "updatedAt": "2019-01-04 23:01:07",
            "scores": [
                {
                    "value": 180,
                    "part": 1,
                    "createdAt": "2019-01-04 23:01:07",
                    "updatedAt": "2019-01-04 23:01:07",
                    "id": 26
                }
            ],
            "id": 8
        }
    },
    {
        "name": "Cindy",
        "rounds": null,
        "timeCap": 1200,
        "createdAt": "2019-01-03 00:00:00",
        "updatedAt": null,
        "movements": [
            {
                "movement": {
                    "name": "Pull Up",
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 2
                },
                "manMeasure": null,
                "womanMeasure": null,
                "createdAt": "2019-01-03 00:00:00",
                "updatedAt": null,
                "reps": 5,
                "id": 1
            }, {
                "movement": {
                    "name": "Push Up",
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 3
                },
                "manMeasure": null,
                "womanMeasure": null,
                "createdAt": "2019-01-03 00:00:00",
                "updatedAt": null,
                "reps": 10,
                "id": 2
            }, {
                "movement": {
                    "name": "Squat",
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 1
                },
                "manMeasure": null,
                "womanMeasure": null,
                "createdAt": "2019-01-03 00:00:00",
                "updatedAt": null,
                "reps": 15,
                "id": 3
            }
        ],
        "id": 1,
        "wodScore": {
            "createdAt": "2019-01-03 00:00:00",
            "updatedAt": null,
            "scores": [
                {
                    "value": 30,
                    "part": 1,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 3
                }, {
                    "value": 30,
                    "part": 2,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 4
                }, {
                    "value": 30,
                    "part": 3,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 5
                }, {
                    "value": 30,
                    "part": 4,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 6
                }, {
                    "value": 30,
                    "part": 5,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 7
                }, {
                    "value": 30,
                    "part": 6,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 8
                }, {
                    "value": 30,
                    "part": 7,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 9
                }, {
                    "value": 30,
                    "part": 8,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 10
                }, {
                    "value": 30,
                    "part": 9,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 11
                }, {
                    "value": 30,
                    "part": 10,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 12
                }, {
                    "value": 30,
                    "part": 11,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 13
                }, {
                    "value": 30,
                    "part": 12,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 14
                }, {
                    "value": 30,
                    "part": 13,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 15
                }, {
                    "value": 30,
                    "part": 14,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 16
                }, {
                    "value": 30,
                    "part": 15,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 17
                }, {
                    "value": 30,
                    "part": 16,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 18
                }, {
                    "value": 30,
                    "part": 17,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 19
                }, {
                    "value": 30,
                    "part": 18,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 20
                }, {
                    "value": 30,
                    "part": 19,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 21
                }, {
                    "value": 17,
                    "part": 20,
                    "createdAt": "2019-01-03 00:00:00",
                    "updatedAt": null,
                    "id": 22
                }
            ],
            "id": 1
        }
    }
];
