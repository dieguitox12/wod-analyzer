<!DOCTYPE HTML>
<html id="html">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="manifest" href="manifest.json">
      <link rel="shortcut icon" href="favicon.ico">
      <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Black+Ops+One" />
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic">
      <meta name="theme-color" content="#ffffff">

      <title>Wod Analyzer</title>

      <!-- Fonts -->
      <link href="/css/app.min.css" rel="stylesheet" type="text/css">
      <style type="text/css" media="print">
        @page {
            size: landscape;
        }
      </style>
  </head>
  <body>
    <noscript>
        You need to enable JavaScript to run this app.
    </noscript>
    <!-- MODALS WILL BE RENDERED HERE. -->
    <div id="modals"></div>

    <!-- MAIN APPLICATION -->
    <div id="root"></div>

    <!--  The idea is to send the authenticated user's information to speed up the loading time. -->
    @auth
        <script>
            var user = {!! json_encode(Auth::user()->toArray()) !!}
        </script>
    @endauth
    <script src="/js/app.min.js"></script>
  </body>
</html>
