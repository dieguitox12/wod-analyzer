<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta property="og:title" content="*|MC:SUBJECT|*">
    <title>*|MC:SUBJECT|*</title>

  <style type="text/css">
		body{
			margin:0;
		}
</style></head>
  <body>
    <center>
      <div style="background-color:#FFFFFF;padding:40px 0 70px;color:#5D5F5F;font-size:18px;margin:0;text-align:center;min-width:640px;width:100%;height:100%;">
        <!-- REAL-CONTENT -->

        <table border="0" cellpadding="0" cellspacing="0" width="620" style="text-align:center;width:100%;margin:0;">
          <tr>
            <td>
              <table border="0" cellpadding="0" cellspacing="0" width="560" class="box" style="background-color:#003833;border:1px solid #003833;border-top-left-radius:12px;border-top-right-radius:12px;font-size:16px;margin:0 auto;min-height:200px;padding:40px 50px;text-align:center;width:100%;">
                <tr>
                  <td>
                    <a href="http://wodanalyer.com">
                      <img width="372" height="62" src="" alt="WodAnalyzer"></a>
                    </td>
                  </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="560" class="box" style="background-color:#FFFFFF;border:1px solid #DEDEDE;border-top:0;border-bottom-left-radius:12px;border-bottom-right-radius:12px;margin:0 auto;padding:40px 60px;text-align:left;width:100%;">
                  <tr class="message">
                    <td width="460" colspan="2" style="width:100%;">
                      <h3 style="color:#003833;font-size:24px;font-weight:bold;margin:0;">Hello {{$name}},</h3>
                      <p>To change your password of Wod Analyzer, click in the button down below.</p>
                      <table border="0" cellpadding="0" cellspacing="0" width="458" class="box" style="font-size:14px;max-width:458px;text-align:center;width:100%;">
                        <tr>
                          <td align="left">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate !important;background-color:#003833;">
                              <tbody>
                                <tr>
                                  <td align="left" style="font-family:Arial;font-size:16px;" valign="middle">
                                    <a href="{{ $url }}" style="font-weight:normal;letter-spacing:normal;line-height:48px;min-width:270px;width:270px;text-align:center;text-decoration:none;color:#FFFFFF;display:block;" target="_blank" title="CHANGE YOUR PASSWORD">CHANGE YOUR PASSWORD</a>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="580" class="box" style="font-size:16px;margin:0 auto;padding:40px 50px 0;text-align:center;width:100%;">
          <tr>
            <td>
              <td style="font-family:Helvetica Neue,Helvetica,Lucida Grande,tahoma,verdana,arial,sans-serif;font-size:13px;color:#aaaaaa;line-height:16px">This message was sent to <a href="mailto:{{$email}}" style="color:#003833;text-decoration:none" target="_blank">{{$email}}</a> <br>{{ \Carbon\Carbon::now()->year }} &copy; Wod Analyzer</td>
            </p>
          </td>
        </tr>
      </table>
    </div>
  </center>
</body>
</html>
