<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('login');

Route::group(['prefix' => 'json'], function () {

    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'AuthController@login');
        Route::post('register', 'AuthController@register');
        Route::post('reset', 'AuthController@resetPassword');
        Route::post('forgot', 'AuthController@forgotPassword');
    });

    Route::group(['middleware' => 'auth'], function () {
        Route::group(['prefix' => 'auth'], function () {
            Route::post('logout', 'AuthController@logout');
        });

        Route::group(['prefix' => 'wod'], function () {
            Route::get('all', 'WodController@allWodsNames');
            Route::get('movements', 'WodController@allMovements');
            Route::get('search', 'WodController@search');
            Route::post('score', 'WodController@score');
            Route::post('create', 'WodController@create');
            Route::get('score/{id}', 'WodController@findByWodScoreId');
            Route::post('upload', 'WodController@uploadVideo');
            Route::delete('score/{id}', 'WodController@delete');
            Route::post('score/edit/{id}', 'WodController@edit');
            Route::get('optimize/{id}', 'WodController@optimize');
        });
    });
});

// Defines the front end routes. Theses will be handled by React Router.
$frontendRoutes = [
    '/',
    '/login',
    '/register',
    '/forgot-password',
    '/reset-password',

    '/scores',
    '/scores/edit/{id}',
    '/scores/{id}',

    '/my-profile',
    '/log',

    '/404'
];

foreach ($frontendRoutes as $route) {
    Route::get($route, 'AppController@reactRoute');
}
